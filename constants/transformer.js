import * as _ from 'lodash';

const tranformer = {
    parseJson: (data, activeLang = null) => {
        return JSON.parse(data)[activeLang || 'vi-VN'] || '';
    },

    redirectLink: (url) => {
        if (!url.includes('.') || url.includes('http')) {
            if (url.includes('https://')) {
                return '//' + url.substring(8);
            }

            if (url.includes('http://')) {
                return '//' + url.substring(7);
            }
            return url;
        } else {
            return '//' + url;
        }
    },

    validateEmail: (email) => {
        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (email.match(mailformat)) { return true; }
        return false;
    },

    convertUnsignedChars: (str) => {
        str.trim();
        str = str.replace(/!|@|#|%|&|=|<|>|`|~|\?|\(|\)|\$|\^|\*|\+|\.|\/|\\/g, '');
        return str;
    },

    validatePhoneNumber: (phoneNumber) => {
        const isNum = /^\d+$/.test(phoneNumber);
        let result = true;
        let message = '';

        if (isNum) {
            if (phoneNumber.length < 9 || phoneNumber.length > 12) {
                result = false;
                message = 'Phone number can not have less than 9 digits or more than 12 digits';
            }
            if (phoneNumber.charAt(0) !== '0') {
                result = false;
                message = 'You have entered an invalid phone number! Phone number must start with 0';
            }
        } else {
            result = false;
            message = 'You have entered an invalid phone number!';
        }

        return { result, message };
    },

    convertCurrency: (numStr) => {
        const x = parseFloat(numStr);

        const currencyString = x.toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
            maximumFractionDigits: 2,
            minimumFractionDigits: 0
        });

        return currencyString.replace('$', '') + ' ₫';
    },

    truncate: (source, size) => {
        if (source == null) {
            source = '';
            return source;
        }
        return source.length > size ? source.slice(0, size - 1) + '…' : source;
    },

    stripVietnameseChars: (str) => {
        str = str.replace(/\s+/g, ' ');
        str.trim();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
        str = str.replace(/Đ/g, 'D');
        return str.toUpperCase();
    },

    validateCustomerName(name) {
        const regex = /^[a-zA-Z][a-zA-Z\s]*$/;
        return regex.test(this.stripVietnameseChars(name));
    },

    convertToPramsURI: (obj) => {
        return Object
            .keys(obj)
            .map(k => k + '=' + obj[k])
            .join('&');
    },

    IsJsonString(str) {
        const typeOfStr = typeof str;
        if (typeOfStr !== 'string') {
            return false;
        }
        try {
            const json = JSON.parse(str);
            if (!json || typeof json !== 'object') {
                return false;
            }
        } catch (e) {
            return false;
        }
        return true;
    },

    addToCart(e, product, quantity = 1) {
        e.preventDefault();
        const initCart = {
            items: [],
            totalQuantity: 0,
            totalPrice: 0
        };
        let cart = localStorage.getItem('cart');

        const data = {
            productId: product.productId,
            discount: 0,
            discountType: 0,
            concurrencyStamp: '',
            quantity: quantity
        };

        if (cart) {
            const newCart = JSON.parse(cart);
            const itemIndex = newCart.items.findIndex(item => item.productId === data.productId);
            if (itemIndex > -1) {
                newCart.items[itemIndex].quantity += data.quantity;
                newCart.totalQuantity += data.quantity;
                newCart.totalPrice += data.salePrice * data.quantity;
            } else {
                newCart.items = [...newCart.items, data];
                newCart.totalQuantity += data.quantity;
                newCart.totalPrice += data.salePrice * data.quantity;
            }

            localStorage.setItem('cart', JSON.stringify(newCart));
            alert('Item is added to your cart');
        } else {
            cart = initCart;
            cart.items = [data];
            cart.totalPrice = data.salePrice * data.quantity;
            cart.totalQuantity = data.quantity;
            localStorage.setItem('cart', JSON.stringify(cart));
            alert('Item is added to your cart');
        }
    },

    favoriteProduct(e, productId) {
        e.preventDefault();
        let favorite = localStorage.getItem('favorite');

        if (favorite) {
            let newFavorite = JSON.parse(favorite);
            const itemIndex = newFavorite.findIndex(item => item === productId);
            if (itemIndex > -1) {
                newFavorite.splice(itemIndex, 1);
                localStorage.setItem('favorite', JSON.stringify(newFavorite));
                alert('Item is remove to your favorite product');
            } else {
                newFavorite = [...newFavorite, productId];
                localStorage.setItem('favorite', JSON.stringify(newFavorite));
                alert('Item is added to your favorite product');
            }
        } else {
            favorite = [productId];
            localStorage.setItem('favorite', JSON.stringify(favorite));
            alert('Item is added to your favorite product');
        }
    },

    removeBreakTag: (str) => {
        str.trim();
        str = str.replace(/<\/br>/g, '');
        return str;
    },

    getNewsTitle: (str) => {
        if (str.includes('-')) return str.split('-')[0];

        return str;
    },

    formatPhoneDisplay: (phone) => {
        const result = [];
        result.push(phone.slice(0, 4));
        result.push(phone.slice(4, 7));
        result.push(phone.slice(7, 12));

        return result.join(' ');
    },

    createHTML: (string) => {
        return { __html: string };
    },

    generateQuery: (cart) => {
        let query = '';
        cart.items.map(item => {
            query += `listProductId=${item.productId}&`;
        });
        return query;
    },

    fetch: (param) => {
        if (param && Array.isArray(param)) {
            return tranformer.fetchCollection(param);
        } else if (param && typeof param === 'object') {
            return tranformer.fetchObject(param);
        }
        return param;
    },
    fetchCollection: (param) => {
        return param.map(item => tranformer.fetch(item));
    },

    fetchObject: (param) => {
        const data = {};

        _.forOwn(param, (value, key) => {
            data[_.camelCase(key)] = tranformer.fetch(value);
        });
        return data;
    },

    setDefaultHeaders: (headers) => {
        const accessToken = localStorage.getItem('access_token');
        if (accessToken) {
            return {
                ...headers,
                Authorization: `Bearer ${accessToken}`
            };
        } else {
            return headers;
        }
    }
};

export default tranformer;
