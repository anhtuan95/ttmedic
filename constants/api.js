import fetch from 'isomorphic-unfetch';
import { API_URL, TENANT_ID, WEBSITE_ID, AUTH_URL_TTMEDIC, CORE_API_URL_TTMEDIC, CLIENT_ID, CLIENT_SECRET, SCOPES } from './config';

const api = {
    get: async (module, url, query) => {
        let queryParams = '';
        if (query !== undefined && Object.keys(query) !== 0) {
            queryParams = Object
                .keys(query)
                .map(k => k + '=' + query[k])
                .join('&');
        }
        const baseUrl = `${API_URL}${module}/${TENANT_ID}/${WEBSITE_ID}`;
        const endpointURL = url ? `${baseUrl}/${url}?${queryParams}` : `${baseUrl}?${queryParams}`;
        const res = await fetch(encodeURI(endpointURL), {
            'Accept-Language': 'vi-VN'
        });

        const resJson = await res.json();

        return resJson;
    },

    post: async (module, url, metaData) => {
        const baseUrl = `${API_URL}${module}/${TENANT_ID}/${WEBSITE_ID}`;
        const endpointURL = url ? `${baseUrl}/${url}` : `${baseUrl}`;

        const res = await fetch(endpointURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(metaData)
        });

        const resJson = await res.json();

        return resJson;
    },
    // TTMEDIC

    setDefaultHeaders: (header) => {
        if (header !== {}) {
            return {
                ...header,
                Authorization: `Bearer ${localStorage.getItem('access_token')}`
            };
        } else {
            return { Authorization: `Bearer ${localStorage.getItem('access_token')}` };
        }
    },

    getCore: async (module, url, query) => {
        let queryParams = '';
        if (query !== undefined && Object.keys(query) !== 0) {
            queryParams = Object
                .keys(query)
                .map(k => k + '=' + query[k])
                .join('&');
        }
        const baseUrl = `${CORE_API_URL_TTMEDIC}${module}`;
        const endpointURL = url ? `${baseUrl}/${url}?${queryParams}` : `${baseUrl}?${queryParams}`;
        // const res = await fetch(encodeURI(endpointURL), {
        //     'Accept-Language': 'vi-VN'
        // });

        const res = await fetch(encodeURI(endpointURL), {
            method: 'GET',
            headers: api.setDefaultHeaders({
                'Accept-Language': 'vi-VN'
            })
        });

        const resJson = await res.json();

        return resJson;
    },

    login: async (metaData) => {
        const endpointURL = `${AUTH_URL_TTMEDIC}/connect/token`;
        // const endpointURL = url ? `${baseUrl}/${url}` : `${baseUrl}`;
        const input = {
            grant_type: 'password',
            client_id: CLIENT_ID,
            scope: SCOPES,
            username: metaData.username,
            password: metaData.password,
            client_secret: CLIENT_SECRET
        };

        const formBody = Object.keys(input).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(input[key])).join('&');

        const res = await fetch(endpointURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Access-Control-Allow-Origin': '*'
            },
            body: formBody
        });

        const resJson = await res.json();
        console.log('resJson', resJson);
        return resJson;
    },

    logout: async () => {
        const endpointURL = `${AUTH_URL_TTMEDIC}/connect/revocation`;

        const input = {
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            // token: '283F043321D032D4980CD2D44AE90AC9494CFEAEDE86527EE9398ED4BA1F190F',
            token: localStorage.getItem('access_token'),
            token_type_hint: 'access_token'
        };

        // const headers = api.setDefaultHeaders();
        const formBody = Object.keys(input).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(input[key])).join('&');

        const res = await fetch(endpointURL, {
            method: 'POST',
            headers: {
                'Accept-Language': 'vi-VN',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                Authorization: `Bearer ${localStorage.getItem('access_token')}`
            },
            body: formBody
        });

        const resJson = await res.json();
        return resJson;
    },

    // ko su dung -wip
    getWithAuth: async (module, url, query) => {
        let queryParams = '';
        if (query !== undefined && Object.keys(query) !== 0) {
            queryParams = Object
                .keys(query)
                .map(k => k + '=' + query[k])
                .join('&');
        }
        const baseUrl = `${API_URL}${module}`;
        const endpointURL = url ? `${baseUrl}/${url}?${queryParams}` : `${baseUrl}?${queryParams}`;
        const res = await fetch(encodeURI(endpointURL), api.setDefaultHeaders({
            'Accept-Language': 'vi-VN'
        }));

        const resJson = await res.json();

        return resJson;
    }
};

export default api;
