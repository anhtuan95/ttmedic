// VJ-SOFT SITE - DEVELOP ENVIRONMENT
// export const FILE_URL = "https://spafile.ghmsoft.vn/";
// export const API_URL = 'http://vjwebsiteapi.ghmsoft.vn/api/v1/'
// export const TENANT_ID = '4ffc2156-cf4d-4a58-acff-228456e563e8'
// export const WEBSITE_ID = '4af9ce46-08ab-456f-99fb-0405c92cc704'

// GHM-SOFT SITE - STAGING ENVIRONMENT
export const FILE_URL = 'https://ghmspafile.ghmsoft.vn/';
export const API_URL = 'https://ghmwebsiteapi.ghmsoft.vn/api/v1/';
export const TENANT_ID = '43a72149-0f34-483a-a27c-f37de7d2931a';
export const WEBSITE_ID = 'b2bbe599-56c3-4dd2-abd0-3a0865f0648a';

export const SPA_API_URL = 'https://ghmspaapi.ghmsoft.vn/api/v1/';

export const SCOPES = 'GHM_Website_Core';
export const CLIENT_ID = 'GHMSOFTCLIENT';
export const CLIENT_SECRET = 'GHMSOFT';

export const URL_NULL_IMG = 'uploads/images/no-image.png';
export const COUNTRY_ID = 'c8bf4873-f769-409b-be90-8872ec5f7025';

// TTMEDIC
export const AUTH_URL_TTMEDIC = 'https://authclient.phongkham125thaithinh.com';
export const FILE_URL_TTMEDIC = 'https://file.phongkham125thaithinh.com/';
export const API_URL_TTMEDIC = 'http://websiteclient.phongkham125thaithinh.com/api/v1/';
export const CORE_API_URL_TTMEDIC = 'https://core.phongkham125thaithinh.com/api/v1/';

export const BLUE_HEAD_PAGE = ['/', '/gioi-thieu', '/tin-tuc'];

export const NEWS_TYPES = [
    { id: 0, name: '{"vi-VN":"Tin tức","en-US":"News"}' },
    { id: 1, name: '{"vi-VN":"Sự kiện","en-US":"Event"}' }
];

export const GENDER = [
    { id: 0, name: '{"vi-VN":"Nữ","en-US":"Female"}' },
    { id: 1, name: '{"vi-VN":"Nam","en-US":"Male"}' }
];

export const DATE_FORMAT = [
    { id: 0, languageId: 'vi-VN', format: 'DD [tháng] M, YYYY' },
    { id: 1, languageId: 'en-US', format: 'DD MMM YYYY' }
];

export const OBJECT_TYPE = {
    CATEGORY_NEWS: 1,
    NEWS: 2,
    CATEGORY_PRODUCT: 3,
    PRODUCT: 4,
    SERVICE: 5
};

export const MENU_OBJECT_TYPES = [
    { id: 0, name: '{"vi-VN":"Tùy chỉnh", "en-US":"Custom"}', seoLink: '' },
    { id: 1, name: '{"vi-VN":"Chuyên mục tin tức", "en-US":"Category News"}', seoLink: '' },
    { id: 2, name: '{"vi-VN":"Tin tức", "en-US":"News"}', seoLink: '/tin-tuc' },
    { id: 3, name: '{"vi-VN":"Chuyên mục sản phẩm", "en-US":"Category Product"}', seoLink: '/san-pham' },
    { id: 4, name: '{"vi-VN":"Sản phẩm", "en-US":"Product"}', seoLink: '/san-pham' },
    { id: 5, name: '{"vi-VN":"Dịch vụ", "en-US":"Service"}', seoLink: '/dich-vu' },
    { id: 6, name: '{"vi-VN":"Phản hồi khách hàng", "en-US":"Customer feedback"}', seoLink: '' },
    { id: 7, name: '{"vi-VN":"Nhóm FAQ", "en-US":"FAQ group"}', seoLink: '' },
    { id: 8, name: '{"vi-VN":"Album", "en-US":"Album"}', seoLink: '' }
];

export const CONSTANT_TEXT = {
    WORKING_HOURS: { 'vi-VN': 'GIỜ LÀM VIỆC', 'en-US': 'WORKING HOURS' },
    BOOKING: { 'vi-VN': 'Đặt lịch', 'en-US': 'Booking' },
    HOTLINE: { 'vi-VN': 'ĐƯỜNG DÂY NÓNG', 'en-US': 'HOTLINE' },
    READ_MORE: { 'vi-VN': 'Xem thêm', 'en-US': 'Read More' },
    LINK: { 'vi-VN': 'Liên kết', 'en-US': 'Link' },
    ADVISORY: { 'vi-VN': 'Tư vấn miễn phí', 'en-US': 'Free Advisory' },
    SIGNUP_FOR: { 'vi-VN': 'Đăng ký nhận tin', 'en-US': 'Sign up for' },
    YOUR_EMAIL: { 'vi-VN': 'Email của bạn', 'en-US': 'Your email' },
    SCHEDULE: { 'vi-VN': 'Đặt lịch', 'en-US': 'Booking' },
    COUNTINUE_BUYING: { 'vi-VN': 'Tiếp tục mua hàng', 'en-US': 'Countinue Buying' },
    YOUR_CART: { 'vi-VN': 'Giỏ hàng của bạn', 'en-US': 'Your cart' },
    CHECKOUT: { 'vi-VN': 'Thanh toán', 'en-US': 'Checkout' },
    SEE_MORE: { 'vi-VN': 'Xem thêm', 'en-US': 'See more' },
    PARTNERS: { 'vi-VN': 'Đối tác', 'en-US': 'Partners' },
    PRODUCTS: { 'vi-VN': 'Sản phẩm', 'en-US': 'Products' },
    SEARCH: { 'vi-VN': 'Tìm kiếm', 'en-US': 'Search' },
    DELIVERY: { 'vi-VN': 'Vận chuyển', 'en-US': 'Delivery' },
    TOTAL: { 'vi-VN': 'Tổng cộng', 'en-US': 'Total' },
    TEMPORARY: { 'vi-VN': 'Tạm tính', 'en-US': 'Temporary' },
    DELIVER_INFOR: { 'vi-VN': 'Thông tin giao hàng', 'en-US': 'Delivery Information' },
    CUSTOMER_NAME: { 'vi-VN': 'Tên khách hàng', 'en-US': 'Customer name' },
    PHONE_NUMBER: { 'vi-VN': 'Số điện thoại', 'en-US': 'Phone number' },
    ADDRESS: { 'vi-VN': 'Địa chỉ', 'en-US': 'Address' },
    PROVINCE: { 'vi-VN': 'Tỉnh/ Thành phố', 'en-US': 'Province' },
    DISTRICT: { 'vi-VN': 'Quận/ Huyện', 'en-US': 'District' },
    DELIVER_METHOD: { 'vi-VN': 'Phương thức vận chuyển', 'en-US': 'Delivery method' },
    PAYMENT_METHOD: { 'vi-VN': 'Phương thức thanh toán', 'en-US': 'Payment method' },
    PAYMENT_ON_DELIVER: { 'vi-VN': 'Thanh toán khi nhận hàng', 'en-US': 'Payment on delivery' },
    BANK_TRANSFER: { 'vi-VN': 'Chuyển khoản ngân hàng', 'en-US': 'Bank transfer' },
    CART: { 'vi-VN': 'Giỏ hàng', 'en-US': 'Cart' },
    ORDER_NOTE: { 'vi-VN': 'Ghi chú đơn hàng', 'en-US': 'Order note' },
    COMPLETE_ORDER: { 'vi-VN': 'Hoàn tất đơn hàng', 'en-US': 'Complete the order' },
    FAVORITE: { 'vi-VN': 'Sản phẩm yêu thích', 'en-US': 'Favorite products' },
    DETAILS: { 'vi-VN': 'Chi tiết', 'en-US': 'Details' },
    USES: { 'vi-VN': 'Công dụng', 'en-US': 'Uses' },
    ELEMENTS: { 'vi-VN': 'Thành phần', 'en-US': 'Elements' },
    USER_MANUAL: { 'vi-VN': 'Hướng dẫn sử dụng', 'en-US': 'User manual' },
    INSTOCK: { 'vi-VN': 'Còn hàng', 'en-US': 'In stock' },
    OUTSTOCK: { 'vi-VN': 'Hết hàng', 'en-US': 'Out of stock' },
    DELETE: { 'vi-VN': 'Xóa', 'en-US': 'Delete' },
    CONSULTATION_PHONE: { 'vi-VN': 'Tư vấn qua điện thoại', 'en-US': 'Consultation through phone' },
    CONSULTATION_EMAIL: { 'vi-VN': 'Tư vấn qua email', 'en-US': 'Consutation through email' },
    CONSULTATION_CONTENT: { 'vi-VN': 'Nội dung cần tư vấn', 'en-US': 'Consutation content' },
    SEND: { 'vi-VN': 'GỬI', 'en-US': 'SEND' },
    EVENT: { 'vi-VN': 'Sự kiện', 'en-US': 'Event' },
    HIGHLIGHTS: { 'vi-VN': 'Nổi bật', 'en-US': 'highlights' },
    MONTH: { 'vi-VN': 'Tháng', 'en-US': 'Month' },
    JAN: { 'vi-VN': 'Tháng 1', 'en-US': 'Jan' },
    OPEN_FACILITY: { 'vi-VN': 'Default Tiêu đề', 'en-US': 'Default title' },
    CONTENT_SAMPLE: { 'vi-VN': 'Default mô tả', 'en-US': 'Default content' },
    ALL: { 'vi-VN': 'Tất cả', 'en-US': 'All' },
    NEWS: { 'vi-VN': 'Tin tức', 'en-US': 'News' },
    COURSE: { 'vi-VN': 'Khóa', 'en-US': 'Course' },
    ADD_TO_CART: { 'vi-VN': 'Thêm vào giỏ', 'en-US': 'Add to cart' },
    RELATED_PRODUCTS: { 'vi-VN': 'Sản phẩm khác', 'en-US': 'Related products' },
    EMPTY_CART: { 'vi-VN': 'Giỏ hàng của bạn đang trống', 'en-US': 'Your cart is empty' },
    CONTINUE_SHOPING: { 'vi-VN': 'Tiếp tục mua hàng', 'en-US': 'Continue shopping' },
    PROVISIONAL: { 'vi-VN': 'Tạm tính', 'en-US': 'Provisional' },
    AVAILABLE: { 'vi-VN': 'Tình trạng', 'en-US': 'Availability' },
    FREE_CONSULTATION: { 'vi-VN': 'Tư vấn miễn phí', 'en-US': 'Free Consultation' },
    FACILITY: { 'vi-VN': 'Cơ sở', 'en-US': 'Facility' },
    QUESTIONS: { 'vi-VN': 'Các câu hỏi', 'en-US': 'Question' },
    FREQUENT: { 'vi-VN': 'Thường gặp', 'en-US': 'Frequent' },
    BOOKING_APPOINTMENT: { 'vi-VN': 'Đặt lịch & Hẹn tư vấn', 'en-US': 'Booking Appointment' },
    SLOGAN: { 'vi-VN': 'J Medical Spa – Phong cách Châu Âu, Tiêu chuẩn Hoa Kỳ', 'en-US': 'J Medical Spa – European Style, American Standard' },
    CHOOSE_BRANCH: { 'vi-VN': 'Chọn cơ sở', 'en-US': 'Choose Branch' },
    CHOOSE_SERVICES: { 'vi-VN': 'Chọn dịch vụ', 'en-US': 'Choose Services' },
    INTEND_TIME: { 'vi-VN': 'Thời gian dự kiến', 'en-US': 'Intend Time' },
    BOOKING_NOTICE: { 'vi-VN': 'Chúng tôi sẽ liên hệ tư vấn và xác nhận thông tin.', 'en-US': 'We will contact you and confirm the information.' },
    BOOK_NOW: { 'vi-VN': 'Đăng ký ngay', 'en-US': 'Book now' },
    NOTE: { 'vi-VN': 'Ghi chú', 'en-US': 'Note' },
    SELECT_BRANCH_NOTICE: { 'vi-VN': 'Hãy chọn một cơ sở', 'en-US': 'Please select 1 branch' },
    BOOKING_SUCCESS: { 'vi-VN': ' Đặt hẹn thành công', 'en-US': ' Successfully Booking' },
    THANKS_STATEMENT: { 'vi-VN': 'Cảm ơn quý khách đã đặt hẹn tại: ', 'en-US': 'Thanks for booking our services at: ' },
    BOOKING_TIME: { 'vi-VN': 'Thời gian đặt lịch: ', 'en-US': 'Booking Time: ' },
    SERVICE: { 'vi-VN': 'DỊCH VỤ: ', 'en-US': 'SERVICE' }
};

export const LANGUAGEIDS = {
    vi: 'vi-VN',
    en: 'en-US',
    jp: 'ja-JP'
};

export const LANGUAGENAMES = {
    'vi-VN': 'vi',
    'en-US': 'en',
    'ja-JP': 'jp'
};
