import React, { Component } from 'react';
import SideMenu from '../../components/product/side_menu';
import SliderBanner from '../../components/product/slider_banner';
import FilterProduct from '../../components/product/filter_product';
import ProductList from '../../components/product/product_list';
import Pagination from '../../components/pagination';
import api from '../../constants/api';
import Transformer from '../../constants/transformer';
import Breadcrumb from '../../components/breadcrumb';
import Head from 'next/head';
import { OBJECT_TYPE } from '../../constants/config';
import PropTypes from 'prop-types';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: {
                page: 1,
                pageSize: 9,
                keyword: '',
                productsCategoryId: '',
                categoryName: ''
            },
            namePath: [],
            seoLinks: [],
            products: [],
            totalRows: 0,
            favorite: []
        };
    }

    componentDidMount() {
        const { products, productCategory, activeLang } = this.props;
        if (products) {
            this.setState({
                products: [...products.data],
                totalRows: products.totalRows,
                search: { ...this.state.search, productsCategoryId: productCategory.id ? productCategory.id : '' }
            });
        }

        const { namePath, seoLinks } = this.getCategoryName(activeLang);
        this.setState({
            namePath,
            seoLinks
        });
    }

    // eslint-disable-next-line camelcase
    UNSAFE_componentWillReceiveProps(nextProps) {
        const { activeLang } = this.props;
        if (nextProps.products.data !== this.props.products.data) {
            this.setState({
                products: [...nextProps.products.data],
                totalRows: nextProps.products.totalRows
            });
        }

        if (nextProps.categoryId !== this.props.categoryId) {
            this.setState({
                search: { ...this.state.search, page: 1, productsCategoryId: nextProps.categoryId }
            });
        }

        if (nextProps.activeLang !== activeLang) {
            const { namePath, seoLinks } = this.getCategoryName(nextProps.activeLang);
            this.setState({
                namePath,
                seoLinks,
                products: [...nextProps.products.data],
                totalRows: nextProps.products.totalRows
            });
        }
    }

    getCategoryName = (activeLang) => {
        const { productCategory } = this.props;
        const namePath = [];
        const seoLinks = [];
        if ({}.hasOwnProperty.call(productCategory, 'namePath')) {
            productCategory.namePath.split('/').map(item => {
                namePath.push(Transformer.parseJson(item, activeLang));
            });
        }
        if ({}.hasOwnProperty.call(productCategory, 'seoLink')) {
            productCategory.seoLinkPath.split('/').map(item => {
                seoLinks.push(item);
            });
        }
        return { namePath, seoLinks };
    }

    updateFavorite = (favorite) => {
        this.setState({ favorite: favorite });
    }

    search = async () => {
        const { activeLang } = this.props;
        const products = await api.get('Products', activeLang, this.state.search);
        await this.setState({
            products: products.data,
            totalRows: products.totalRows
        });
    }

    changeKeyword = (keyword) => {
        this.setState({
            search: { ...this.state.search, page: 1, keyword }
        }, () => this.search());
    }

    changePage = (page) => {
        this.setState({
            search: { ...this.state.search, page }
        }, () => this.search());
    }

    render() {
        const { productMenu, productMenuTree, productSlide, activeLang, updateCartQuantity, settings } = this.props;
        const { products, totalRows, search, favorite, namePath, seoLinks } = this.state;

        return (
            <div>
                {
                    <Head>
                        <meta name="description" content={Transformer.IsJsonString(settings.metaDescription) ? Transformer.parseJson(settings.metaDescription, activeLang) : ''}></meta>
                        <title>{Transformer.IsJsonString(settings.metaTitle) ? Transformer.parseJson(settings.metaTitle, activeLang) : ''}</title>
                    </Head>
                }
                <Breadcrumb type={[OBJECT_TYPE.PRODUCT, OBJECT_TYPE.CATEGORY_PRODUCT]} name={namePath} seoLinks={seoLinks} activeLang={activeLang} />
                <div className="product_page">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3">
                                <SideMenu sideMenu={productMenuTree} productMenu={productMenu} selectedId={search.productsCategoryId} activeLang={activeLang} />
                            </div>
                            <div className="col-md-9">
                                <SliderBanner productSlide={productSlide} />
                                <FilterProduct numbersOfResult={totalRows} categoryId={search.productsCategoryId} categoryName={search.categoryName} search={this.changeKeyword} activeLang={activeLang} />
                                <ProductList
                                    products={products}
                                    activeLang={activeLang}
                                    favoriteProduct={favorite}
                                    updateFavoriteProduct={this.updateFavorite}
                                    isFavorite={true}
                                    updateCartQuantity={updateCartQuantity} />
                            </div>
                        </div>
                    </div>
                    {
                        totalRows > 9 &&
                        <Pagination
                            position={'end'}
                            totalRows={totalRows}
                            search={search}
                            onChangePage={(pager) => this.changePage(pager.currentPage)} />
                    }
                </div>
            </div>
        );
    }
}

export async function getServerSideProps(props) {
    const categoryId = props.query.productsCategoryId ? props.query.productsCategoryId : '';
    const productMenuTree = await api.get('Menus/detail', 'left');
    const productMenu = await api.get('Menus', 'left');
    const products = await api.get('Products', 'vi-VN', { page: 1, pageSize: 9, ...props.query });
    const productSlide = await api.get('HardContents/detail-by-code', 'SLIDE-SAN-PHAM');
    const productCategory = categoryId ? await api.get('ProductCategory', categoryId) : {};

    return {
        props: {
            productMenu: productMenu[0],
            productMenuTree: productMenuTree.data,
            products: products,
            categoryId,
            productSlide: productSlide.data,
            productCategory: categoryId ? productCategory.data : {},
            query: props.query
        }
    };
}

Product.propTypes = {
    settings: PropTypes.any,
    productMenu: PropTypes.any,
    productMenuTree: PropTypes.any,
    productSlide: PropTypes.any,
    activeLang: PropTypes.any,
    updateCartQuantity: PropTypes.any,
    products: PropTypes.any,
    productCategory: PropTypes.any,
    categoryId: PropTypes.any
};

export default Product;
