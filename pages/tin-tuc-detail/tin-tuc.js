import React from 'react';
import HighLightEvents from '../../components/news/highlight_events';
import NewsList from '../../components/news/news_list';
import PropTypes from 'prop-types';
import Transformer from '../../constants/transformer';
import Head from 'next/head';
function NewsCategory(props) {
    const { highLightEvents, activeLang, settings } = props;

    return (
        <React.Fragment>
            <Head>
                <title>{Transformer.parseJson(settings.metaTitle, activeLang)}</title>
                <meta name="description" content={Transformer.parseJson(settings.metaDescription, activeLang)} />
                <meta name="keywords" content={Transformer.parseJson(settings.metaKeyword, activeLang)} />
            </Head>
            <div className="news_page">
                <HighLightEvents events={highLightEvents} activeLang={activeLang} />
                <NewsList activeLang={activeLang} />
            </div>
        </React.Fragment>
    );
}

NewsCategory.propTypes = {
    highLightEvents: PropTypes.any,
    activeLang: PropTypes.any,
    settings: PropTypes.any
};

export default NewsCategory;
