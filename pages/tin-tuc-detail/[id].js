import api from '../../constants/api';
import Content from '../../components/news/content';
import RelatedPosts from '../../components/news/related_posts';
import Breadcrumb from '../../components/breadcrumb';
import Head from 'next/head';
import PropTypes from 'prop-types';

function NewsDetail(props) {
    const { post, relatedPosts, socialNetworks, activeLang } = props;

    return (
        <div>
            <Head>
                <title>{post.metaTitle}</title>
                <meta name="description" content={post.metaDescription}></meta>
                <meta name="keywords" content={post.metaKeyword}></meta>
            </Head>
            <Breadcrumb type={[2]} name={[post.title]} activeLang={activeLang} />
            <div className="single_content">
                <div className="container">
                    <div className="row">
                        <RelatedPosts relatedPosts={relatedPosts} activeLang={activeLang} />
                        <Content post={post} socialNetworks={socialNetworks} activeLang={activeLang} />
                    </div>
                </div>
            </div>
        </div>
    );
}

export async function getServerSideProps(props) {
    const id = props.query.id ? props.query.id : null;

    const post = await api.get('News/detail', `${id}/vi-VN`);
    const relatedPosts = await api.get('News/related', `${id}/vi-VN/5`);
    const socialNetworks = await api.get('SocialNetworks', null);

    return {
        props: {
            post: post.data,
            relatedPosts,
            socialNetworks
        }
    };
}

NewsDetail.propTypes = {
    post: PropTypes.any,
    relatedPosts: PropTypes.any,
    socialNetworks: PropTypes.any,
    activeLang: PropTypes.any
};

export default NewsDetail;
