import React, { useState, useEffect } from 'react';
import userSWR from 'swr';
import fetch from 'unfetch';
import api from '../constants/api';
import { API_URL, TENANT_ID, WEBSITE_ID, FILE_URL, NEWS_TYPES, MENU_OBJECT_TYPES, DATE_FORMAT, URL_NULL_IMG } from '../constants/config';
import Transformer from '../constants/transformer';
import Pagination from '../components/pagination';
import Link from 'next/link';

var moment = require('moment');
const fetcher = url => fetch(url).then(r => r.json());
function SearchList(props) {
    const { query, title } = props;
    const [searchs, setSearchs] = useState([]);
    const [totalRows, setTotalRows] = useState(0);

    const [search, setSearch] = useState({
        keyword: '',
        page: 1,
        pageSize: 5
    });

    useEffect(() => {
        setSearch({ ...search, page: 1, keyword: title });
    }, [title]
    );

    const dateFormat = DATE_FORMAT.find(item => item.languageId === 'vi-VN');
    const { data, error } = userSWR(`${API_URL}/Websites/search-all/${TENANT_ID}/${WEBSITE_ID}/vi-VN?${Transformer.convertToPramsURI(search)}`, fetcher);

    useEffect(() => {
        if (data !== undefined && data.data !== null) {
            setSearchs(data.data);
            setTotalRows(data.totalRows);
        }
    });

    return (
        <div className="news_page">
            <div className="cate_filter py-md-5 py-3">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="fs-20 ff-montbold cl-blue text-uppercase" data-aos="fade-left">KẾT QUẢ CHO TỪ KHÓA "{query.keyword}"</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="filter_list py-md-5" id="search_list">
                {
                    searchs.length > 0 &&
                    searchs.map((item, index) => {
                        let type = Transformer.parseJson(MENU_OBJECT_TYPES[item.type].name);
                        let url = '';
                        let title = '';
                        let description = '';
                        {
                            Transformer.IsJsonString(item.title) === true
                                ? title = Transformer.truncate(Transformer.parseJson(item.title), 200)
                                : title = Transformer.truncate(item.title, 200);
                        }
                        {
                            Transformer.IsJsonString(item.description) === true
                                ? description = Transformer.truncate(Transformer.parseJson(item.description), 200)
                                : description = Transformer.truncate(item.description, 200);
                        }

                        if (item.type === 1) {
                            url = '/tin-tuc';
                        } else if (item.type === 2) {
                            url = `/${item.seoLink}`;
                            type = Transformer.parseJson(NEWS_TYPES[item.newsType].name);
                        } else if (item.type === 3) {
                            url = `/san-pham?productsCategoryId=${item.id}`;
                        } else if (item.type === 4) {
                            url = `/${item.seoLink}`;
                        } else if (item.type === 5) {
                            url = `/${item.seoLink}`;
                        } else if (item.type === 6) {
                            url = '/cam-nhan-khach-hang';
                        } else if (item.type === 7) {
                            url = '/tin-tuc-detail';
                        } else if (item.type === 8) {
                            url = '/tin-tuc-detail';
                        }

                        return (
                            <div className="item_post" data-aos="fade-up" key={index}>
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-1" >
                                            <div className="cate_name py-md-4 py-2">
                                                {type}
                                            </div>
                                        </div>
                                        <div className="col-md-8">
                                            <div className="py-md-4">
                                                <div className="row">
                                                    <div className="col-md-6 order-md-1">
                                                        <h4>
                                                            <Link href={url}>
                                                                <a>
                                                                    {title}
                                                                </a>
                                                            </Link>
                                                        </h4>
                                                    </div>
                                                    {
                                                        item.newsType === 1
                                                            ? <React.Fragment>
                                                                <div className="col-md-6 order-md-3">
                                                                    <div className="date_post"><strong>T: </strong>{moment(item.startTime).format(dateFormat.format)}</div>
                                                                </div>
                                                                <div className="col-md-6 order-md-4">
                                                                    <div className="address_post mb-3"><strong>A: </strong>{Transformer.truncate(item.note, 200)}</div>
                                                                </div>
                                                            </React.Fragment>
                                                            : <React.Fragment>
                                                                <div className="col-md-6 order-md-3"></div>
                                                                <div className="col-md-6 order-md-4">
                                                                    <div className="address_post mb-3">{item.note}</div>
                                                                </div>
                                                            </React.Fragment>
                                                    }
                                                    <div className="col-md-6 order-md-2">
                                                        <p dangerouslySetInnerHTML={Transformer.createHTML(description)}></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div className="col-md-3 thumbnail_container">
                                            <div className="thumbnail_post">
                                                <img src={item.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.images}`} className="img-fluid lazyload" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })
                }
                {
                    totalRows > 5 &&
                    <Pagination
                        position={'end'}
                        totalRows={totalRows}
                        search={search}
                        onChangePage={(pager) => setSearch({
                            ...search,
                            page: pager.currentPage
                        })} />
                }
            </div>
        </div>
    );
}

export async function getServerSideProps(props) {
    const title = Transformer.stripVietnameseChars(props.query.keyword);
    const searchs = await api.get('Websites/search-all', 'vi-VN', { page: 1, pageSize: 5, ...title });
    return {
        props: {
            searchs: searchs.data,
            totalRows: searchs.totalRows,
            query: props.query,
            title
        }
    };
}

export default SearchList
    ;
