import React from 'react'
import DeliveryForm from '../components/checkout/delivery_form'
import ItemsList from '../components/checkout/items_list'
import Breadcrumb from '../components/breadcrumb'
import { CONSTANT_TEXT, OBJECT_TYPE } from '../constants/config'

export default function Payment(props) {
    const { activeLang } = props
    return (
        <div>
            <Breadcrumb type={[OBJECT_TYPE.PRODUCT]} name={[CONSTANT_TEXT.YOUR_CART[activeLang], CONSTANT_TEXT.CHECKOUT[activeLang]]} seoLinks={['/gio-hang']} />
            <div className="product_checkout py-md-5 py-3">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-4">
                            <div className="fs-30 cl-blue text-uppercase">{CONSTANT_TEXT.CHECKOUT[activeLang]}</div>
                        </div>
                    </div>
                </div>
                <hr />
                <div className="container">
                    <div className="row">
                        <ItemsList activeLang={activeLang} />
                        <DeliveryForm activeLang={activeLang} />
                    </div>
                </div>
            </div>
        </div>
    )
}
