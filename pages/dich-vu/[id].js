import Title from '../../components/service/title';
import Cover from '../../components/service/cover';
import Introduction from '../../components/service/introduction';
import Effects from '../../components/service/effects';
import Section4 from '../../components/service/section4';
import Section5 from '../../components/service/section5';
import Section6 from '../../components/service/section6';
import ServiceFAQ from '../../components/service/faq';
import Services from '../../components/service/services';
import Breadcrumb from '../../components/breadcrumb';
import api from '../../constants/api';
import Transformer from '../../constants/transformer';
import Head from 'next/head';
import PropTypes from 'prop-types';

function Service(props) {
    const { serviceDetail, serviceMenuTree, activeLang } = props;
    const serviceName = Transformer.removeBreakTag(Transformer.parseJson(serviceDetail.title, activeLang));

    return (
        <div>
            <Head>
                <title>{Transformer.parseJson(serviceDetail.metaTitle, activeLang)}</title>
                <meta name="description" content={Transformer.parseJson(serviceDetail.metaDescription, activeLang)}></meta>
                <meta name="keywords" content={Transformer.parseJson(serviceDetail.metaKeyword, activeLang)}></meta>
            </Head>

            <Breadcrumb type={[5]} name={[serviceName]} activeLang={activeLang} />
            <Title serviceDetail={serviceDetail} activeLang={activeLang} ></Title>
            <Cover serviceDetail={serviceDetail} activeLang={activeLang} ></Cover>
            <Introduction serviceTitle={serviceDetail.serviceTitle.find(item => item.order === 0)} activeLang={activeLang} />
            <Effects serviceTitle={serviceDetail.serviceTitle.find(item => item.order === 1)} activeLang={activeLang} />
            <Section4 serviceTitle={serviceDetail.serviceTitle.find(item => item.order === 2)} activeLang={activeLang} />
            <Section5 serviceTitle={serviceDetail.serviceTitle.find(item => item.order === 3)} activeLang={activeLang} />
            <Section6 serviceTitle={serviceDetail.serviceTitle.find(item => item.order === 4)} activeLang={activeLang} />
            <ServiceFAQ serviceFaq={serviceDetail.serviceFaq} activeLang={activeLang} />
            <Services serviceMenuTree={serviceMenuTree} serviceDetail={serviceDetail} activeLang={activeLang} />
        </div>
    );
}

export async function getServerSideProps(props) {
    const id = props.query.id ? props.query.id : null;
    const serviceDetail = await api.get('Services', id);
    const serviceMenuTree = await api.get('Menus/detail', 'center');
    return {
        props: {
            serviceDetail: serviceDetail.data,
            serviceMenuTree: serviceMenuTree.data
        }
    };
}

Service.propTypes = {
    serviceDetail: PropTypes.any,
    serviceMenuTree: PropTypes.any,
    activeLang: PropTypes.any
};

export default Service;
