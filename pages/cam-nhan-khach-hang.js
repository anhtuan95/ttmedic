import React, { useState, useEffect } from 'react';
import userSWR from 'swr';
import fetch from 'unfetch';
import Feedback from '../components/customerFeedback/feedback';
import Title from '../components/customerFeedback/title';
import Pagination from '../components/pagination';
import { API_URL, TENANT_ID, WEBSITE_ID, LANGUAGEIDS } from '../constants/config';
import Transformer from '../constants/transformer';
import api from '../constants/api';
import PropTypes from 'prop-types';
import Head from 'next/head';

const fetcher = url => fetch(url).then(r => r.json());

function CustomerFeedbacks(props) {
    const { title, activeLang, setting } = props;
    const [feedbacks, setFeedbacks] = useState([]);
    const [totalRows, setTotalRows] = useState(0);
    const [search, setSearch] = useState({
        page: 1,
        pageSize: 3,
        type: ''
    });

    const { data } = userSWR(`${API_URL}/CustomerFeedbacks/${TENANT_ID}/${WEBSITE_ID}/?${Transformer.convertToPramsURI(search)}`, fetcher);

    useEffect(() => {
        if (data !== undefined) {
            setFeedbacks(data.data);
            setTotalRows(data.totalRows);
        }
    });

    return (
        <div>
            <Head>
                <title>{Transformer.parseJson(setting.metaTitle, activeLang)}</title>
                <meta name="description" content={Transformer.parseJson(setting.metaDescription, activeLang)} />
                <meta name="keywords" content={Transformer.parseJson(setting.metaKeyword, activeLang)} />
            </Head>

            <div className="review_page py-md-5 py-3">
                <div className="container">
                    <Title title={title} activeLang={activeLang} />
                    <Feedback feedbacks={feedbacks} activeLang={activeLang} />
                </div>
                {
                    totalRows > 3 &&
                    <Pagination
                        position={'center'}
                        totalRows={totalRows}
                        search={search}
                        onChangePage={(pager) => setSearch({
                            ...search,
                            page: pager.currentPage
                        })} />
                }
            </div>
        </div>
    );
}

export async function getStaticProps({ locale }) {
    const setting = await api.get('Websites/setting', null);
    const titleHC = await api.get('HardContents/detail-by-code', 'CAM-NHAN-KHACH-HANG');

    return {
        props: {
            setting,
            title: titleHC.data,
            activeLang: LANGUAGEIDS[[locale]]
        }
    };
}

CustomerFeedbacks.propTypes = {
    setting: PropTypes.any,
    title: PropTypes.any,
    activeLang: PropTypes.any
};

export default CustomerFeedbacks;
