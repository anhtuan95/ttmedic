import React from 'react';
import App from 'next/app';
import Layout from '../components/layout';
import api from '../constants/api';
import AOS from 'aos';
import { API_URL, TENANT_ID, WEBSITE_ID, LANGUAGEIDS } from '../constants/config';
import Transformer from '../constants/transformer';

export default class MyApp extends App {
    constructor(props) {
        super(props);

        this.state = {
            activeLang: null,
            totalQuantity: 0,
            idPath: '',
            metas: []
        };
    }

    static async getInitialProps({ Component, router, ctx }) {
        let pageProps = {};
        const { locale } = router;

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }

        const languages = await api.get('Websites', 'languages');
        const setting = await api.get('Websites/setting', null);
        const branchContacts = await api.get('BranchContacts', '1');
        const menuBottom = await api.get('Menus', 'bottom');
        const menuTop = await api.get('Menus/detail', 'top');
        const socialNetworks = await api.get('SocialNetworks', null);
        const menuTopItems = await api.get('Menus', 'top');
        const allBranchContacts = await api.get('BranchContacts', null);

        return {
            pageProps,
            menuTopItems: menuTopItems[0].menuItems,
            layoutProps: {
                languages,
                setting,
                branchContacts,
                allBranchContacts,
                menuBottom,
                menuTop,
                socialNetworks,
                activeLang: LANGUAGEIDS[[locale]]
            }
        };
    }

    componentDidMount() {
        const cart = localStorage.getItem('cart');

        if (cart) {
            const jsonCart = JSON.parse(cart);
            // this.getCartTotalQuantity(jsonCart);
        }

        AOS.init();
    }

    updateMetaTags = (data) => {
        const { activeLang } = this.state;
        data.forEach(item => {
            item.value = Transformer.IsJsonString(item.value) ? Transformer.parseJson(item.value, activeLang) : item.value;
        });

        this.setState({
            metas: [...data]
        });
    }

    getCartTotalQuantity = async (cart) => {
        const { activeLang } = this.state;
        const query = Transformer.generateQuery(cart);
        let totalQuantity = 0;

        const res = await fetch(`${API_URL}Products/by-local/${TENANT_ID}/${WEBSITE_ID}/${activeLang}?${query}`, { 'Accept-Language': 'vi-VN' });
        const resJson = await res.json();
        resJson.data.map(item => {
            cart.items.map(cItem => {
                if (item.productId === cItem.productId) {
                    totalQuantity += cItem.quantity;
                }
            });
        });

        this.setState({
            totalQuantity
        });
    }

    updateCartQuantity = () => {
        const cart = JSON.parse(localStorage.getItem('cart'));

        this.setState({
            totalQuantity: cart.totalQuantity
        });
    }

    setActiveMenuItem = (objectId) => {
        this.props.menuTopItems.map(item => {
            if (item.objectId === objectId) {
                this.setState({
                    idPath: item.idPath
                });
            }
        });
    }

    render() {
        const { Component, pageProps, router, layoutProps } = this.props;
        const { totalQuantity, idPath, metas } = this.state;

        return (
            <Layout
                routerName={router.route}
                {...layoutProps}
                metas={metas}
                updateMetaTags={this.updateMetaTags}
                totalQuantity={totalQuantity} idPath={idPath}>
                <Component {...pageProps}
                    updateCartQuantity={this.updateCartQuantity}
                    setActiveMenuItem={this.setActiveMenuItem}
                    allBranchContacts={layoutProps.branchContactsAll}
                    updateMetaTags={this.updateMetaTags} />

            </Layout>
        );
    }
}
