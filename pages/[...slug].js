import React from 'react';
import api from '../constants/api';
import ProductDetail from './san-pham-detail/[id]';
import NewsDetail from './tin-tuc-detail/[id]';
import ServiceDetail from './dich-vu/[id]';
import Products from './san-pham/[id]';
import NewsCategory from './tin-tuc-detail/tin-tuc';
import { OBJECT_TYPE, LANGUAGEIDS } from '../constants/config';
import PropTypes from 'prop-types';

const SeoLink = ({
    objectType, activeLang, updateCartQuantity, setActiveMenuItem, updateMetaTags,
    productDetail, relatedProducts, newsDetail, relatedNews, socialNetworks, serviceMenuTree, settings,
    serviceDetail, productMenuTree, products, productSlide, productMenu, productCategory, highLightEvents
}) => {
    return (
        <div>
            {objectType === OBJECT_TYPE.PRODUCT &&
                <ProductDetail productDetail={productDetail} relatedProducts={relatedProducts} activeLang={activeLang} updateCartQuantity={updateCartQuantity} />}

            {objectType === OBJECT_TYPE.NEWS &&
                <NewsDetail post={newsDetail} relatedPosts={relatedNews} socialNetworks={socialNetworks} activeLang={activeLang} updateCartQuantity={updateCartQuantity} />}

            {objectType === OBJECT_TYPE.SERVICE &&
                <ServiceDetail serviceDetail={serviceDetail} serviceMenuTree={serviceMenuTree} activeLang={activeLang} updateCartQuantity={updateCartQuantity} updateMetaTags={updateMetaTags} />}

            {objectType === OBJECT_TYPE.CATEGORY_PRODUCT &&
                <Products
                    products={products} productMenuTree={productMenuTree} productSlide={productSlide} productMenu={productMenu} productCategory={productCategory}
                    activeLang={activeLang} updateCartQuantity={updateCartQuantity} settings={settings} />}

            {objectType === OBJECT_TYPE.CATEGORY_NEWS &&
                <NewsCategory highLightEvents={highLightEvents} settings={settings} activeLang={activeLang} />
            }
        </div>
    );
};

export async function getServerSideProps({ params, locale }) {
    const activeLang = LANGUAGEIDS[[locale]];
    const seolink = params.slug[0];
    let returnProps = {
        objectType: '',
        productDetail: {},
        relatedProducts: {},
        newsDetail: {},
        relatedNews: {},
        socialNetworks: {},
        highLightEvents: {},
        serviceMenuTree: {},
        settings: {},
        serviceDetail: {},
        products: {},
        productMenuTree: {},
        productSlide: {}
    };
    let updateProps = {};

    const menuSeolink = await api.get('Menus/item/by-seolink', null, { seolink });
    if (seolink === 'san-pham') {
        updateProps = await getSeoLinkData({ data: { objectType: OBJECT_TYPE.CATEGORY_PRODUCT, objectId: '' } }, activeLang);
    }

    if (seolink === 'tin-tuc') {
        updateProps = await getSeoLinkData({ data: { objectType: OBJECT_TYPE.CATEGORY_NEWS, objectId: '' } }, activeLang);
    }

    if (menuSeolink.data === null) {
        const websiteSeolink = await api.get('Websites/router-by-seolink', null, { seolink });

        if (websiteSeolink.data !== null) {
            // setActiveMenuItem(websiteSeolink.data.objectId);
            updateProps = await getSeoLinkData(websiteSeolink, activeLang);
        }
    } else {
        // setActiveMenuItem(menuSeolink.data.objectId);
        updateProps = await getSeoLinkData(menuSeolink, activeLang);
    }

    returnProps = { ...returnProps, ...updateProps };

    return {
        props: {
            activeLang,
            ...returnProps
        }
    };
}

const getSeoLinkData = async (websiteSeolink, activeLang) => {
    switch (websiteSeolink.data.objectType) {
        case OBJECT_TYPE.PRODUCT: {
            const productDetail = await api.get('Products/detail', `${websiteSeolink.data.objectId}/${activeLang}`);
            const relatedProducts = await api.get('Products/related', `${websiteSeolink.data.objectId}/${activeLang}`);

            return {
                objectType: OBJECT_TYPE.PRODUCT,
                productDetail: productDetail.data,
                relatedProducts: relatedProducts.data
            };
        }

        case OBJECT_TYPE.CATEGORY_NEWS: {
            const data = await api.get('News/hot', `${activeLang}/1/5`);
            const settings = await api.get('Websites/setting', null);
            return {
                objectType: OBJECT_TYPE.CATEGORY_NEWS,
                highLightEvents: data,
                settings: settings
            };
        }

        case OBJECT_TYPE.NEWS: {
            const newsDetail = await api.get('News/detail', `${websiteSeolink.data.objectId}/${activeLang}`);
            const relatedNews = await api.get('News/related', `${websiteSeolink.data.objectId}/${activeLang}/5`);
            const socialNetworks = await api.get('SocialNetworks', null);

            return {
                objectType: OBJECT_TYPE.NEWS,
                newsDetail: newsDetail.data,
                relatedNews: relatedNews,
                socialNetworks: socialNetworks
            };
        }

        case OBJECT_TYPE.SERVICE: {
            const serviceDetail = await api.get('Services', `${websiteSeolink.data.objectId}`);
            const serviceMenuTree = await api.get('Menus/detail', 'center');

            return {
                objectType: OBJECT_TYPE.SERVICE,
                serviceMenuTree: serviceMenuTree.data,
                serviceDetail: serviceDetail.data
            };
        }

        case OBJECT_TYPE.CATEGORY_PRODUCT: {
            const products = await api.get('Products', activeLang, { page: 1, pageSize: 9, productsCategoryId: websiteSeolink.data.objectId });
            const productMenuTree = await api.get('Menus/detail', 'left');
            const productMenu = await api.get('Menus', 'left');
            const productSlide = await api.get('HardContents/detail-by-code', 'SLIDE-SAN-PHAM');
            const productCategory = websiteSeolink.data.objectId ? await api.get('ProductCategory', websiteSeolink.data.objectId) : { data: {} };
            const settings = await api.get('Websites/setting', null);

            return {
                objectType: OBJECT_TYPE.CATEGORY_PRODUCT,
                products: products,
                productMenuTree: productMenuTree.data,
                productSlide: productSlide.data,
                productMenu: productMenu[0],
                productCategory: productCategory.data,
                settings
            };
        }

        default:
            return {};
    }
};

SeoLink.propTypes = {
    objectType: PropTypes.any,
    activeLang: PropTypes.any,
    updateCartQuantity: PropTypes.any,
    setActiveMenuItem: PropTypes.any,
    updateMetaTags: PropTypes.any,
    productDetail: PropTypes.any,
    relatedProducts: PropTypes.any,
    newsDetail: PropTypes.any,
    relatedNews: PropTypes.any,
    socialNetworks: PropTypes.any,
    serviceMenuTree: PropTypes.any,
    serviceDetail: PropTypes.any,
    productMenuTree: PropTypes.any,
    products: PropTypes.any,
    productSlide: PropTypes.any,
    productMenu: PropTypes.any,
    productCategory: PropTypes.any,
    highLightEvents: PropTypes.any,
    settings: PropTypes.any
};

export default SeoLink;
