import BannerAbout from '../components/aboutpage/banner_about';
import Mission from '../components/aboutpage/mission';
import Certificate from '../components/aboutpage/certificate';
import Vision from '../components/aboutpage/vision';
import Value from '../components/aboutpage/value';
import Partners from '../components/aboutpage/partners';
import api from '../constants/api';
import { LANGUAGEIDS } from '../constants/config';
import PropTypes from 'prop-types';
import Head from 'next/head';
import Transformer from '../constants/transformer';

function AboutPage({ partners, values, visions, missions, staffs, introduces, activeLang, setting }) {
    return (
        <div>
            <Head>
                <title>{Transformer.parseJson(setting.metaTitle, activeLang)}</title>
                <meta name="description" content={Transformer.parseJson(setting.metaDescription, activeLang)} />
                <meta name="keywords" content={Transformer.parseJson(setting.metaKeyword, activeLang)} />
            </Head>

            <BannerAbout introduces={introduces} activeLang={activeLang} />
            <Mission missions={missions} activeLang={activeLang} />
            <Certificate missions={missions} activeLang={activeLang} />
            <Vision visions={visions} staffs={staffs} activeLang={activeLang} />
            <Value values={values} activeLang={activeLang} />
            <Partners partners={partners} activeLang={activeLang} />
        </div>
    );
}

export async function getStaticProps({ locale }) {
    const setting = await api.get('Websites/setting', null);
    const partners = await api.get('Partners', null);
    const values = await api.get('HardContents/detail-by-code', 'GIA-TRI');
    const visions = await api.get('HardContents/detail-by-code', 'TAM-NHIN');
    const missions = await api.get('HardContents/detail-by-code', 'SU-MENH');
    const staffs = await api.get('HardContents/detail-by-code', 'TAP-THE-NHAN-VIEN-JSPA');
    const introduces = await api.get('HardContents/detail-by-code', 'GIOI-THIEU');

    return {
        props: {
            setting,
            partners,
            values,
            visions,
            missions,
            staffs,
            introduces,
            activeLang: LANGUAGEIDS[[locale]]
        }
    };
}

AboutPage.propTypes = {
    setting: PropTypes.any,
    partners: PropTypes.any,
    values: PropTypes.any,
    visions: PropTypes.any,
    missions: PropTypes.any,
    staffs: PropTypes.any,
    introduces: PropTypes.any,
    activeLang: PropTypes.any
};

export default AboutPage;
