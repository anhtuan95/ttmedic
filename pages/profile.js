import React, { useEffect, useState } from 'react';
import { API_URL_TTMEDIC } from '../constants/config';
import userSWR from 'swr';
import fetch from 'unfetch';
// import Transformer from '../constants/transformer';
import PatientDetail from '../components/profile/patientDetail';
import PatientXRay from '../components/profile/patientXRay';
import { useRouter } from 'next/router';

function Profile() {
    const fetcher = url => fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`
        }
    }).then(r => r.json());

    const [patientDetail, setPatientDetail] = useState({});
    const [patientXRayImages, setPatientXRayImages] = useState([]);
    const [accessToken, setAccessToken] = useState(null);
    const dataPatient = userSWR(accessToken ? `${API_URL_TTMEDIC}/Benhnhan/detail` : null, fetcher);
    const dataXRay = userSWR((accessToken && patientXRayImages.length > 0) ? `${API_URL_TTMEDIC}/Benhnhan` : null, fetcher);
    const router = useRouter();

    useEffect(() => {
        const accessTokenLS = localStorage.getItem('access_token');
        const dataXRay = JSON.parse(localStorage.getItem('data_xray'));
        if (dataXRay && dataXRay.length > 0) {
            setPatientXRayImages(dataXRay);
        }
        if (accessTokenLS !== 'null') {
            setAccessToken(accessTokenLS);
        } else router.push('/');
    }, []);

    useEffect(() => {
        if (dataPatient.data !== undefined) {
            setPatientDetail(dataPatient.data);
            router.push({
                pathname: '/profile',
                query: { username: dataPatient.data.maBenhNhan }
            });
        }
        if (dataXRay.data !== undefined) {
            setPatientXRayImages(dataXRay.data);
        }
    }, [dataPatient.data, dataXRay.data]);

    return (
        <div className="page-container">
            {/* BEGIN CONTENT */}
            <div className="page-content-wrapper">
                {/* BEGIN CONTENT BODY */}
                <div className="page-content">
                    <div className="container">
                        <h3 className="page-title"> Thông tin bệnh nhân &nbsp;
                            <small>Thông tin bệnh nhân - Hình ảnh chụp chiếu</small>
                        </h3>
                        <div className="row">
                            <PatientDetail patientDetail={patientDetail} />
                            <PatientXRay patientXRayImages={patientXRayImages} />
                        </div>
                    </div>
                </div>
                {/* END CONTENT BODY */}
            </div>
            {/* END CONTENT */}
        </div>
    );
}

export default Profile;
