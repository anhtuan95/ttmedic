import React from 'react';
import Link from 'next/link';

export default function AfterCheckout() {
    return (
        <div className="after-checkout">
            <div className="dialog">
                <h3>Thank you for your purchase!</h3>
                <button>
                    <Link href="/index">
                        <a>Back to Home</a>
                    </Link>
                </button>
            </div>
        </div>
    );
}
