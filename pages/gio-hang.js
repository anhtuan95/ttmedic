import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import userSWR from 'swr';
import fetch from 'unfetch';
import { API_URL, TENANT_ID, WEBSITE_ID, FILE_URL, CONSTANT_TEXT, OBJECT_TYPE, URL_NULL_IMG } from '../constants/config';
import Transformer from '../constants/transformer';
import Breadcrumb from '../components/breadcrumb';

const fetcher = url => fetch(url).then(r => r.json());

function Cart({ activeLang, updateCartQuantity }) {
    const [cart, setCart] = useState({
        items: [],
        totalQuantity: 0,
        totalPrice: 0
    });

    const [searchQuery, setSearchQuery] = useState('');
    const [fetchingDone, setFetchingDone] = useState(false);
    const { data } = userSWR(searchQuery !== '' ? `${API_URL}Products/by-local/${TENANT_ID}/${WEBSITE_ID}/${activeLang}?${searchQuery}` : null, fetcher);

    useEffect(() => {
        let cart = localStorage.getItem('cart');

        if (cart) {
            cart = JSON.parse(cart);
            if (cart.items.length > 0) {
                setCart({ ...cart });
            }
        }
    }, []);

    useEffect(() => {
        const query = Transformer.generateQuery(cart);
        setSearchQuery(query);
    }, [cart]);

    useEffect(() => {
        if (data !== undefined) {
            const newCart = {
                items: [],
                totalQuantity: 0,
                totalPrice: 0
            };

            data.data.map(item => {
                cart.items.map(cartItem => {
                    if (item.productId === cartItem.productId) {
                        item.quantity = cartItem.quantity;
                        newCart.totalQuantity += cartItem.quantity;
                        newCart.totalPrice += item.salePrice * cartItem.quantity;
                        newCart.items.push(item);
                    }
                });
            });

            setCart({ ...newCart });
            localStorage.setItem('cart', JSON.stringify(newCart));
            updateCartQuantity();
            setFetchingDone(true);
        }
    }, [data]);

    const subtractItem = (id) => {
        const itemIndex = cart.items.findIndex(item => item.productId === id);
        if (itemIndex > -1) {
            if (cart.items[itemIndex].quantity - 1 === 0) {
                cart.totalQuantity -= cart.items[itemIndex].quantity;
                cart.totalPrice -= cart.items[itemIndex].salePrice * cart.items[itemIndex].quantity;
                cart.items.splice(itemIndex, 1);

                localStorage.setItem('cart', JSON.stringify(cart));
                setCart({ ...cart });
                updateCartQuantity();
                return;
            }

            cart.items[itemIndex].quantity -= 1;
            cart.totalQuantity -= 1;
            cart.totalPrice -= cart.items[itemIndex].salePrice;

            localStorage.setItem('cart', JSON.stringify(cart));
            setCart({ ...cart });
            updateCartQuantity();
        }
    };

    const addItem = (id) => {
        const itemIndex = cart.items.findIndex(item => item.productId === id);

        if (itemIndex > -1) {
            cart.items[itemIndex].quantity += 1;
            cart.totalQuantity += 1;
            cart.totalPrice += cart.items[itemIndex].salePrice;
        }

        localStorage.setItem('cart', JSON.stringify(cart));
        setCart({ ...cart });
        updateCartQuantity();
    };

    const removeItem = (e, id) => {
        e.preventDefault();
        const itemIndex = cart.items.findIndex(item => item.productId === id);

        if (itemIndex > -1) {
            cart.totalQuantity -= cart.items[itemIndex].quantity;
            cart.totalPrice -= cart.items[itemIndex].salePrice * cart.items[itemIndex].quantity;
            cart.items.splice(itemIndex, 1);
        }

        localStorage.setItem('cart', JSON.stringify(cart));
        setCart({ ...cart });
        updateCartQuantity();
    };

    return (
        <div>
            <Breadcrumb type={[OBJECT_TYPE.PRODUCT]} name={[CONSTANT_TEXT.YOUR_CART[activeLang]]} activeLang={activeLang} />
            <div className="product_cart py-md-5 py-3">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-6">
                            <div className="fs-30 cl-blue text-uppercase">{CONSTANT_TEXT.YOUR_CART[activeLang]}</div>
                        </div>
                        <div className="col-md-6">
                            <div className="text-right">
                                <Link href={'/san-pham'}>
                                    <a className="cl-blue ff-montbold">{CONSTANT_TEXT.CONTINUE_SHOPING[activeLang]}</a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="cart_info">
                    <div className="container">
                        {
                            cart.items.length === 0 &&
                            <div><h3>{CONSTANT_TEXT.EMPTY_CART[activeLang]}</h3></div>
                        }
                        <div className="table-responsive">

                            <table className="table table-borderless">
                                <tbody>
                                    {
                                        fetchingDone &&
                                        cart.items.map((item, index) => (
                                            <tr key={index}>
                                                <td>
                                                    <img src={item.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.images}`} className="img-fluid lazyload" alt=""></img>
                                                </td>
                                                <td style={{ height: '125px', wordBreak: 'break-word' }}>
                                                    <h3>
                                                        <Link href={item.seoLink}>
                                                            <a style={{ color: 'inherit' }}>{Transformer.truncate(item.title, 100)}</a>
                                                        </Link>
                                                    </h3>
                                                    <div>
                                                        <span className="price_single">{Transformer.convertCurrency(item.salePrice)}</span> <a href="#" onClick={(e) => removeItem(e, item.productId)}>{CONSTANT_TEXT.DELETE[activeLang]}</a>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className=" bd-lr px-3">
                                                        <div className="row">
                                                            <span className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 pl-0 pr-0">
                                                                <button
                                                                    type="button"
                                                                    className="btn btn-default"
                                                                    onClick={() => subtractItem(item.productId)}
                                                                    data-type="minus" data-field="quant[1]">
                                                                    <i className="fa fa-minus"></i>
                                                                </button>
                                                            </span>
                                                            <input type="text" className="form-control col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 text-center" value={item.quantity} style={{ backgroundColor: 'transparent' }} readOnly ></input>
                                                            <span className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 pl-0 pr-0">
                                                                <button
                                                                    type="button"
                                                                    className="btn btn-default"
                                                                    data-type="plus" onClick={() => addItem(item.productId)}
                                                                    data-field="quant[1]">
                                                                    <i className="fa fa-plus"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td >
                                                    <div className="price_total" style={{ width: '100px' }}>{Transformer.convertCurrency(item.salePrice * item.quantity)}</div>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {
                    cart.items.length > 0 &&
                    <div className="container">
                        <div className="row justify-content-end">
                            <div className="col-md-5">
                                <div className="box_total">
                                    <table className="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>{CONSTANT_TEXT.TEMPORARY[activeLang]}</td>
                                                <td><div className="price_total">{Transformer.convertCurrency(cart.totalPrice)}</div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div className="text-right">
                                        <Link href={'/thanh-toan'}>
                                            <a className="btn btn-gold"><span>{CONSTANT_TEXT.CHECKOUT[activeLang]}</span></a>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    );
}

export default Cart
    ;
