import React from 'react';
import BannerTopService from '../components/service/banner_top';
import ListService from '../components/service/list_service';
import api from '../constants/api';
import PropTypes from 'prop-types';
import { LANGUAGEIDS } from '../constants/config';
import Transformer from '../constants/transformer';
import Head from 'next/head';
function ServicePage(props) {
    const { serviceBanner, activeLang, serviceExperience, menuTop, settings } = props;
    return (
        <React.Fragment>
            <Head>
                <title>{Transformer.parseJson(settings.metaTitle, activeLang)}</title>
                <meta name="description" content={Transformer.parseJson(settings.metaDescription, activeLang)} />
                <meta name="keywords" content={Transformer.parseJson(settings.metaKeyword, activeLang)} />
            </Head>
            <BannerTopService serviceBanner={serviceBanner} activeLang={activeLang} />
            <ListService serviceExperience={serviceExperience} activeLang={activeLang} menuTop={menuTop.menuItems[2]} />
        </React.Fragment>
    );
}

export async function getStaticProps({ locale }) {
    const serviceBanner = await api.get('HardContents/detail-by-code', 'DICH-VU');
    const serviceExperience = await api.get('HardContents/detail-by-code', 'EXPERIENCE-SERVICES');
    const menuTop = await api.get('Menus/detail', 'top');
    const settings = await api.get('Websites/setting', null);
    return {
        props: {
            serviceBanner: serviceBanner.data,
            serviceExperience: serviceExperience.data,
            menuTop: menuTop.data,
            settings,
            activeLang: LANGUAGEIDS[[locale]]
        }
    };
}

ServicePage.propTypes = {
    serviceBanner: PropTypes.any,
    activeLang: PropTypes.any,
    settings: PropTypes.object,
    serviceExperience: PropTypes.any,
    menuTop: PropTypes.any
};

export default ServicePage;
