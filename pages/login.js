import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import api from '../constants/api';
import Transformer from '../constants/transformer';
import * as _ from 'lodash';
import { API_URL_TTMEDIC } from '../constants/config';
import userSWR from 'swr';
import fetch from 'unfetch';

export default function Login() {
    const fetcher = url => fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`
        }
    }).then(r => r.json());

    const [showLoginForm, setShowLoginForm] = useState(true);
    const [accessToken, setAccessToken] = useState(null);
    const [username, setUsername] = useState('');
    const router = useRouter();
    // const appSettings = userSWR(accessToken ? `${CORE_API_URL_TTMEDIC}AppSettings` : null, fetcher);
    const dataXRay = userSWR(accessToken ? `${API_URL_TTMEDIC}/Benhnhan` : null, fetcher);
    const toggleShowLoginForm = () => {
        setShowLoginForm(!showLoginForm);
    };

    useEffect(() => {
        console.log('router', router);
        if (router.query.username !== undefined) {
            setUsername(router.query.username);
        }
    }, []);

    useEffect(() => {
        if (dataXRay.data !== undefined) {
            const storageXRay = JSON.stringify(dataXRay.data);
            localStorage.setItem('data_xray', storageXRay);
            router.push('/profile');
        }
    }, [dataXRay.data]);

    const submitLogin = async () => {
        const username = document.getElementById('username').value;
        const password = document.getElementById('password').value;
        if (username === '' || password === '') {
            alert('Please enter username and password');
        } else {
            const token = await api.login({ username, password });
            const data = Transformer.fetch(token);
            if (_.has(data, 'accessToken')) {
                localStorage.setItem('access_token', data.accessToken);
                setAccessToken(data.accessToken);
            } else if (_.has(data, 'error')) {
                const errMessage = data.errorDescription.replaceAll('_', ' ');
                alert(errMessage);
            }
        }
    };

    const handleOnChange = (e) => {
        setUsername(e.target.value);
    };

    return (
        <div className="user-login-5">
            <div className="row bs-reset align-items-center ">
                <div className="col-md-6 bs-reset">
                    <div className="login-bg" style={{ backgroundImage: 'url("assets/images/login/bg1.jpg")' }}>
                        <img className="login-logo" src="/assets/images/logo-full.png" />
                    </div>
                </div>
                <div className="col-md-6 login-container bs-reset">
                    <div className="login-content">
                        <img src="assets/images/logo.png" />
                        <h1>Đăng nhập</h1>
                        {
                            showLoginForm &&
                            <form action="javascript:;" className="login-form" method="post">
                                <div className="row">
                                    <div className="col-xs-6">
                                        <label>Tên đăng nhập (Mã bệnh nhân)</label>
                                        <input
                                            className="form-control form-control-solid placeholder-no-fix"
                                            onChange={handleOnChange}
                                            value={username || ''}
                                            type="text"
                                            autoComplete="on"
                                            placeholder="Tên đăng nhập..." name="username" id="username" required />
                                    </div>
                                    <div className="col-xs-6">
                                        <label>Mật khẩu</label>
                                        <input className="form-control form-control-solid placeholder-no-fix" type="password" autoComplete="off" placeholder="Mật khẩu..." name="password" id="password" required />
                                    </div>
                                </div>
                                <div className="alert alert-danger display-hide">
                                    <button className="close" data-close="alert" />
                                    <span>Vui lòng nhập tài khoản và mật khẩu. </span>
                                </div>
                                <div className="row">
                                    <div className="col-sm-4">
                                        {/* <div className="rem-password">
                                            <p>Nhớ mật khẩu
                                                <div className='checker'>
                                                    <input type="checkbox" className="rem-checkbox" />
                                                </div>
                                            </p>
                                        </div> */}
                                    </div>
                                    <div className="col-sm-8 text-right">
                                        {/* <div className="forgot-password">
                                            <p className="forget-password" onClick={toggleShowLoginForm}>Quên mật khẩu?</p>
                                        </div> */}
                                        <button className="btn btn-circle yellow-crusta" type="submit" onClick={submitLogin}>Đăng nhập</button>
                                    </div>
                                </div>
                            </form>
                        }

                        {/* BEGIN FORGOT PASSWORD FORM */}
                        {
                            !showLoginForm &&
                            <form className="forget-form" action="javascript:;" method="post">
                                <h3 className="font-green">Quên mật khẩu ?</h3>
                                <p> Enter your e-mail address below to reset your password. </p>
                                <div className="form-group">
                                    <input className="form-control placeholder-no-fix" type="text" autoComplete="off" placeholder="Email" name="email" />
                                </div>
                                <div className="form-actions">
                                    <button type="button" id="back-btn" className="btn grey btn-default" onClick={toggleShowLoginForm} >Trở lại</button>
                                    <button type="submit" className="btn blue btn-success uppercase pull-right">Đồng ý</button>
                                </div>
                            </form>
                        }

                        {/* END FORGOT PASSWORD FORM */}
                    </div>
                    {/* <div className="login-footer">
                        <div className="row bs-reset">
                            <div className="col-xs-4 bs-reset">
                                <ul className="login-social">
                                    <li>
                                        <a href="javascript:;">
                                            <i className="icon-social-facebook" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i className="icon-social-twitter" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i className="icon-social-dribbble" />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-xs-8 bs-reset">
                                <div className="login-copyright text-right">
                                    <p>Copyright © Thaithinhmedic 2021</p>
                                </div>
                            </div>
                        </div>
                    </div> */}
                </div>
            </div>
        </div>
    );
}
