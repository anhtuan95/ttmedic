import FormContact from '../components/contact/form_contact';
import LocationSpa from '../components/contact/location_spa';
import MoreInfo from '../components/contact/more_info';
import api from '../constants/api';
import { LANGUAGEIDS } from '../constants/config';
import PropTypes from 'prop-types';
import Head from 'next/head';
import Transformer from '../constants/transformer';

function Contact(props) {
    const { socialNetworks, contactHC, contactJSpaHC, branchContacts, moreInfoHC, activeLang, setting } = props;

    return (
        <div>
            <Head>
                <title>{Transformer.parseJson(setting.metaTitle, activeLang)}</title>
                <meta name="description" content={Transformer.parseJson(setting.metaDescription, activeLang)} />
                <meta name="keywords" content={Transformer.parseJson(setting.metaKeyword, activeLang)} />
            </Head>

            <div className="contact_page pb-md-5">
                <FormContact socialNetworks={socialNetworks} contactHC={contactHC} contactJSpaHC={contactJSpaHC} activeLang={activeLang} />
                <LocationSpa branchContacts={branchContacts} activeLang={activeLang} />
                <MoreInfo moreInfoHC={moreInfoHC} activeLang={activeLang} />
            </div>
        </div>
    );
}

export async function getStaticProps({ locale }) {
    const setting = await api.get('Websites/setting', null);
    const socialNetworks = await api.get('SocialNetworks', null);
    const contactHC = await api.get('HardContents/detail-by-code', 'LIEN-HE');
    const contactJSpaHC = await api.get('HardContents/detail-by-code', 'CHUOI-HE-THONG-JSPA');
    const branchContacts = await api.get('BranchContacts', null);
    const moreInfoHC = await api.get('HardContents/detail-by-code', 'TRANG-THIET-BI');

    return {
        props: {
            setting,
            socialNetworks,
            contactHC: contactHC.data,
            contactJSpaHC: contactJSpaHC.data,
            branchContacts,
            moreInfoHC: moreInfoHC.data,
            activeLang: LANGUAGEIDS[[locale]]
        }
    };
}

Contact.propTypes = {
    setting: PropTypes.any,
    socialNetworks: PropTypes.any,
    contactHC: PropTypes.any,
    contactJSpaHC: PropTypes.any,
    branchContacts: PropTypes.any,
    moreInfoHC: PropTypes.any,
    activeLang: PropTypes.any
};

export default Contact;
