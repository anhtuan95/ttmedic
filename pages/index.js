import PropTypes from 'prop-types';

function Home(props) {
    // const {
    //     locale, banners, services, ourCustomers, updateCartQuantity,
    //     ourCustomersHC, mainServicesHC, productListHC, aboutUsHC, setting
    // } = props;
    // useEffect(() => {
    //     setTimeout(function () {
    //         AOS.init();
    //         AOS.refresh();
    //     }, 1000);
    // });

    return (
        <div>
            <div style={{ width: '50%', margin: '10px auto' }}>

            </div>

            {/* <Head>
                <title>{Transformer.parseJson(setting.metaTitle, locale)}</title>
                <meta name="description" content={Transformer.parseJson(setting.metaDescription, locale)} />
                <meta name="keywords" content={Transformer.parseJson(setting.metaKeyword, locale)} />
            </Head>

            <BannerSite banner={banners[0]} activeLang={locale} />

            <MainService menu={services} mainServicesHC={mainServicesHC} activeLang={locale} />

            <ProductList productListHC={productListHC} activeLang={locale} updateCartQuantity={updateCartQuantity} />

            <AboutUs aboutUsHC={aboutUsHC} activeLang={locale} />

            <OurStory ourCustomersHC={ourCustomersHC} activeLang={locale} />

            <OurCustomer ourCustomers={ourCustomers} activeLang={locale} />

            <ContactUs activeLang={locale} /> */}
        </div>
    );
}

export async function getStaticProps({ locale }) {
    // const setting = await api.get('Websites/setting', null);
    // const banners = await api.get('Banners', 'top');
    // const services = await api.get('Menus/detail', 'center');
    // const mainServicesHC = await api.get('HardContents/detail-by-code', 'DICH-VU-CHINH');
    // const ourCustomers = await api.get('CustomerFeedbacks/get-all', '3');
    // const ourCustomersHC = await api.get('HardContents/detail-by-code', 'CAU-CHUYEN-KHACH-HANG');
    // const productListHC = await api.get('HardContents/detail-by-code', 'LAM-DEP-CUNG-JSPA');
    // const aboutUsHC = await api.get('HardContents/detail-by-code', 'VE-CHUNG-TOI');
    // const branchContacts = await api.get('BranchContacts', null);

    return {
        props: {
            // setting,
            // banners,
            // services: services.data,
            // ourCustomers,
            // ourCustomersHC: ourCustomersHC.data,
            // mainServicesHC: mainServicesHC.data,
            // productListHC: productListHC.data,
            // aboutUsHC: aboutUsHC.data,
            // branchContacts,
            // locale: LANGUAGEIDS[[locale]]
        }
    };
}

Home.propTypes = {
    setting: PropTypes.any,
    locale: PropTypes.string,
    banners: PropTypes.any,
    services: PropTypes.any,
    ourCustomers: PropTypes.any,
    updateCartQuantity: PropTypes.any,
    ourCustomersHC: PropTypes.any,
    mainServicesHC: PropTypes.any,
    productListHC: PropTypes.any,
    aboutUsHC: PropTypes.any
};

export default Home;
