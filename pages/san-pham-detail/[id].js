import ThumbnailSlider from '../../components/product/thumbnail_slide';
import InfoDetail from '../../components/product/info_detail';
import ProductTabs from '../../components/product/product_tabs';
import ProductTabsContent from '../../components/product/product_tabs_content';
import RelatedProducts from '../../components/product/related_products';
import Breadcrumb from '../../components/breadcrumb';
import api from '../../constants/api';
import Head from 'next/head';
import { OBJECT_TYPE } from '../../constants/config';

function ProductDetail(props) {
    const { productDetail, relatedProducts, activeLang, updateCartQuantity } = props;

    if (productDetail) {
        return (
            <div>
                <Head>
                    <title>{productDetail.metaTitle}</title>
                    <meta name="description" content={productDetail.metaDescription}></meta>
                    <meta name="keywords" content={productDetail.metaKeyword}></meta>
                </Head>
                <Breadcrumb type={[OBJECT_TYPE.PRODUCT]} name={[productDetail.title]} activeLang={activeLang} />
                <div className="product_detail">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-5">
                                <ThumbnailSlider productImages={productDetail.productImages} />
                            </div>
                            <div className="col-md-7">
                                <InfoDetail productDetail={productDetail} updateCartQuantity={updateCartQuantity} activeLang={activeLang} />
                            </div>
                        </div>
                    </div>
                    <ProductTabs activeLang={activeLang} />
                    <div className="container">
                        <ProductTabsContent productDetail={productDetail} />
                        {
                            relatedProducts.length > 0 &&
                            <RelatedProducts relatedProducts={relatedProducts} updateCartQuantity={updateCartQuantity} activeLang={activeLang} />
                        }
                    </div>
                </div>
            </div>

        );
    } else {
        return (
            <div>
                <h1 style={{ textAlign: 'center' }}>NO product found</h1>
            </div>
        );
    }
}

export async function getServerSideProps(props) {
    const id = props.query.id ? props.query.id : null;
    const productDetail = await api.get('Products/detail', `${id}/vi-VN`);
    const relatedProducts = await api.get('Products/related', `${id}/vi-VN`);

    return {
        props: {
            productDetail: productDetail.data,
            relatedProducts: relatedProducts.data
        }
    };
}

export default ProductDetail;
