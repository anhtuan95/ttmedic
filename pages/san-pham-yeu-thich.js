import React, { useState, useEffect } from 'react';
import userSWR from 'swr';
import fetch from 'unfetch';
import { API_URL, TENANT_ID, WEBSITE_ID, CONSTANT_TEXT } from '../constants/config';
import api from '../constants/api';
import SliderBanner from '../components/product/slider_banner';
import SideMenu from '../components/product/side_menu';
import ProductList from '../components/product/product_list';
import Pagination from '../components/pagination';
import Transformer from '../constants/transformer';

const fetcher = url => fetch(url).then(r => r.json());

function ProductFavorite(props) {
    const { activeLang, productSlide, productMenuTree, productMenu, categoryId, updateCartQuantity } = props;
    const [products, setProducts] = useState([]);
    const [totalRows, setTotalRows] = useState(0);
    const [search, setSearch] = useState({
        page: 1,
        pageSize: 9,
        productsCategoryId: null,
        favorite: []

    });

    useEffect(() => {
        const { categoryId } = props;
        let favorite = localStorage.getItem('favorite');
        if (favorite) {
            favorite = JSON.parse(favorite);
            setSearch({ ...search, page: 1, favorite: favorite, productsCategoryId: categoryId });
        }
    }, [categoryId]);

    const [searchQuery, setSearchQuery] = useState('');
    useEffect(() => {
        let query = '';
        search.favorite.map(item =>
            query += `listProductId=${item}&`
        );
        setSearchQuery(query);
    }, [search]);

    let url = `${API_URL}Products/by-local/${TENANT_ID}/${WEBSITE_ID}/${activeLang}?${searchQuery.substring(0, searchQuery.length - 1)}`;
    url +=
        search.productsCategoryId !== null
            ? `&productsCategoryId=${search.productsCategoryId}`
            : '';
    url += search.page ? `&page=${search.page}` : '';
    url += search.pageSize ? `&pageSize=${search.pageSize}` : '';
    const { data, error } = userSWR(url, fetcher);

    useEffect(() => {
        if (data !== undefined && data !== null) {
            setProducts(data.data);
            setTotalRows(data.totalRows);
        }
    });

    const updateFavorite = (favorite) => {
        setSearch({ ...search, page: 1, favorite: favorite });
    };

    return (
        <div className="product_page">
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <SideMenu sideMenu={productMenuTree} productMenu={productMenu} selectedId={search.productsCategoryId} isFavorite={true} activeLang={activeLang} />
                    </div>
                    <div className="col-md-9">
                        <SliderBanner productSlide={productSlide} />
                        <div className="filter_product" data-aos="fade-up">
                            <div className="row py-2 align-items-center">
                                <div className="col-md-6 col-6">{totalRows} {CONSTANT_TEXT.PRODUCTS[activeLang]}</div>
                                <div className="col-md-6 col-6 text-right">
                                    <div className="san-pham-yeu-thich">{CONSTANT_TEXT.FAVORITE[activeLang]}</div>
                                </div>
                            </div>
                        </div>
                        <ProductList products={products} updateCartQuantity={updateCartQuantity} updateFavoriteProduct={updateFavorite} favoriteProduct={search.favorite} isFavorite={true} activeLang={activeLang} />

                    </div>
                </div>
            </div>
            {
                totalRows > 9 &&
                <Pagination
                    position={'end'}
                    totalRows={totalRows}
                    search={search}
                    onChangePage={(pager) => setSearch({
                        ...search,
                        page: pager.currentPage
                    })} />
            }
        </div>
    );
}

export async function getServerSideProps(props) {
    const categoryId = props.query.productsCategoryId ? props.query.productsCategoryId : '';
    const productMenuTree = await api.get('Menus/detail', 'left');
    const productMenu = await api.get('Menus', 'left');
    const productSlide = await api.get('HardContents', '11a07318-1534-4f47-8d07-424be8d9dcd0');

    return {
        props: {
            productMenu: productMenu[0],
            productMenuTree: productMenuTree.data,
            productSlide: productSlide.data,
            categoryId
        }
    };
}

export default ProductFavorite;
