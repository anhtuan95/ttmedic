import React from 'react';
import Head from 'next/head';
import Footer from './footer';
import Menu from './menu';
import MenuMobile from './menu_mobile';
import HeaderSite from './header_site';
import BookingModal from './homepage/booking_modal';
import CallToAction from './common/call_to_action';
import { FILE_URL } from '../constants/config';
import PropTypes from 'prop-types';

function Layout({
    children, languages, setting, branchContacts, allBranchContacts, menuBottom, menuTop, routerName,
    socialNetworks, activeLang, totalQuantity, updateActiveLanguage, idPath
}) {
    return (
        <div>
            <Head>
                <meta charSet="utf-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
                {/* <title>JMedical Spa</title> */}
                <meta name="next-head-count" content="" />
                <meta httpEquiv="x-dns-prefetch-control" content="on" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                {/* <meta property="description" name="description" content={Transformer.parseJson(setting.metaDescription, activeLang)} /> */}

                <link rel="icon" href={`${FILE_URL}${setting.favicon}`} alt="" />
                <link rel="dns-prefetch" href="https://cdnjs.cloudflare.com/" />
                <link rel="dns-prefetch" href="https://ajax.googleapis.com/" />
                <link rel="dns-prefetch" href="https://rawgit.com/" />
                <link rel="preconnect" href="https://fonts.gstatic.com" />
                <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap" rel="stylesheet" />
                <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap" rel="stylesheet" />
                <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800&display=swap" rel="stylesheet" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" integrity="sha512-stI6dFNqqOFkPG83D0fOm3+lQu9LUv8ifVdyRrfUIE68MtFPDz/d322WUR9zzgv190m+4e/ovRctOaZN/a53iw==" crossOrigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha512-c8AIFmn4e0WZnaTOCXTOLzR+uIrTELY9AeIuUq6ODGaO619BjqG2rhiv/y6dIdmM7ba+CpzMRkkztMPXfVBm9g==" crossOrigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossOrigin="anonymous" /><link rel="stylesheet" href="/assets/css/slick.css" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossOrigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossOrigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw==" crossOrigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css" integrity="sha512-1cK78a1o+ht2JcaW6g8OXYwqpev9+6GqOkz9xmBN9iUUhIndKtxwILGWYOSibOKjLsEdjyjZvYDq/cZwNeak0w==" crossOrigin="anonymous" />
                <link rel="stylesheet" href="/assets/css/jquery.datetimepicker.css" />

                <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
                <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
                <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
                <link href="/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
                <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

                <link href="assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
                <link href="assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
                <link href="https://k1ngzed.com/dist/EasyZoom/easyzoom.css" rel="stylesheet" type="text/css" />
                <link href="https://k1ngzed.com/dist/swiper/swiper.min.css" rel="stylesheet" type="text/css" />

                <link rel="stylesheet" href="/assets/css/main.css" />
                <link rel="stylesheet" href="/assets/css/service.css" />
                <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
                <link href="/assets/css/layout.css" rel="stylesheet" type="text/css" />
                <link rel="shortcut icon" href="favicon.ico" />
            </Head>
            <Menu languages={languages}
                updateActiveLanguage={updateActiveLanguage}
                setting={setting} menuTop={menuTop} activeLang={activeLang} totalQuantity={totalQuantity} idPath={idPath} routerName={routerName} />
            <MenuMobile setting={setting} menuTop={menuTop} activeLang={activeLang} totalQuantity={totalQuantity}
                languages={languages}
                idPath={idPath}
                branchContacts={branchContacts}
                socialNetworks={socialNetworks}
                updateActiveLanguage={updateActiveLanguage} />
            <div className="overlay_menu"></div>
            <HeaderSite
                routerName={routerName}
                setting={setting} branchContacts={branchContacts} menuTop={menuTop}
                socialNetworks={socialNetworks} languages={languages}
                updateActiveLanguage={updateActiveLanguage}
                activeLang={activeLang}
                totalQuantity={totalQuantity}
                idPath={idPath} />
            <div>{children}</div>
            {
                activeLang &&
                <React.Fragment>
                    <CallToAction activeLang={activeLang} branchContact={branchContacts[0]} />
                    <BookingModal activeLang={activeLang} branchContacts={allBranchContacts} branchContact={branchContacts[0]} />
                </React.Fragment>
            }

            <Footer branchContacts={branchContacts} setting={setting} menuBottom={menuBottom} socialNetworks={socialNetworks} activeLang={activeLang} />

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" integrity="sha512-3n19xznO0ubPpSwYCRRBgHh63DrV+bdZfHK52b1esvId4GsfwStQNPJFjeQos2h3JwCmZl0/LgLxSKMAI55hgw==" crossOrigin="anonymous"></script>
            <script type="text/javascript" src="https://rawgit.com/jedfoster/Readmore.js/master/readmore.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.bundle.min.js" integrity="sha512-uo0BDK5V972jjjo8Z0vohOa6WOTIPaRGPI/Pg3GLzErcfOK8bkZpY3aHfG1WwS2uZ6cJ6GcrRiO4mRCJjGWOhQ==" crossOrigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossOrigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js" integrity="sha512-BQHCnffk8X4AKtVvm5fNoL8PlxlObAxUuofkgCoS22nvoMPdhJnf5gkvQZY0D9UBWg1mAUDWFjIET65o2vY8wQ==" crossOrigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js" integrity="sha512-A7AYk1fGKX6S2SsHywmPkrnzTZHrgiVT7GcQkLGDe2ev0aWb8zejytzS8wjo7PGEXKqJOrjQ4oORtnimIRZBtw==" crossOrigin="anonymous"></script>
            <script type="text/javascript" src="/assets/js/jquery.datetimepicker.full.min.js"></script>
            <script async defer src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.3.0/lazysizes.min.js" integrity="sha512-JrL1wXR0TeToerkl6TPDUa9132S3PB1UeNpZRHmCe6TxS43PFJUcEYUhjJb/i63rSd+uRvpzlcGOtvC/rDQcDg==" crossOrigin="anonymous"></script>

            <script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>

            <script src="https://k1ngzed.com/dist/swiper/swiper.min.js" type="text/javascript"></script>
            <script src="https://k1ngzed.com/dist/EasyZoom/easyzoom.js" type="text/javascript"></script>

            <script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="/assets/js/main.js"></script>
            <script src="/assets/js/scripts.js" type="text/javascript"></script>
        </div>
    );
}

Layout.propTypes = {
    children: PropTypes.any,
    languages: PropTypes.any,
    setting: PropTypes.any,
    branchContacts: PropTypes.any,
    allBranchContacts: PropTypes.any,
    menuBottom: PropTypes.any,
    menuTop: PropTypes.any,
    routerName: PropTypes.any,
    socialNetworks: PropTypes.any,
    activeLang: PropTypes.any,
    totalQuantity: PropTypes.any,
    updateActiveLanguage: PropTypes.any,
    idPath: PropTypes.any
};

export default Layout;
