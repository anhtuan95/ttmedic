import Tranformer from '../../constants/transformer';
import { FILE_URL, URL_NULL_IMG } from '../../constants/config';

function Value({ values, activeLang }) {
    return (
        <div className="section5_about pb-md-5 pb-3">
            <div className="container">
                <h3 className="mb-md-5 mb-3 fs-30 ff-montbold cl-blue text-uppercase" data-aos="fade-left">{Tranformer.parseJson(values.data.name, activeLang)}</h3>
                <div className="row">
                    {values.data.hardContentDetail.map(item => (
                        <div key={item.id} className="col-md-4">
                            <div className="value_item">
                                <div className="text-right icon">
                                    <img src={item.image === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.image}`} className="img-fluid lazyload" data-aos="flip-right" alt="" />
                                </div>
                                <div className="ff-mulibold" data-aos="fade-left">{`${Tranformer.parseJson(item.name, activeLang)}`}</div>
                                <div data-aos="fade-right">
                                    <p>{`${Tranformer.parseJson(item.description, activeLang)}`}</p>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default Value
    ;
