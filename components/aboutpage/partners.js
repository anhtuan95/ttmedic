import { useEffect } from 'react';
import { FILE_URL, CONSTANT_TEXT, URL_NULL_IMG } from '../../constants/config';
import Tranformer from '../../constants/transformer';

function Partners({ partners, activeLang }) {
    const partnersCarousel = () => {
        jQuery('.slider_partner').slick({
            slidesToShow: 5,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            infinite: true,
            dots: false,
            centerMode: true,
            prevArrow: jQuery('.partner_nav .prev-slider'),
            nextArrow: jQuery('.partner_nav .next-slider'),
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    };

    useEffect(() => {
        partnersCarousel();
    }, []);

    return (
        <div className="section6_about pb-md-5 pb-3">
            <div className="container">
                <div className="row">
                    <div className="col-md-8">
                        <h3 className="mb-md-5 mb-3 fs-30 ff-montbold cl-blue text-uppercase" data-aos="fade-left">{(CONSTANT_TEXT.PARTNERS[activeLang])}</h3>
                    </div>
                    <div className="col-md-4">
                        <div className="text-right">
                            <div className="partner_nav">
                                <div className="prev-slider"><i className="fa fa-long-arrow-left" aria-hidden="true"></i></div>
                                <div className="next-slider"><i className="fa fa-long-arrow-right" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="slider_partner">
                {partners.map(item => (
                    <div key={item.id} className="item">
                        <div className="logo_partner">
                            <img src={item.thumbnail === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.thumbnail}`} className="img-fluid lazyload" alt="" />
                        </div>
                        <div className="content_partner"><div>{`${Tranformer.parseJson(item.description)}`}</div></div>
                        <div className="link_partner">
                            <a>{`${item.url}`}</a>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Partners;
