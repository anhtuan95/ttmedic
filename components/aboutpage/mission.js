import Transformer from '../../constants/transformer';
import { FILE_URL } from '../../constants/config';

function Mission({ missions, activeLang }) {
    return (
        <div className="section2_about py-md-5 py-3">
            <div className="container">
                <div className="row">
                    <div className="col-md-2">
                        <h3 className="mb-md-5 mb-3 fs-30 ff-montbold cl-blue text-center text-uppercase title-text" data-aos="fade-left">{Transformer.parseJson(missions.data.name, activeLang)}</h3>
                        <div className="fs-14 mb-3 cl-blue ff-mulibold" data-aos="fade-right">{`${Transformer.parseJson(missions.data.hardContentDetail[0].name, activeLang)}`}</div>
                    </div>
                    <div className="col-md-4">
                        <div className="d-flex flex-column justify-content-center h-100" data-aos="fade-left">
                            <p>{`${Transformer.parseJson(missions.data.hardContentDetail[0].description, activeLang)}`}</p>
                        </div>
                    </div>
                    <div className="col-md-1"></div>
                    <div className="col-md-5">
                        <div className="img_mision" data-aos="fade-up">
                            <img src={`${FILE_URL}${missions.data.hardContentDetail[0].image}`} className="img-fluid lazyload" alt={missions.data.hardContentDetail[0].alt} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Mission
    ;
