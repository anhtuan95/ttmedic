import React, { useEffect } from 'react';
import Tranformer from '../../constants/transformer';
import { FILE_URL, URL_NULL_IMG } from '../../constants/config';

function Vision({ visions, staffs, activeLang }) {
    const relatedBannerCarousel = () => {
        jQuery('.slider_staff').slick({
            slidesToShow: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            speed: 1500,
            infinite: true,
            dots: false,
            prevArrow: jQuery('.staff_nav .prev-slider'),
            nextArrow: jQuery('.staff_nav .next-slider')
        });
    };

    useEffect(() => {
        relatedBannerCarousel();
    }, []);
    return (
        <div className="section4_about pt-md-5 py-3">
            <div className="container">
                <h3 className="mb-md-5 mb-3 fs-30 ff-montbold cl-blue text-center text-uppercase" data-aos="fade-left">{Tranformer.parseJson(visions.data.name, activeLang)}</h3>
                <div className="row">
                    {visions.data.hardContentDetail.map(item => (
                        <div key={item.id} className="col-md-4">
                            <h4 className="ff-mulibold fs-14 mb-4 cl-blue" data-aos="fade-left">{`${Tranformer.parseJson(item.name, activeLang)}`}</h4>
                            <div data-aos="fade-right">
                                <p>{`${Tranformer.parseJson(item.description, activeLang)}`}</p>
                            </div>

                        </div>
                    ))}
                </div>
            </div>

            <div className="team_box py-md-5 py-3">
                <div className="bg_img">
                    <img src={staffs.data.image === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${staffs.data.image}`} className="img-fluid lazyload" alt="" />
                </div>
                <div className="container">
                    <div className="position-relative">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="slider_staff" data-aos="fade-up">
                                    {staffs.data.hardContentDetail.map(item => (
                                        <div key={item.id} className="item">
                                            <img src={item.image === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.image}`} className="img-fluid lazyload" alt="" />
                                            <div className="meta_info cl-blue">
                                                <div className="person ff-montbold">
                                                    {`${Tranformer.parseJson(item.name)}`}
                                                </div>
                                                {`${Tranformer.parseJson(item.description)}`}
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <div className="col-md-9">
                                <div className="ff-montbold pt-5 mt-5" data-aos="fade-left">{`${Tranformer.parseJson(staffs.data.name)}`}</div>
                            </div>
                        </div>
                        <div className="staff_nav">
                            <div className="prev-slider"><i className="fa fa-long-arrow-left" aria-hidden="true"></i></div>
                            <div className="next-slider"><i className="fa fa-long-arrow-right" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Vision
    ;
