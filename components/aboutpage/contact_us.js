function ContactUs() {
    return (
        <div className="contact_us bg-gray py-5">
            <div className="container">
                <div className="row justify-content-between">
                    <div className="col-md-2">
                        <div className="row">
                            <div className="col-md-12 col-6">
                                <h3 className="mb-4 fs-30 ff-montbold cl-blue text-uppercase" data-aos="fade-left">Liên hệ</h3>
                                <p data-aos="fade-right">Đến với J Medical Spa để có những phút giây thư giãn nhất.</p>
                            </div>
                            <div className="col-md-12 col-6">
                                <img src="assets/images/contact.png" className="img-fluid lazyload" data-aos="fade-up" alt="" />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9">
                        <div data-aos="fade-up">
                            <div className="row">
                                <div className="col-md-6">
                                    <input type="text" className="form-control" placeholder="TÊN KHÁCH HÀNG (*)" />
                                </div>
                                <div className="col-md-6">
                                    <input type="text" className="form-control" placeholder="SĐT (*)" />
                                </div>
                                <div className="col-md-6">
                                    <input type="email" className="form-control" placeholder="EMAIL (*)" />
                                </div>
                                <div className="col-md-6">
                                    <select name="" id="" className="form-control">
                                        <option value="">TƯ VẤN QUA ĐIỆN THOẠI</option>
                                        <option value="">TƯ VẤN QUA EMAIL</option>
                                    </select>
                                </div>
                                <div className="col-md-12">
                                    <textarea name="" cols="30" rows="2" placeholder="GHI CHÚ" className="form-control"></textarea>
                                </div>
                            </div>
                            <input type="submit" className="btn btn-blue mt-4" value="Gửi" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ContactUs
    ;
