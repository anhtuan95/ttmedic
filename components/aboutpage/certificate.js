import { FILE_URL } from '../../constants/config';

function Certificate({ missions }) {
    return (
        <div className="section3_about">
            <div className="bg_certificate" data-aos="fade-right">
                <img src={`${FILE_URL}${missions.data.hardContentDetail[1].image}`} className="img-fluid lazyload" alt="" />
            </div>
            <div className="img_certificate">
                <img src={`${FILE_URL}${missions.data.hardContentDetail[2].image}`} className="img-fluid lazyload" alt="" data-aos="fade-left" />
            </div>
        </div>
    );
}

export default Certificate
    ;
