import Transformer from '../../constants/transformer';
import { FILE_URL, URL_NULL_IMG } from '../../constants/config';

function BannerAbout({ introduces, activeLang }) {
    if (introduces === undefined) return null;
    return (
        <div className="banner_about bg-blue">
            <div className="container">
                <div className="about_top py-md-5">
                    <div className="row mx-md-0 align-items-center">
                        <div className="col-md-2 px-md-0">
                            <div className="pr-md-5">
                                <ul className="list-unstyled" data-aos="fade-right">
                                    <li className="mb-md-4">{`${Transformer.parseJson(introduces.data.hardContentDetail[0].name, activeLang)}`}</li>
                                    <li>{`${Transformer.parseJson(introduces.data.hardContentDetail[0].description, activeLang)}`}</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-4 px-md-0">
                            <div className="mb-3 mb-md-0">
                                <img src={`${FILE_URL}${introduces.data.hardContentDetail[1].image}`} className="img-fluid w15 lazyload" alt="" />
                                <img src={`${FILE_URL}${introduces.data.hardContentDetail[1].image}`} className="img-fluid w15 lazyload" alt="" />
                            </div>
                            <div className="cl-yellow fs-20 mb-3 ff-montbold" data-aos="fade-up">
                                {`${Transformer.parseJson(introduces.data.hardContentDetail[1].name, activeLang)}`}
                            </div>
                        </div>
                        <div className="col-md-1 px-md-0"></div>
                        <div className="col-md-5 px-md-0">
                            <div data-aos="fade-up">
                                <p> {`${Transformer.parseJson(introduces.data.hardContentDetail[2].name, activeLang)}`}</p>
                                <p>{`${Transformer.parseJson(introduces.data.hardContentDetail[2].description, activeLang)}`}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="about_bottom">

                    <div id="circle">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="300px" height="300px" viewBox="0 0 300 300" enableBackground="new 0 0 300 300" xmlSpace="preserve">
                            <defs>
                                <path id="circlePath" d="M 150, 150 m -60, 0 a 60,60 0 0,1 120,0 a 60,60 0 0,1 -120,0 " />
                            </defs>
                            <circle cx={150} cy={100} r={75} fill="none" />
                            <g>
                                <use xlinkHref="#circlePath" fill="none" />
                                <text fill="#000">
                                    <textPath xlinkHref="#circlePath">GIẢI PHÁP CHUẨN  &nbsp; &#870; &nbsp; CHO VẺ ĐẸP TỰ NHIÊN  &nbsp; &#870; &nbsp;</textPath>
                                </text>
                            </g>
                        </svg>
                    </div>
                    {/* <img src={`${FILE_URL}${introduces.data.hardContentDetail[0].image}`} className="img-fluid lazyload" alt="" /> */}

                    <div className="row mx-md-0 align-items-end">
                        <div className="col-md-2 px-md-0">
                            <div className="text-gold white-space-nowrap ff-montbold mb-5 fs-28" data-aos="fade-up" dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(introduces.data.name, activeLang))} >
                            </div>
                        </div>
                        <div className="col-md-10 px-md-0">
                            <div className="img_about" data-aos="fade-right">
                                <img src={introduces.data.image === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${introduces.data.image}`} className="img-fluid lazyload" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BannerAbout
    ;
