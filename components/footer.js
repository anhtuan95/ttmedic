// import Transformer from '../constants/transformer';
// import { FILE_URL, CONSTANT_TEXT, URL_NULL_IMG } from '../constants/config';
// import api from '../constants/api';
// import Link from 'next/link';
import PropTypes from 'prop-types';

function Footer({ setting, menuBottom, socialNetworks, branchContacts, activeLang }) {
    return (
        <footer>
            <div className="container">
                <div className="row p-footer">
                    <div className="col-md-8">
                        <ul className="footer-link">
                            <li>Trang chủ |</li>
                            <li>Giới thiệu |</li>
                            <li>Chuyên khoa |</li>
                            <li>Dịch vụ hỗ trợ |</li>
                            <li>Tin tức |</li>
                            <li>Tuyển dụng |</li>
                            <li>Liên hệ</li>
                        </ul>
                        <h4 className="text-uppercase"><b>Phòng khám đa khoa 125 Thái thịnh</b></h4>

                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-map"></i>
                        Địa chỉ: Thái thịnh
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-clock-o"></i>
                        Giờ làm việc : 8:00 - 18:00
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-envelope"></i>
                        Email:ghh@gmail.com
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-phone"></i>
                        Điện thoại: 0-987989890
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-headphones"></i>
                        Hỗ trợ Chăm sóc khách hàng : 0243855522 / 024 73096888
                        </div>
                        <div className="row align-items-center mt-3">
                            <div className="col-6">
                                <ul className="social-icons social-icons-color">
                                    <li>
                                        <a href="javascript:;" data-original-title="facebook" className="facebook"> </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-original-title="youtube" className="youtube"> </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-6">
                                <a href="#" className="btn btn-circle yellow-crusta">Gửi tin nhắn</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <iframe name="f2fd5baa32f461c" width="100%" height="300px"
                            data-testid="fb:page Facebook Social Plugin" title="fb:page Facebook Social Plugin"
                            frameBorder="0" allowtransparency="true" allowFullScreen="true" scrolling="no"
                            allow="encrypted-media"
                            src="https://www.facebook.com/v5.0/plugins/page.php?adapt_container_width=true&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df2c009a7c6f4118%26domain%3Dthaithinhmedic.vn%26origin%3Dhttp%253A%252F%252Fthaithinhmedic.vn%252Ff75af15fcc9f4%26relation%3Dparent.parent&amp;container_width=263&amp;height=300&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fphongkhamdakhoa125thaithinh%2F&amp;locale=vi_VN&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline"
                        ></iframe>
                    </div>
                </div>

            </div>
            <p className="text-center m-0 py-2 copyright" >Bản quyền thuộc về © 2017 Công ty cổ
            phần Bệnh viện Thái Thịnh.</p>
        </footer>
        // <div id="footer_site">
        //     <div className="container">
        //         <div className="text-center">
        //             <a id="goTop">
        //                 <i className="fa fa-angle-up" aria-hidden="true"></i>
        //             </a>
        //         </div>
        //         <div className="row justify-content-between">
        //             <div className="col-md-1 col-3">
        //                 <Link href="/index">
        //                     <a>
        //                         <img src={setting.logoFooter === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${setting.logoFooter}`} className="img-fluid lazyload" alt="" />
        //                     </a>
        //                 </Link>
        //             </div>
        //             <div className="col-md-2 col-9">
        //                 <h3 className="ff-montbold fs-14 text-uppercase cl-blue mt-0">{CONSTANT_TEXT.HOTLINE[activeLang]}</h3>
        //                 <p><strong>{`${setting.hotline}`}</strong></p>
        //                 <p>{CONSTANT_TEXT.ADVISORY[activeLang]}</p>
        //                 {
        //                     branchContacts[0].branchContactDetails.map(item => (
        //                         (item.contactType === 0)
        //                             ? <p key={item.id}>Email: {item.contactValue}</p>
        //                             : null
        //                     ))
        //                 }
        //                 <ul className="list-inline social mt-md-5">
        //                     {socialNetworks.map(item => (
        //                         <li key={item.id} className="list-inline-item">
        //                             <Link href={Transformer.redirectLink(item.url)}>
        //                                 <a>
        //                                     <i className={`fa fa-${item.icon}`} aria-hidden="true"></i>
        //                                 </a>
        //                             </Link>
        //                         </li>
        //                     ))}
        //                 </ul>
        //             </div>
        //             <div className="col-md-3">
        //                 <h3 className="ff-montbold fs-14 text-uppercase cl-blue">{CONSTANT_TEXT.ADDRESS[activeLang]}</h3>
        //                 {branchContacts.map(item => (
        //                     <p key={item.id}>{`${Transformer.parseJson(item.address, activeLang)}`}</p>
        //                 ))
        //                 }
        //                 <h3 className="ff-montbold fs-14 text-uppercase cl-blue">{CONSTANT_TEXT.WORKING_HOURS[activeLang]}</h3>
        //                 {branchContacts.map(item => (
        //                     <p key={item.id}>{`${Transformer.parseJson(item.workTime, activeLang)}`}</p>
        //                 ))
        //                 }
        //             </div>
        //             <div className="col-md-3">
        //                 <h3 className="ff-montbold fs-14 text-uppercase cl-blue">{CONSTANT_TEXT.SIGNUP_FOR[activeLang]}</h3>
        //                 <input type="text" className="form-control" placeholder={CONSTANT_TEXT.YOUR_EMAIL[activeLang]} id="subEmail" />
        //                 <button type="submit" name="submit_email" className="btn btn-blue mt-md-5 mt-3" onClick={() => doSubscribe()}>{CONSTANT_TEXT.SEND[activeLang]}</button>
        //             </div>
        //             <div className="col-md-2">
        //                 <h3 className="ff-montbold fs-14 text-uppercase cl-blue">{CONSTANT_TEXT.LINK[activeLang]}</h3>
        //                 <ul>
        //                     {menuBottom && menuBottom[0].menuItems && menuBottom[0].menuItems.map((item, index) => (
        //                         <li key={index}>
        //                             <Link href={Transformer.redirectLink(item.url)}>
        //                                 <a >{`${Transformer.parseJson(item.name, activeLang)}`}</a>
        //                             </Link>
        //                         </li>
        //                     ))}
        //                 </ul>
        //             </div>
        //         </div>
        //     </div>
        // </div>

    );
}

// async function doSubscribe() {
//     const email = document.getElementById('subEmail').value;

//     if (!Transformer.validateEmail(email)) {
//         alert('You have entered an invalid email address!');
//         return;
//     }

//     const insertSubscribe = await api.post('Subscribes', null, {
//         email: email
//     });

//     alert(insertSubscribe.message);
//     if (insertSubscribe.code === 1) {
//         document.getElementById('subEmail').value = '';
//     }
// }

Footer.propTypes = {
    setting: PropTypes.any,
    menuBottom: PropTypes.any,
    socialNetworks: PropTypes.any,
    branchContacts: PropTypes.any,
    activeLang: PropTypes.any
};

export default Footer;
