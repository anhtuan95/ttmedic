import { useState } from 'react';
import Link from 'next/link';
import { FILE_URL, CONSTANT_TEXT } from '../constants/config';
import Transformer from '../constants/transformer';

function renderRecursiveMenu(menuData, activeLang) {
    return (
        <ul>
            {
                menuData.map(item => (
                    <li key={item.id} className={item.children.length > 0 ? 'has-child' : ''}>
                        <Link prefetch={false} href={item.data.objectId ? `${item.data.seoLink}` : Transformer.redirectLink(item.data.url)}>
                            <a >{`${Transformer.parseJson(item.data.name, activeLang)}`}</a>
                        </Link>
                        {(item.children.length > 0) && renderRecursiveMenu(item.children, activeLang)}
                    </li>))
            }
        </ul>
    );
}

function MenuMobile({ setting, menuTop, activeLang, totalQuantity, updateActiveLanguage, languages, branchContacts, socialNetworks }) {
    const activeLanguage = languages.find(x => x.languageId === activeLang);
    const activeLangIcon = activeLanguage ? activeLanguage.icon : languages[0].icon;
    const [showLanguageList, setShowLanguageList] = useState(false);

    const showLanguageListFnc = () => {
        setShowLanguageList(!showLanguageList);
    };

    const selectLanguage = (languageId) => {
        updateActiveLanguage(languageId);
        setShowLanguageList(!showLanguageList);
    };

    const toggleMobileMenu = () => {
        document.getElementById('hamburger_btn').click();
    };

    return (
        <div className="menu_mobile">

            <div className="text-gold ff-montbold mb-4 fs-30" dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(setting.brand, activeLang))}>
            </div>
            <div className="row">
                <div className="col-12">
                    <ul className="menu_site">
                        {menuTop && menuTop.data.menuItems &&
                            menuTop.data.menuItems.filter((menu, index) => index % 2 === 0).map(item => (
                                <li key={item.id} className={item.children.length > 0 ? 'has-child' : ''}>
                                    <Link prefetch={false} href={item.data.objectId ? `${item.data.seoLink}` : Transformer.redirectLink(item.data.url)}><a >{`${Transformer.parseJson(item.data.name, activeLang)}`}</a></Link>
                                    {(item.children.length > 0) && renderRecursiveMenu(item.children, activeLang)}
                                </li>
                            ))}
                    </ul>
                    <ul className="menu_site">
                        {menuTop && menuTop.data.menuItems &&
                            menuTop.data.menuItems.filter((menu, index) => index % 2 === 1).map(item => (
                                <li key={item.id} className="">
                                    <Link prefetch={false} href={item.data.objectId ? `${item.data.seoLink}` : Transformer.redirectLink(item.data.url)}><a >{`${Transformer.parseJson(item.data.name, activeLang)}`}</a></Link>
                                </li>
                            ))}
                    </ul>
                </div>
            </div>

            <div className="info_site">
                <div className="row mx-md-0">
                    <div className="col-md-4 px-md-0">
                        <ul className="list-unstyled mt-4">
                            <li><strong>{CONSTANT_TEXT.HOTLINE[activeLang]}</strong></li>
                            <li><p>{`${setting.hotline}`}</p></li>
                            <li><strong>{CONSTANT_TEXT.WORKING_HOURS[activeLang]}</strong></li>
                            {branchContacts.map(item => (
                                <li key={item.id}><p>{`${Transformer.parseJson(item.workTime, activeLang)}`}</p></li>
                            ))
                            }
                        </ul>
                        <a className="btn btn-gold" data-toggle="modal" data-target="#bookingnow" onClick={toggleMobileMenu}><span>{CONSTANT_TEXT.BOOKING[activeLang]}</span></a>
                    </div>
                    <div className="col-md-6 px-md-0">
                        <ul className="list-unstyled mt-4">
                            <li><strong>{CONSTANT_TEXT.ADDRESS[activeLang]}</strong></li>

                            {branchContacts.map(item => (
                                <li key={item.id}><p>{`${Transformer.parseJson(item.address, activeLang)}`}</p></li>
                            ))
                            }
                        </ul>

                        <ul className="list-inline social language-select">
                            {socialNetworks.map(item => (
                                <li key={item.id} className="list-inline-item">
                                    <Link href={Transformer.redirectLink(item.url)}>
                                        <a>
                                            <i className={`fa fa-${item.icon}`} aria-hidden="true"></i>
                                        </a>
                                    </Link>
                                </li>
                            ))}
                            <li className="list-inline-item has-child text-center" onClick={showLanguageListFnc}>
                                <a>
                                    <img src={`${FILE_URL}${activeLangIcon}`} className="lazyload" alt="" />
                                </a>
                                <ul className={`multiLang-list ${showLanguageList ? 'active' : ''}`}>
                                    {languages.map(item => (
                                        <li onClick={() => selectLanguage(item.languageId)} key={item.languageId}>{`${item.name}`}</li>
                                    ))}
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MenuMobile;
