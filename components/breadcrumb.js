import { MENU_OBJECT_TYPES, OBJECT_TYPE } from '../constants/config'
import Transformer from '../constants/transformer'
import Link from 'next/link'

function Breadcrumb(props) {
    const { type, name, seoLinks, activeLang } = props
    const renderBreadcrumb = () => {
        const { name, type } = props
        let breadcrumb = []
        for (let i = 0; i < name.length; i++) {
            if (i === (name.length - 1)) {
                breadcrumb.push(
                    <li key={i} className="list-inline-item">
                        {type.includes(OBJECT_TYPE.NEWS) ? Transformer.getNewsTitle(name[i]) : name[i]}
                    </li>
                )
            } else {
                breadcrumb.push(
                    <li key={i} className="list-inline-item">
                        <Link href={seoLinks !== undefined ? Transformer.redirectLink(seoLinks[i]) : '#'}>
                            <a >{type.includes(OBJECT_TYPE.NEWS) ? Transformer.getNewsTitle(name[i]) : name[i]}</a>
                        </Link>
                    </li>
                )
            }

        }
        return breadcrumb
    }

    const renderBreadcrumbType = () => {
        const { type } = props
        let breadcrumbTypes = []
        for (let i = 0; i < type.length; i++) {
            breadcrumbTypes.push(
                <li key={i} className="list-inline-item">
                    <Link href={MENU_OBJECT_TYPES[type[i]].seoLink}>
                        <a>{Transformer.parseJson(MENU_OBJECT_TYPES[type[i]].name, activeLang)}</a>
                    </Link>
                </li>
            )
        }
        return breadcrumbTypes
    }

    return (
        <div className="breadcrumb_site">
            <div className="container">
                <ul className="list-inline">
                    {
                        renderBreadcrumbType()
                    }
                    {
                        renderBreadcrumb()
                    }
                </ul>
            </div>
        </div>
    )
}

export default Breadcrumb