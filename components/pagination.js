import React, { Component } from 'react'
import PropTypes from 'prop-types'

const propTypes = {
    onChangePage: PropTypes.func.isRequired,
    initialPage: PropTypes.number,
}

const defaultProps = {
    initialPage: 1,
}

class Pagination extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pager: {
                pageSize: 5,
            },
            totalRows: 0,
            isFirstTimeClick: false
        };
    }

    componentDidMount() {
        const { currentPage, search } = this.props
        this.setState({
            totalRows: this.props.totalRows,
            pager: { pageSize: search.pageSize }
        }, () => {
            if (currentPage !== undefined) {
                this.setPage(currentPage);
            } else {
                this.setPage(this.props.initialPage);
            }
        })
    }

    componentWillReceiveProps(nextProps) {
        const { totalRows } = nextProps

        if (totalRows !== this.state.totalRows) {
            this.setState({
                totalRows,
                isFirstTimeClick: false
            }, () => {
                this.setPage(this.props.initialPage);
            })
        }
        if (nextProps.search.productsCategoryId !== this.props.search.productsCategoryId) {
            this.setState({
                totalRows,
                isFirstTimeClick: false
            }, () => {
                this.setPage(this.props.initialPage);
            })
        }
    }

    setPage = (page) => {
        let pager = this.state.pager
        pager = this.getPager(this.state.totalRows, page, pager.pageSize);
        this.setState({ pager }, () => {
            if (this.state.isFirstTimeClick)
                this.props.onChangePage(this.state.pager)

            this.setState({
                isFirstTimeClick: true
            })
        });
    }

    getPager(totalItems, currentPage, pageSize) {
        const { search } = this.props
        currentPage = currentPage || 1
        pageSize = pageSize || search.pageSize

        let totalPages = Math.ceil(totalItems / pageSize)
        let startPage, endPage
        if (totalPages <= 10) {
            startPage = 1
            endPage = totalPages
        } else {
            if (currentPage <= 6) {
                startPage = 1
                endPage = 10
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9
                endPage = totalPages
            } else {
                startPage = currentPage - 5
                endPage = currentPage + 4
            }
        }

        const pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i)
        return {
            totalItems,
            currentPage,
            pageSize,
            totalPages,
            startPage,
            endPage,
            pages
        };
    }

    render() {
        const pager = this.state.pager
        const { position } = this.props
        if (!pager.pages) {
            return null
        }

        return (
            <div className="page_nav mt-5">
                <div className="container">
                    <nav aria-label="Page navigation">
                        <ul className={position !== undefined ? `pagination justify-content-${position}` : `pagination justify-content-center`}>
                            <li className={pager.currentPage === 1 ? 'page-item disabled' : 'page-item'}>
                                <a className="page-link" href="#search_list" aria-label="Previous" onClick={() => this.setPage(pager.currentPage - 1)}>
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            {
                                pager.pages.map((page, index) => (
                                    <li key={index} className={pager.currentPage === page ? 'page-item active' : 'page-item'}>
                                        <a className="page-link" href="#search_list" onClick={() => this.setPage(page)}>{page}</a>
                                    </li>
                                ))
                            }
                            <li className={pager.currentPage === pager.totalPages ? 'page-item disabled' : 'page-item'}>
                                <a className="page-link" href="#search_list" aria-label="Next" onClick={() => this.setPage(pager.currentPage + 1)}>
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        )
    }
}

Pagination.propTypes = propTypes;
Pagination.defaultProps = defaultProps;

export default Pagination