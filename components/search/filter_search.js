import React, { Component } from 'react'
import Link from 'next/link'
class FilterSearch extends Component {
    constructor(props) {
        super(props)

        this.state = {
            keyword: '',
            title: '',
        }
    }

    handleValueChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        const { keyword } = this.state
        return (
            <div className="search">
                <form>
                    <div className="form_search">
                        <input type="text" className="form-control" placeholder={`Nhập từ khóa`} onChange={this.handleValueChange} name="keyword" value={keyword} />
                    </div>
                    <Link href={{ pathname: '/search-all', query: { keyword: keyword } }}>
                        <button >
                            <i className="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </Link>
                </form>
            </div>
        )
    }
}

export default FilterSearch