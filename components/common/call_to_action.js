import React from 'react';
import { CONSTANT_TEXT } from '../../constants/config';
import Link from 'next/link';
import * as _ from 'lodash';

function CallToAction({ activeLang, branchContact }) {
    const phoneNumber = branchContact.branchContactDetails.find(item => item.contactType === 1).contactValue;
    return (
        <React.Fragment>
            <div id="call_to_action">
                <ul>
                    <li><a href="/" data-toggle="modal" data-target="#bookingnow" className="vertical">{CONSTANT_TEXT.SCHEDULE[activeLang]}</a></li>
                    <li><a href="/"><img src="assets/images/messager.png" alt="facebook_messenger" className="lazyload" /></a></li>
                    <li><a href={`tel:${phoneNumber}`}><img src="assets/images/call.png" alt="contact_us" className="lazyload" /></a></li>
                    <li><Link href="gio-hang"><a><img src="assets/images/cart-icon.png" alt="go_to_cart" className="lazyload" /></a></Link></li>
                </ul>
            </div>
            <div id="demo_poupsuccess">
                <div>
                    <a href="#" data-placement="top" data-toggle="modal" data-target="#bookingsuccess" id="show_booking_success">Success</a>
                </div>
            </div>
        </React.Fragment>
    );
}

export default CallToAction
    ;
