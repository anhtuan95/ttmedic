export function PrevArrow(props) {
    const { onClick } = props
    return <div className="prev-slider" onClick={onClick}><span></span></div>
}

export function NextArrow(props) {
    const { onClick } = props
    return <div className="next-slider" onClick={onClick}><span></span></div>
}