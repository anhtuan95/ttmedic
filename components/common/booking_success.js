import React, { useState, useEffect } from 'react';
import { CONSTANT_TEXT } from '../../constants/config';
import Transformer from '../../constants/transformer';

var moment = require('moment');

export default function BookingSuccess({ bookingTime, activeLang, branchContact }) {
    const [weekDay, setWeekDay] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');

    useEffect(() => {
        if (moment(bookingTime).isValid()) {
            const locale = activeLang.split('-')[0];
            moment.locale(locale);
            setWeekDay(moment(bookingTime).format('dddd'));
            setDate(moment(bookingTime).format('DD/MM/YYYY'));
            setTime(moment(bookingTime).format('HH:mm'));
        }
    }, [bookingTime]);

    return (
        <div>
            <div className="modal fade" id="bookingsuccess" tabIndex={-1} role="dialog" aria-labelledby="booking" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg">
                    <div className="modal-content">
                        <div className="modal-body">
                            <div className="title_booking">
                                <div className="icon">
                                    <i className="fa fa-calendar-o" />
                                </div>
                                <h5 className="cl-blue fs-20 mb-0 ff-montbold" id="bookingalert">{CONSTANT_TEXT.BOOKING_APPOINTMENT[activeLang]}</h5>
                                <p>{CONSTANT_TEXT.SLOGAN[activeLang]}</p>
                                <a className="close" data-dismiss="modal" aria-label="Close" id="close-booking-success">+</a>
                            </div>
                            <div className="row">
                                <div className="col-7">
                                    <div className="p-md-5 p-3">
                                        <h3 className="cl-blue fs-14"><img src="assets/images/star.png" className="w15 lazyload" alt="" />{CONSTANT_TEXT.BOOKING_SUCCESS[activeLang]}</h3>
                                        <p>{CONSTANT_TEXT.THANKS_STATEMENT[activeLang]}<strong>{branchContact.website}</strong></p>
                                        {/* <p>Mã xác nhận sẽ được gửi đến quý khách trong <strong>60 phút</strong></p> */}
                                        <p>{CONSTANT_TEXT.BOOKING_TIME[activeLang]}<strong style={{ textTransform: 'capitalize' }}>{`${weekDay}, ${date}, ${time}`}</strong></p>
                                    </div>
                                </div>
                                <div className="col-5">
                                    <img src="assets/images/imgsuccess.png" className="img-fluid lazyload" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
