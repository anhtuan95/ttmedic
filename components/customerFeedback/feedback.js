import React, { useState, useEffect } from 'react'
import fetch from 'unfetch'
import { FILE_URL, URL_NULL_IMG } from '../../constants/config'
import Transformer from '../../constants/transformer'
import Link from 'next/link'

const fetcher = url => fetch(url).then(r => r.json())

function Feedback(props) {
    const { activeLang } = props
    const [feedbacks, setFeedbacks] = useState([])

    useEffect(() => {
        setFeedbacks(props.feedbacks)
    }, [props.feedbacks])

    return (
        <div className="box_toggle mt-5" id="search_list">
            <div className="accordion" id="accordionExample" data-aos="fade-up">
                {
                    feedbacks.length > 0 ?
                        feedbacks.map((item, index) => (
                            <div className="card" key={item.id}>
                                <div className="card-header" id={`heading${item.id}`}>
                                    <a className="" data-toggle="collapse" data-target={`#collapse${item.id}`} aria-expanded={index === 0 ? "true" : "false"} aria-controls={`#collapse${item.id}`}>
                                        <span className="name text-gold">{Transformer.truncate(Transformer.parseJson(item.name, activeLang), 23)}</span>
                                        <span className="text-gold">{Transformer.truncate(Transformer.parseJson(item.position, activeLang), 40)}</span>
                                        <span><i className="fa fa-angle-up" aria-hidden="true"></i></span>
                                    </a>
                                </div>
                                <div id={`collapse${item.id}`} className={`collapse ${index === 0 ? "show" : ""}`} aria-labelledby={`heading${item.id}`} data-parent="#accordionExample">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-md-5">
                                                <div className="customer_info">
                                                    <div className="name">{Transformer.truncate(Transformer.parseJson(item.name, activeLang), 23)}</div>
                                                    <div className="working">{Transformer.truncate(Transformer.parseJson(item.position, activeLang), 40)}</div>
                                                    <div className="img_customer mt-4">
                                                        <img src={item.avata !== null ? `${FILE_URL}${item.avata}` : `${FILE_URL}${URL_NULL_IMG}`} className="img-fluid lazyload" alt={Transformer.parseJson(item.name, activeLang)} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-7">
                                                <div className="video_intro">
                                                    {
                                                        item.url !== "" && item.url !== "#" &&
                                                        <Link href={Transformer.redirectLink(item.url)}>
                                                            <a data-fancybox=""><i className="fa fa-caret-right" aria-hidden="true"></i></a>
                                                        </Link>
                                                    }
                                                    <img
                                                        className="img-fluid lazyload"
                                                        src={item.thumbnail !== null ? `${FILE_URL}${item.thumbnail}` : `${FILE_URL}${URL_NULL_IMG}`}
                                                        alt={Transformer.parseJson(item.alt, activeLang)} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mt-md-5">
                                            <div className="row">
                                                <div className="col-md-5">
                                                    <div className="quote_text">{Transformer.truncate(Transformer.parseJson(item.description, activeLang), 100)}</div>
                                                </div>
                                                <div className="col-md-7" dangerouslySetInnerHTML={Transformer.createHTML(Transformer.truncate(Transformer.parseJson(item.content, activeLang), 1300))}></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )) :
                        (
                            <div className="card">
                                <div className="card-header" id="headingThree">
                                    <a className="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <span className="name text-gold">Chị Hạnh</span>
                                        <span className="text-gold">Bắc Giang - mẹ bầu lần 3</span>
                                        <span><i className="fa fa-angle-up" aria-hidden="true"></i></span>
                                    </a>
                                </div>
                                <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-md-5">
                                                <div className="customer_info">
                                                    <div className="name">Chị Hạnh</div>
                                                    <div className="working">Bắc Giang - mẹ bầu lần 3</div>
                                                    <div className="img_customer mt-4">
                                                        <img src="assets/images/customer.png" className="img-fluid lazyload" alt=""></img>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-7">
                                                <div className="video_intro">
                                                    <Link href="https://www.youtube.com/watch?v=jLBZPcwuUFM">
                                                        <a data-fancybox=""><i className="fa fa-caret-right" aria-hidden="true"></i></a>
                                                    </Link>
                                                    <img src="assets/images/massage.jpg" className="img-fluid lazyload" alt=""></img>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mt-md-5">
                                            <div className="row">
                                                <div className="col-md-5">
                                                    <div className="quote_text">Tôi thích nhất là dịch vụ massage Thụy Điển và massage lưu dẫn hệ bạch huyết tại J Medical Spa.</div>
                                                </div>
                                                <div className="col-md-7">
                                                    <p>Trong khoảng thời gian công tác tại Việt Nam Mark D Hurwizt - Tiến sĩ y học tại Philadenphia, Hoa Kỳ đã chọn J Medical Spa để chăm sóc sức khỏe của mình. Ông thích nhất là dịch vụ massage Thụy Điển và massage lưu dẫn hệ bạch huyết tại J Medical Spa.</p>
                                                    <p>Mr. Hurwizt chia sẻ, ông vốn dành đến 12h/ngày hoặc hơn để làm việc, vì thế cơ thể luôn trong tình trạng đau nhức, mệt mỏi. Khi còn sinh sống tại Mỹ, Mr. Hurwizt đã đến các thẩm mỹ viện và spa để thực hiện massage thư giãn như một thói quen. Vậy nên đến với đất nước Việt Nam có nhiều điểm khác biệt về văn hóa và khí hậu, Mr. Hurwizt vô cùng lo lắng về sức khỏe của mình. Cho đến khi một người đồng nghiệp giới thiệu đến J Medical Spa, ông đã có những trải nghiệm tuyệt vời ở đây.</p>
                                                    <p>Về sự khách biệt giữa chất lượng dịch vụ massage ở Mỹ và ở Việt Nam, Mr. Hurwizt cho biết massage tại J Medical Spa là sự kết hợp hoàn hảo từ massage cổ truyền Việt Nam và phương pháp massage hiện đại của phương Tây, rất tốt cho việc thư giãn và trị liệu những đau đớn, mệt mỏi gặp phải trong sinh hoạt hàng ngày.</p>
                                                    <p>Mr. Hurwizt rất vui vẻ cho biết ông sẽ rất hân hạnh giới thiệu dịch vụ tại J Medical Spa cho bạn bè và người thân đến trải nghiệm. Ông còn khẳng định rằng nếu ông là một người bản địa sẽ đến đây thường xuyên để duy trì sức khỏe tốt nhất.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                }
            </div>
        </div>
    )
}

export default Feedback