import React from 'react'
import Transformer from '../../constants/transformer'

function Title({ title, activeLang }) {
    return (
        <div className="row align-items-center">
            <div className="col-md-5">
                <div
                    className="fs-30 ff-montbold cl-blue"
                    data-aos="fade-left"
                    dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(title.name, activeLang))}></div>
            </div>
            <div className="col-md-6">
                <div data-aos="fade-right">
                    <p>{Transformer.parseJson(title.description, activeLang)}</p>
                </div>
                <div>
                    <img src="assets/images/star.png" className="w15" alt="" />
                    <img src="assets/images/star.png" className="w15" alt="" />
                </div>
            </div>
        </div>
    )
}


export default Title