import Transformer from '../../constants/transformer'
import {CONSTANT_TEXT} from '../../constants/config'
import Link from 'next/link'

function ServiceFAQ(props) {
    const { serviceFaq, activeLang } = props

    if (serviceFaq.length === 0) return null
    return (
        <div className="section7_sv py-5">
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <h3 className="fs-20 cl-blue mb-3 ff-montbold text-uppercase" data-aos="fade-right">{ CONSTANT_TEXT.QUESTIONS[activeLang] }<br></br>{ CONSTANT_TEXT.FREQUENT[activeLang] }</h3>
                    </div>
                    <div className="col-md-9">
                        <div className="accordion" id="FaQ_1" data-aos="fade-left">
                            {
                                serviceFaq.map((item, index) => {
                                    return (
                                        <div className="item_faq" key={index}>
                                            <div className="card-header" id={`heading_${index + 1}`}>
                                                <div className="mb-0">
                                                    <a data-toggle="collapse" data-target={`#collapse_${index + 1}`} aria-expanded="false" aria-controls={`collapse_${index + 1}`}>{Transformer.parseJson(item.question, activeLang)}</a>
                                                </div>
                                            </div>
                                            <div id={`collapse_${index + 1}`} className="collapse" aria-labelledby={`heading_${index + 1}`} data-parent="#FaQ_1">
                                                <div className="card-body">{Transformer.parseJson(item.answer, activeLang)}</div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ServiceFAQ