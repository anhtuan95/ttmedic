import { FILE_URL, URL_NULL_IMG } from '../../constants/config'
import Transformer from '../../constants/transformer'

function Effects(props) {
    const { serviceTitle, activeLang } = props
    if (serviceTitle === undefined) return null

    return (
        <div className="section3_sv py-md-5 py-3">
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <h3 className="fs-20 mb-4 cl-blue ff-montbold text-uppercase" data-aos="fade-right"
                            dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(serviceTitle.title, activeLang))}
                        >
                        </h3>
                    </div>
                    <div className="col-md-9">
                        <div className="row">
                            {
                                serviceTitle.serviceItem.map((item, index) => (
                                    <div className="col-md-3 col-6" key={index}>
                                        <div className="item_box" data-aos="fade-left" data-aos-delay={index * 200}>
                                            <figure>
                                                <img src={item.image === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.image}`} className="img-fluid lazyload" alt={Transformer.parseJson(item.alt, activeLang)} />
                                            </figure>
                                            <h4>{Transformer.truncate(Transformer.parseJson(item.name, activeLang), 60)}</h4>
                                            <p>{Transformer.truncate(Transformer.parseJson(item.description, activeLang), 160)}</p>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Effects
