import { FILE_URL } from '../../constants/config'
import Transformer from '../../constants/transformer'

function Section5(props) {
    const { serviceTitle, activeLang } = props

    if (serviceTitle === undefined) return null
    return (
        <div className="section5_sv py-5">
            <div className="container">
                <h3 className="fs-20 cl-blue mb-3 ff-montbold text-uppercase" data-aos="fade-right"><div dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(serviceTitle.title, activeLang))}></div></h3>
                <div className="slider_sv" data-aos="fade-left">
                    {
                        serviceTitle.serviceItem.map((item, index) => (
                            <div className="item" key={index}>
                                <img src={`${FILE_URL}${item.image}`} className="img-fluid lazyload" alt={Transformer.parseJson(item.alt, activeLang)} />
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}

export default Section5