import Transformer from '../../constants/transformer'

function Introduction(props) {
    const { serviceTitle, activeLang } = props

    if (serviceTitle === undefined) return null
    return (
        <div className="section2_sv py-md-5 py-3">
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <h3 className="fs-20 cl-blue mb-4 ff-montbold text-uppercase" data-aos="fade-right">{Transformer.parseJson(serviceTitle.title, activeLang)}</h3>
                    </div>
                    <div className="col-md-9">
                        <div className="content_box" data-aos="fade-left">
                            {
                                serviceTitle.serviceItem.map((item, index) => (
                                    <p key={index}>{Transformer.parseJson(item.description, activeLang)}</p>
                                ))
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Introduction