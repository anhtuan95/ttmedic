import React from 'react'
import Transformer from "../../constants/transformer"
import { FILE_URL, URL_NULL_IMG } from "../../constants/config"
import tranformer from '../../constants/transformer'
import Link from 'next/link'

function ListService(props) {
    const { serviceExperience, activeLang, menuTop } = props

    return (
        <div className="all_services">
            <hr />
            <div className="container">
                <h3>
                    <div className="fs-30 cl-blue"
                        dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(serviceExperience.name, activeLang))}>
                    </div>
                </h3>
            </div>
            <hr />
            <div className="list_services">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-11">
                            <div className="row justify-content-center">
                                {menuTop && menuTop.children &&
                                    menuTop.children.map(item => (
                                        <div className="col-md-4 col-10" key={item.id}>
                                            <div className="item_service">
                                                <div className="icon_sv mb-3">
                                                    <img src={item.data.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.data.images}`} className="img-fluid lazyload" alt="" />
                                                </div>
                                                <h4 className="ff-mulibold fs-14 cl-blue">{`${Transformer.parseJson(item.data.name, activeLang)}`}</h4>
                                                <ul>
                                                    {
                                                        item.children &&
                                                        item.children.map(child => (
                                                            <li key={child.id}>
                                                                <Link href={tranformer.redirectLink(child.data.seoLink)}>
                                                                    <a>{`${Transformer.parseJson(child.text, activeLang)}`}</a>
                                                                </Link>
                                                            </li>
                                                        ))
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ListService