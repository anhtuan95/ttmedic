
import Transformer from '../../constants/transformer'

function Title(props) {
    const { serviceDetail, activeLang } = props
    return (
        <div className="section1_sv py-md-5 py-3">
            <div className="container">
                <div
                    className="fs-30 text-center ff-montbold cl-blue text-uppercase"
                    data-aos="fade-left"
                    dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(serviceDetail.description, activeLang))}>
                </div>
            </div>
        </div>
    )
}

export default Title