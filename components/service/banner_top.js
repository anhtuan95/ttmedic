import React from 'react';
import { CONSTANT_TEXT, FILE_URL, URL_NULL_IMG } from '../../constants/config';
import Transformer from '../../constants/transformer';

function BannerTopService(props) {
    const { serviceBanner, activeLang } = props;
    return (
        <div className="service_hero py-md-5 pt-3">
            <div className="container">
                <div className="content_hero py-md-5 py-0">
                    <figure>
                        <img src={serviceBanner.image !== null
                            ? `${FILE_URL}${serviceBanner.image}`
                            : `${FILE_URL}${URL_NULL_IMG}`} className="img-fluid lazyload"
                            alt={Transformer.parseJson(serviceBanner.alt, activeLang)} />
                    </figure>
                    <div className="row">
                        <div className="col-md-3">
                            <h3 className="fs-30 ff-montbold cl-blue">
                                {CONSTANT_TEXT.SERVICE[activeLang]}
                            </h3>
                        </div>
                        <div className="col-md-3">
                            <div className="fs-14 cl-blue ff-mulibold">{Transformer.parseJson(serviceBanner.name, activeLang)}</div>
                            <div>
                                <img src="assets/images/star.png" className="w15" alt="" />
                                <img src="assets/images/star.png" className="w15" alt="" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <p>{Transformer.parseJson(serviceBanner.description, activeLang)}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default BannerTopService
    ;
