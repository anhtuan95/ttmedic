import { FILE_URL, URL_NULL_IMG } from '../../constants/config'
import Transformer from '../../constants/transformer'

function Section4(props) {
    const { serviceTitle, activeLang } = props

    if (serviceTitle === undefined) return null
    return (
        <div className="section4_sv py-md-5 py-3">
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <h3 className="fs-20 mb-4 cl-blue ff-montbold text-uppercase" data-aos="fade-right"><div dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(serviceTitle.title, activeLang))}></div></h3>
                    </div>
                    <div className="col-md-9">
                        {
                            serviceTitle.serviceItem.map((item, index) => (
                                <div className="item_box" data-aos="fade-left" key={index}>
                                    <div className="row align-items-center">
                                        <div className="col-md-3 col-4">
                                            <div className="icon_box">
                                                <img src={item.image === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.image}`} className="img-fluid lazyload" alt={Transformer.parseJson(item.alt, activeLang)} />
                                            </div>
                                        </div>
                                        <div className="col-md-9 col-8">
                                            <h4>{Transformer.truncate(Transformer.parseJson(item.name, activeLang), 75)}</h4>
                                            <div dangerouslySetInnerHTML={Transformer.createHTML(Transformer.truncate(Transformer.parseJson(item.description, activeLang), 400))}></div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Section4