import { FILE_URL, URL_NULL_IMG } from '../../constants/config'
import Transformer from '../../constants/transformer'

function Cover(props) {
    const { serviceDetail, activeLang } = props
    return (
        <div className="section_cover" data-aos="fade-up">
            <img src={serviceDetail.image !== null ?
                `${FILE_URL}${serviceDetail.image}` :
                `${FILE_URL}${URL_NULL_IMG}`} className="img-fluid lazyload"
                alt={Transformer.parseJson(serviceDetail.alt, activeLang)} />
        </div>
    )
}

export default Cover