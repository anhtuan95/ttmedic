import { FILE_URL, URL_NULL_IMG } from '../../constants/config'
import Link from 'next/link'
import Transformer from '../../constants/transformer'

function Services(props) {
    const { serviceMenuTree, serviceDetail, activeLang } = props

    return (
        <div className="section8_sv py-md-0 py-5">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-md-6 py-3">
                        <div data-aos="fade-right">
                            <div className="row">
                                <div className="col-md-4">
                                    <h3 className="text-gold mb-4 ff-montbold fs-20">{Transformer.parseJson(serviceMenuTree.name, activeLang)}</h3>
                                    <div className="p-4 stamp">
                                        <img src="../assets/images/stamp2.png" className="img-fluid lazyload" alt="" />
                                    </div>
                                </div>
                                <div className="col-md-8 col-12">
                                    <div className="row">
                                        {
                                            serviceMenuTree.menuItems.map((item, index) => (
                                                <div className="col-md-6 col-6" key={index}>
                                                    <h4>
                                                        <Link href={item.data.objectId ? `/dich-vu/${item.data.objectId}` : Transformer.redirectLink(item.data.url)}>
                                                            <a>{Transformer.truncate(Transformer.parseJson(item.text, activeLang), 20)}</a>
                                                        </Link>
                                                    </h4>
                                                    {
                                                        item.childCount > 0 &&
                                                        <ul>
                                                            {
                                                                item.children.map((child, index) => (
                                                                    <li key={index}>
                                                                        <Link href={child.data.objectId ? `/dich-vu/${child.data.objectId}` : Transformer.redirectLink(child.data.url)}>
                                                                            <a >{Transformer.truncate(Transformer.parseJson(child.text, activeLang), 25)}</a>
                                                                        </Link>
                                                                    </li>
                                                                ))
                                                            }
                                                        </ul>
                                                    }
                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="service_img mt-md-0 mt-4" data-aos="fade-left">
                            <img src={serviceDetail.thumbnail !== "" ? `${FILE_URL}${serviceDetail.thumbnail}` : `${FILE_URL}${URL_NULL_IMG}`} className="img-fluid lazyload" alt={Transformer.parseJson(serviceDetail.alt, activeLang)} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Services