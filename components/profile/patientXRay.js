import React, { useEffect } from 'react';
// import { FILE_URL_TTMEDIC, DATE_FORMAT } from '../../constants/config';
import PropTypes from 'prop-types';
import moment from 'moment';

export default function PatientXRay({ patientXRayImages }) {
    useEffect(() => {
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 5,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            breakpoints: {
                0: {
                    slidesPerView: 3
                },
                992: {
                    slidesPerView: 4
                }
            }
        });

        var galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            thumbs: {
                swiper: galleryThumbs
            }
        });

        // change carousel item height
        // gallery-top
        const productCarouselTopWidth = $('.gallery-top').outerWidth();
        $('.gallery-top').css('height', productCarouselTopWidth);

        // gallery-thumbs
        const productCarouselThumbsItemWith = $('.gallery-thumbs .swiper-slide').outerWidth();
        $('.gallery-thumbs').css('height', productCarouselThumbsItemWith);

        // activation zoom plugin
        var $easyzoom = $('.easyzoom').easyZoom();
    });

    if (patientXRayImages.length === 0) return null;
    return (
        <div className="col-md-8">
            {/* END PAGE HEADER */}
            <div className="portfolio-content portfolio-1">
                <div className="product__carousel">
                    <div className="swiper-container gallery-top">
                        <div className="swiper-wrapper">
                            {
                                patientXRayImages.map((item, index) => (
                                    <div className="swiper-slide easyzoom easyzoom--overlay" key={index}>
                                        <a href={`data:image/png;base64,${item.image}`}>
                                            <img src={`data:image/png;base64,${item.image}`} alt="" />
                                            {/* <img src={`${FILE_URL_TTMEDIC}${item.duongDan}`} alt /> */}
                                        </a>
                                    </div>
                                ))
                            }
                        </div>
                        {/* Add Arrows */}
                        <div className="swiper-button-next swiper-button-white" />
                        <div className="swiper-button-prev swiper-button-white" />
                    </div>
                    <div className="swiper-container gallery-thumbs">
                        <div className="swiper-wrapper">
                            {
                                patientXRayImages.map((item, index) => (
                                    <div className="swiper-slide" key={index}>
                                        <img src={`data:image/png;base64,${item.image}`} alt="" />
                                        <small>{moment(item.ngayTao).format('DD/MM/YYYY')}</small>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                    {/* Swiper and EasyZoom plugins end */}
                </div>
            </div>
        </div>
    );
}

PatientXRay.propTypes = {
    patientXRayImages: PropTypes.array
};
