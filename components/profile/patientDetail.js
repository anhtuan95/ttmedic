import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

export default function PatientDetail(props) {
    const { patientDetail } = props;
    // if (patientDetail === {}) return null;
    return (
        <div className="col-md-4">
            <div className="profile-sidebar">
                {/* PORTLET MAIN */}
                <div className="portlet light profile-sidebar-portlet ">
                    {/* SIDEBAR USER TITLE */}
                    <div className="profile-usertitle">
                        <div className="profile-usertitle-name">{patientDetail.hoTenBenhNhan}</div>
                        <small>Mã BN: {patientDetail.maBenhNhan}</small>
                    </div>
                    {/* END SIDEBAR USER TITLE */}
                    <div>
                        <h4 className="profile-desc-title">Thông tin</h4>

                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-phone" /> Điện thoại: {patientDetail.soDienThoai}
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-envelope" /> Email: {patientDetail.soDienThoai}
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-birthday-cake" /> Ngày sinh: {moment(patientDetail.ngayThangNamSinh).format('DD/MM/YYYY')}
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-calendar" /> Tuổi: {patientDetail.tuoi}
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-male" /> Giới tính: {patientDetail.maGioiTinh ? 'Nam' : 'Nữ'}
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-map" /> Địa chỉ: {patientDetail.diaChi}
                        </div>
                        <div className="margin-top-20 profile-desc-link">
                            <i className="fa fa-facebook" /> Facebook: {patientDetail.facebook}
                        </div>
                    </div>
                </div>
                {/* END PORTLET MAIN */}
            </div>
        </div>
    );
}

PatientDetail.propTypes = {
    patientDetail: PropTypes.any
};
