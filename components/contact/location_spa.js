import { FILE_URL, CONSTANT_TEXT, URL_NULL_IMG } from '../../constants/config'
import { Component } from 'react'
import Transformer from '../../constants/transformer'
import Link from 'next/link';

class LocationSpa extends Component {

    componentDidMount() {
        this.sliderContact();
    }

    sliderContact = () => {
        const { branchContacts } = this.props
        branchContacts.map((item, index) => {
            $(`.slider_location${index + 1}`).slick({
                slidesToShow: 1,
                autoplay: true,
                autoplaySpeed: 5000,
                speed: 1500,
                infinite: true,
                dots: true,
                prevArrow: jQuery(`.slider${index + 1}_prev`),
                nextArrow: jQuery(`.slider${index + 1}_next`),
            });
        })
    }

    render() {
        const { branchContacts, activeLang } = this.props

        return (
            <div>
                {
                    branchContacts.map((item, index) => {
                        return (
                            <div className="location_spa pb-md-5" data-aos="fade-up" key={index}>
                                <div className="title_location">
                                    <div className="container">
                                        <div className="bg-white">
                                            <div className="row mx-0 align-items-center">
                                                <div className="col-md-2 px-0">
                                                    <div className="brand_name"><span>{CONSTANT_TEXT.FACILITY[activeLang]}</span> {index + 1}</div>
                                                </div>
                                                <div className="col-md-3 px-0">
                                                    <div className="brand_address">
                                                        <strong>A:</strong> <span>{Transformer.parseJson(item.address, activeLang)}</span>
                                                    </div>
                                                </div>
                                                <div className="col-md-3 px-0">
                                                    <div className="brand_phone">
                                                        <strong>P:</strong> <span>{item.branchContactDetails.map(detail => detail.contactType === 1 ? Transformer.formatPhoneDisplay(detail.contactValue) : '')}</span>
                                                    </div>
                                                </div>
                                                <div className="col-md-2 col-6 px-0">
                                                    <Link href={Transformer.redirectLink(item.googleMap)}>
                                                        <a className="map_contact">Google map</a>
                                                    </Link>
                                                </div>
                                                <div className="col-md-1 col-3 px-0">
                                                    <div className={`slider${index + 1}_prev arrow_nav`}>
                                                        <img src={`assets/images/prev.png`} className="img-fluid lazyload" alt="" />
                                                    </div>
                                                </div>
                                                <div className="col-md-1 col-3 px-0">
                                                    <div className={`slider${index + 1}_next arrow_nav`}>
                                                        <img src={`assets/images/next.png`} className="img-fluid lazyload" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={`slider_location slider_location${index + 1}`}>
                                    {
                                        item.branchContactImages.map((image, index) => {
                                            return (
                                                <div className="item" key={index}>
                                                    <img src={image.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${image.images}`} className="img-fluid lazyload" alt={Transformer.parseJson(image.alt, activeLang)} />
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}


export default LocationSpa