import { FILE_URL } from '../../constants/config';
import Transformer from '../../constants/transformer';

function MoreInfo(props) {
    const { moreInfoHC, activeLang } = props;

    return (
        <div className="more_info">
            {
                moreInfoHC.hardContentDetail.map((item, index) => {
                    return (
                        <div className="box_section" key={index}>
                            <div className="bg_img">
                                <img src={`${FILE_URL}${moreInfoHC.hardContentDetail[index].image}`} className="img-fluid lazyload" alt="" />
                            </div>
                            <div className="container">
                                <div className={`row ${(index % 2 !== 0) ? 'justify-content-end' : ''}`}>
                                    <div className="col-md-6">
                                        <div className="p-md-5" data-aos="fade-left">
                                            <h3 className="fs-20 mb-md-5 mb-3 ff-mulibold cl-blue text-uppercase">{Transformer.parseJson(moreInfoHC.hardContentDetail[index].name, activeLang)}</h3>
                                            {
                                                <p >{Transformer.parseJson(moreInfoHC.hardContentDetail[index].description, activeLang)}</p>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })
            }
        </div>
    );
}

export default MoreInfo
    ;
