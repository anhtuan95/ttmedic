import api from '../../constants/api';
import Transformer from '../../constants/transformer';
import { CONSTANT_TEXT } from '../../constants/config';
import Link from 'next/link';
import PropTypes from 'prop-types';

function FormContact(props) {
    const { socialNetworks, contactHC, contactJSpaHC, activeLang } = props;
    return (
        <div className="form_contact pt-5">
            <div className="container">
                <div className="row">
                    <div className="col-md-2">
                        <h3 className="fs-30 ff-montbold cl-blue" data-aos="fade-left">{Transformer.parseJson(contactHC.name, activeLang)}</h3>
                    </div>
                    <div className="col-md-2 col-6">
                        <div data-aos="fade-up">
                            <div className="fs-14 ff-montbold cl-blue text-uppercase">
                                {Transformer.parseJson(contactHC.hardContentDetail[0].name, activeLang)}
                            </div>
                            {Transformer.parseJson(contactHC.hardContentDetail[0].description, activeLang)}
                        </div>
                    </div>
                    <div className="col-md-2 col-6">
                        <div data-aos="fade-up">
                            <div className="fs-14 ff-montbold cl-blue text-uppercase">
                                {Transformer.parseJson(contactHC.hardContentDetail[1].name, activeLang)}
                            </div>
                            {Transformer.parseJson(contactHC.hardContentDetail[1].description, activeLang)}
                        </div>
                    </div>
                    <div className="col-md-2">
                        <ul className="list-inline social mt-3 mt-md-0" data-aos="fade-up">
                            {
                                socialNetworks.map((item, index) => {
                                    return (
                                        <li className="list-inline-item" key={index}>
                                            <Link href={Transformer.redirectLink(item.url)}><a><i className={`fa fa-${item.icon}`} aria-hidden="true"></i></a></Link>
                                        </li>
                                    );
                                })
                            }
                        </ul>
                    </div>
                    <div className="col-md-2">
                        <a data-toggle="modal" data-target="#bookingnow" data-aos="fade-up" className="btn btn-blue mt-3 mt-md-0">{CONSTANT_TEXT.SCHEDULE[activeLang]}</a>
                    </div>
                </div>
                <div className="row justify-content-end mt-md-5">
                    <div className="col-md-10">
                        <div className="mb-2 mt-4 mt-md-0 fs-14 ff-montbold cl-blue text-uppercase" data-aos="fade-left">
                            {CONSTANT_TEXT.FREE_CONSULTATION[activeLang]}
                        </div>
                        <div data-aos="fade-right">
                            <div className="row justify-content-between align-items-center">
                                <div className="col-md-6">
                                    <input type="text" className="form-control mb-3" placeholder={CONSTANT_TEXT.CUSTOMER_NAME[activeLang]} id="customerName" />
                                </div>
                                <div className="col-md-6">
                                    <input type="number" className="form-control mb-3" placeholder={CONSTANT_TEXT.PHONE_NUMBER[activeLang]} id="phoneNumber" autoComplete={'off'} />
                                </div>
                                <div className="col-md-6">
                                    <input type="text" className="form-control mb-3" placeholder="Email" id="feedbackEmail" />
                                </div>
                                <div className="col-md-6">
                                    <select name="" id="title" className="form-control">
                                        <option value="TƯ VẤN QUA ĐIỆN THOẠI" className="text-uppercase">{CONSTANT_TEXT.CONSULTATION_PHONE[activeLang]}</option>
                                        <option value="TƯ VẤN QUA EMAIL" className="text-uppercase">{CONSTANT_TEXT.CONSULTATION_EMAIL[activeLang]}</option>
                                    </select>
                                </div>
                                <div className="col-md-6">
                                    <textarea name="" className="form-control" rows="2" placeholder={CONSTANT_TEXT.CONSULTATION_CONTENT[activeLang]} id="content"></textarea>
                                </div>
                                <div className="col-md-6">
                                    <input type="submit" className="btn btn-blue" value={CONSTANT_TEXT.SEND[activeLang]} onClick={() => sendFeedback()} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row py-md-5">
                    <div className="col-md-4">
                        <div className="fs-30 mt-4 mt-md-0 cl-blue ff-montbold" data-aos="fade-up">{Transformer.parseJson(contactJSpaHC.name, activeLang)}</div>
                    </div>
                    <div className="col-md-4">
                        <div className="mb-3 mb-md-0" data-aos="fade-left">
                            <p>{Transformer.parseJson(contactJSpaHC.description, activeLang)}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

async function sendFeedback() {
    const fullName = document.getElementById('customerName').value;
    let phoneNumber = document.getElementById('phoneNumber').value;
    const email = document.getElementById('feedbackEmail').value;
    const title = document.getElementById('title').value;
    const content = document.getElementById('content').value;

    if (phoneNumber.charAt(0) === '+') {
        phoneNumber = phoneNumber.substring(1);
    }

    if (!Transformer.validateCustomerName(fullName)) {
        document.getElementById('customerName').focus();
        alert('Customer name can not contain number or special character');
        return;
    }

    if (!Transformer.validateEmail(email.trim())) {
        document.getElementById('feedbackEmail').focus();
        alert('You have entered an invalid email address!');
        return;
    }

    if (!Transformer.validatePhoneNumber(phoneNumber).result) {
        document.getElementById('phoneNumber').focus();
        alert(Transformer.validatePhoneNumber(phoneNumber).message);
        return;
    }

    const insertFeedback = await api.post('Feedbacks', null, {
        fullName: fullName.trim(),
        email: email.trim(),
        phoneNumber,
        title,
        content,
        description: null,
        isView: false
    });

    alert(insertFeedback.message);
    if (insertFeedback.code === 1) {
        document.getElementById('customerName').value = '';
        document.getElementById('phoneNumber').value = '';
        document.getElementById('feedbackEmail').value = '';
        document.getElementById('content').value = '';
    }
}

FormContact.propTypes = {
    socialNetworks: PropTypes.any,
    contactHC: PropTypes.any,
    contactJSpaHC: PropTypes.any,
    activeLang: PropTypes.any
};

export default FormContact;
