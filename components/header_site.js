import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { LANGUAGENAMES } from '../constants/config';
// import Transformer from '../constants/transformer';
// import FilterSearch from '../components/search/filter_search';
// import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import api from '../constants/api';
import PropTypes from 'prop-types';

function HeaderSite({ routerName, setting, menuTop, branchContacts, socialNetworks, languages, activeLang, totalQuantity, updateActiveLanguage, idPath }) {
    // const activeLanguage = languages.find(x => x.languageId === activeLang);
    // const activeLangIcon = activeLanguage ? activeLanguage.icon : languages[0].icon;
    const [isLogin, setIsLogin] = useState(false);
    const router = useRouter();
    useEffect(() => {
        const Login = localStorage.getItem('access_token');
        if (Login !== 'null') {
            setIsLogin(true);
        }
    });
    // const selectLanguage = (languageId) => {
    //     router.push(router.pathname, router.asPath, { locale: LANGUAGENAMES[[languageId]] });
    // };

    const logout = () => {
        console.log('logout');
        api.logout();
        localStorage.setItem('access_token', null);
        setIsLogin(false);
        router.push('/');
    };

    return (
        <React.Fragment>
            <header>
                <div className="container py-2">
                    <div className="row align-items-center ">
                        <div className="col-md-2">
                            <Link href="/" locale={LANGUAGENAMES[[activeLang]]}>
                                <a className="nav-link"><img src="assets/images/logo.png" alt height="60" /></a>
                            </Link>
                        </div>
                        <div className="col-md-10">
                            <ul className="navbar-nav  header-info">
                                <li className="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                    <ul className="hotline">
                                        <li>
                                            <b><i className="fa fa-headphones"></i> TỔNG ĐÀI</b><br />
                                           0243855522 / 024 73096888
                                        </li>
                                        <li>
                                            <b><i className="fa fa-phone"></i> HOTLINE</b><br />
                                          0972881125
                                        </li>
                                        <li>
                                            <b><i className="fa fa-clock-o"></i> GIỜ LÀM VIỆC</b><br />
                                           7:00 | T2 - CN
                                        </li>
                                    </ul>
                                </li>
                                <li className="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                    <ul className="social-icons social-icons-color pt-2">
                                        <li><a href="javascript:;" data-original-title="facebook" className="facebook"></a></li>
                                        <li><a href="javascript:;" data-original-title="youtube" className="youtube"></a></li>
                                    </ul>
                                </li>
                                <li className="nav-item pl-4 pl-md-0 ml-0 ml-md-4 pt-2">
                                    <Link href={isLogin ? '/profile' : '/login'} locale={LANGUAGENAMES[[activeLang]]}>
                                        {
                                            isLogin
                                                ? <a><i className="fa fa-user" aria-hidden="true"></i></a>
                                                : <a className="btn btn-circle  green-meadow btn-sm">Đăng nhập</a>
                                        }

                                    </Link>
                                    &nbsp;
                                    {
                                        isLogin &&
                                        <a className="btn btn-circle  yellow-crusta btn-sm" onClick={logout}>Đăng xuất</a>
                                    }
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <div className="navigation-wrap start-header start-style ">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <nav className="navbar navbar-expand-md mb-0">
                                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="navbar-toggler-icon" />
                                </button>
                                <div className="collapse navbar-collapse p-0" id="navbarSupportedContent">
                                    <ul className="navbar-nav ml-auto py-4 py-md-0 menu">
                                        <li className="nav-item px-3 py-3">
                                            <Link href="/" locale={LANGUAGENAMES[[activeLang]]}>
                                                <a className="nav-link">Trang chủ</a>
                                            </Link>
                                        </li>
                                        <li className="nav-item px-3 py-3">
                                            <Link href="/" locale={LANGUAGENAMES[[activeLang]]}>
                                                <a className="nav-link">Giới thiệu</a>
                                            </Link>
                                        </li>
                                        <li className="nav-item px-3 py-3">
                                            <Link href="/" locale={LANGUAGENAMES[[activeLang]]}>
                                                <a className="nav-link">Chuyên khoa</a>
                                            </Link>
                                        </li>
                                        <li className="nav-item px-3 py-3">
                                            <Link href="/" locale={LANGUAGENAMES[[activeLang]]}>
                                                <a className="nav-link">Dịch vụ hỗ trợ</a>
                                            </Link>
                                        </li>
                                        <li className="nav-item px-3 py-3">
                                            <Link href="/" locale={LANGUAGENAMES[[activeLang]]}>
                                                <a className="nav-link">Tin tức</a>
                                            </Link>
                                        </li>
                                        <li className="nav-item px-3 py-3">
                                            <Link href="/" locale={LANGUAGENAMES[[activeLang]]}>
                                                <a className="nav-link">Tuyển dụng</a>
                                            </Link>
                                        </li>
                                        <li className="nav-item px-3 py-3">
                                            <Link href="/" locale={LANGUAGENAMES[[activeLang]]}>
                                                <a className="nav-link">Liên hệ</a>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

HeaderSite.propTypes = {
    setting: PropTypes.any,
    locale: PropTypes.string,
    banners: PropTypes.any,
    socialNetworks: PropTypes.any,
    languages: PropTypes.any,
    updateCartQuantity: PropTypes.any,
    activeLang: PropTypes.any,
    branchContacts: PropTypes.any,
    menuTop: PropTypes.any,
    routerName: PropTypes.any,
    totalQuantity: PropTypes.any,
    updateActiveLanguage: PropTypes.any,
    idPath: PropTypes.any
};

export default HeaderSite;
