import React, { useEffect } from 'react'
import { FILE_URL } from '../../constants/config'
import Transformer from '../../constants/transformer'
function SliderBanner(props) {
    const { productSlide, activeLang } = props
    const relatedBannerCarousel = () => {
        jQuery('.slider_banner_product').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            infinite: true,
            arrows: true,
            prevArrow: '<div class="prev-slider"><span></span></div>',
            nextArrow: '<div class="next-slider"><span></span></div>',
        });
    }

    useEffect(() => {
        relatedBannerCarousel()
    }, [])
    return (
        <div>
            {
                productSlide && productSlide.hardContentDetail.length > 0 ?
                    <div className="slider_banner_product">
                        {
                            productSlide.hardContentDetail.map((item, index) => (
                                <div className="item" key={index}>
                                    <img src={`${FILE_URL}${item.image}`} className="img-fluid lazyload" alt={Transformer.parseJson(item.alt, activeLang)} />
                                </div>
                            ))
                        }
                    </div> :
                    <div className="slider_banner_product">
                        <div className="item">
                            <img src="assets/images/slide_product.jpg" className="img-fluid lazyload" alt="" />
                        </div>
                    </div>
            }
        </div>
    )
}

export default SliderBanner