import React, { Component } from 'react'
import { CONSTANT_TEXT, FILE_URL } from '../../constants/config'
import Transformer from '../../constants/transformer'
import Link from 'next/link'

class ProductList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            favorite: [],

        }
    }
    updateFavorite = (e, productId) => {
        const { updateFavoriteProduct, isFavorite } = this.props
        e.preventDefault()
        Transformer.favoriteProduct(e, productId)
        let favorite = localStorage.getItem('favorite')
        if (favorite) {
            favorite = JSON.parse(favorite)
            this.setState({ favorite })
        }
        if (isFavorite) {
            updateFavoriteProduct(favorite)
        }
    }

    addToCart = (e, product) => {
        e.preventDefault()
        Transformer.addToCart(e, product)
        this.props.updateCartQuantity()
    }

    render() {
        const { products, activeLang, favoriteProduct } = this.props
        return (
            <div className="filter_content" id="search_list">
                <div className="row">
                    {
                        products.length > 0 &&
                        products.map((product, index) => {
                            return (
                                <div className="col-md-4" key={index}>
                                    <div className="item_product" data-aos="fade-up">
                                        <div className="wrap_product">
                                            <figure>
                                                <img src={product.images === null ? `${FILE_URL}uploads/images/no-image.png` : `${FILE_URL}${product.images}`} className="img-fluid lazyload" alt="" />
                                                <div className="add_to_cart">
                                                    <a onClick={(e) => this.addToCart(e, product)}><i className="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                                </div>
                                                <div className="white_list">
                                                    <a onClick={(e) => this.updateFavorite(e, product.productId)}>
                                                        <i className={`fa fa-heart ${favoriteProduct.includes(product.productId) && 'redIcon'} `} aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </figure>
                                            <h4 style={{ wordBreak: 'break-word' }}>{Transformer.truncate(product.title, 35)}</h4>
                                            <div className="product_cate">- {Transformer.truncate(product.description, 65)} -</div>
                                            <div className="price">{Transformer.convertCurrency(product.salePrice)}</div>
                                            <Link href={`/${product.seoLink}`}>
                                                <a href={`/${product.seoLink}`} className="btn btn-blue">{CONSTANT_TEXT.READ_MORE[activeLang]}</a>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default ProductList