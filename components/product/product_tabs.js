import React from 'react';
import { CONSTANT_TEXT } from '../../constants/config';
import PropTypes from 'prop-types';

function ProductTabs({ activeLang }) {
    return (
        <div className="menu_tab mt-5 mb-md-3 py-2">
            <div className="container">
                <ul className="nav nav-tabs nav-fill" id="myTab" role="tablist">
                    <li className="nav-item" role="presentation">
                        <a href="#detail" className="nav-link active" id="detail-tab" data-toggle="tab" role="tab"
                            aria-controls="detail" aria-selected="true">{CONSTANT_TEXT.DETAILS[activeLang]}</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a href="#uses" className="nav-link" id="uses-tab" data-toggle="tab" role="tab" aria-controls="uses"
                            aria-selected="false">{CONSTANT_TEXT.USES[activeLang]}</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a href="#elements" className="nav-link" id="ingredient-tab" data-toggle="tab" role="tab"
                            aria-controls="ingredient" aria-selected="false">{CONSTANT_TEXT.ELEMENTS[activeLang]}</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a href="#user_manual" className="nav-link" id="guide-tab" data-toggle="tab" role="tab" aria-controls="guide"
                            aria-selected="false">{CONSTANT_TEXT.USER_MANUAL[activeLang]}</a>
                    </li>
                </ul>
            </div>
        </div>
    );
}

ProductTabs.propTypes = {
    activeLang: PropTypes.any
};

export default ProductTabs;
