import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { CONSTANT_TEXT, FILE_URL, URL_NULL_IMG } from '../../constants/config'
import Transformer from '../../constants/transformer'
import { PrevArrow, NextArrow } from '../common/slideArrow'

function RelatedProducts(props) {
    const { relatedProducts, activeLang, updateCartQuantity } = props
    const [productFavorite, setProductFavorite] = useState([])
    const slideToShows = (relatedProducts.length < 4) ? relatedProducts.length : 4
    const relatedProductsCarousel = () => {
        jQuery('.product_related').slick({
            slidesToShow: 4,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            infinite: false,
            arrows: true,
            prevArrow: '<div class="prev-slider"><span></span></div>',
            nextArrow: '<div class="next-slider"><span></span></div>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    }

    useEffect(() => {
        let favorite = localStorage.getItem('favorite')
        if (favorite) {
            favorite = JSON.parse(favorite)
            setProductFavorite([...favorite])
        }
        relatedProductsCarousel()
    }, [])
    const addToCart = async (e, product) => {
        e.preventDefault()
        Transformer.addToCart(e, product)
        updateCartQuantity()
    }

    const updateFavorite = async (e, productId) => {
        e.preventDefault()
        Transformer.favoriteProduct(e, productId)
        let favorite = localStorage.getItem('favorite')
        if (favorite) {
            favorite = JSON.parse(favorite)
            setProductFavorite([...favorite])
        }
    }

    return (
        <div className="box_related mt-md-5">
            <h3 className="mb-4 fs-30 ff-montbold cl-blue text-uppercase text-center">{CONSTANT_TEXT.RELATED_PRODUCTS[activeLang]}</h3>
            <div className="product_related">
                {
                    relatedProducts && relatedProducts.map((product, index) => (
                        <div className="item_product" key={index}>
                            <div className="wrap_product">
                                <figure>
                                    <img src={product.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${product.images}`} className="img-fluid lazyload" alt={product.alt} />
                                    <div className="add_to_cart">
                                        <a onClick={(e) => addToCart(e, product)}><i className="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                    </div>
                                    <div className="white_list">
                                        <a onClick={(e) => updateFavorite(e, product.productId)}>
                                            <i className={`fa fa-heart ${productFavorite.includes(product.productId) && 'redIcon'} `} aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </figure>
                                <h4 style={{ wordBreak: 'break-word' }}>{Transformer.truncate(product.title, 35)}</h4>
                                <div className="product_cate">- {Transformer.truncate(product.description, 60)} -</div>
                                <div className="price">{Transformer.convertCurrency(product.salePrice)}</div>
                                <Link href={`/${product.seoLink}`}>
                                    <a className="btn btn-blue">{CONSTANT_TEXT.READ_MORE[activeLang]}</a>
                                </Link>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default RelatedProducts