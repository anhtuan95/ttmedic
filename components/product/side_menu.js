import React, { Component } from 'react'
import Link from 'next/link'
import Transformer from '../../constants/transformer'

class SideMenu extends Component {
    constructor(props) {
        super(props)

        this.state = {
            sideMenu: {},
            idPath: [],
            selectedId: '',
        }
    }

    componentDidMount() {
        const { productMenu, selectedId, sideMenu } = this.props
        let idPath = []
        if (productMenu) {
            productMenu.menuItems.map(item => {
                if (item.objectId === selectedId) {

                    idPath = item.idPath ? item.idPath.split('.') : []
                    this.setState({
                        idPath,
                    })
                }
            })
        }

        this.setState({
            selectedId,
            sideMenu
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selectedId !== this.props.selectedId) {
            const { productMenu, selectedId, sideMenu } = nextProps
            let idPath = []
            if (productMenu) {
                productMenu.menuItems.map(item => {
                    if (Number(item.objectId) === selectedId) {
                        idPath = item.idPath ? item.idPath.split('.') : []
                        this.setState({
                            idPath,
                        })
                    }
                })
            }
            this.setState({
                selectedId,
                sideMenu
            })
        }
    }

    getNamePath = (namePath) => {
        let namePathArr = namePath.split("/")
    }

    renderRecursiveMenu = (menuData, selectedId, activeLang, isFavorite) => {
        return (
            <ul>
                {
                    menuData.map((item, index) => {
                        let itemLink = item.data.objectType !== 0 ? item.data.seoLink : Transformer.redirectLink(item.data.url)
                        return (
                            <li className={`${item.childCount > 0 ? ' has-child' : ''}${this.state.idPath.includes(String(item.id)) ? ' active' : ''}`} key={index}>
                                <Link href={isFavorite ? { pathname: 'san-pham-yeu-thich', query: { productsCategoryId: item.data.objectId } } : itemLink}>
                                    <a data-id={item.data.objectId}>{`${Transformer.parseJson(item.data.name, activeLang)}`}</a>
                                </Link>
                                {(item.children.length > 0) && this.renderRecursiveMenu(item.children, selectedId, activeLang, isFavorite)}
                            </li>
                        )
                    })
                }
            </ul>
        )
    }

    render() {
        const { sideMenu, selectedId } = this.state
        const { activeLang, isFavorite } = this.props
        return (
            <React.Fragment>
                <ul className="list_cate_product">
                    {
                        sideMenu && sideMenu.menuItems &&
                        sideMenu.menuItems.map((item, index) => {
                            let itemLink = item.data.objectType !== 0 ? item.data.seoLink : Transformer.redirectLink(item.data.url)
                            return (
                                <li className={`${item.childCount > 0 ? ' has-child' : ''}${this.state.idPath.includes(String(item.id)) ? ' active' : ''}`} key={index}>
                                    <Link href={isFavorite ? { pathname: 'san-pham-yeu-thich', query: { productsCategoryId: item.data.objectId } } : itemLink}>
                                        <a>{`${Transformer.parseJson(item.data.name, activeLang)}`}</a>
                                    </Link>
                                    {item.childCount > 0 && this.renderRecursiveMenu(item.children, selectedId, activeLang, isFavorite)}
                                </li>
                            )
                        })
                    }
                </ul>
                <div>Khoảng giá</div>
            </React.Fragment>
        )
    }
}

export default SideMenu