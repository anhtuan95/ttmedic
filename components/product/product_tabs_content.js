import React from 'react';

function createHTML(string) {
    return { __html: string };
}

function ProductTabsContent(props) {
    const { productDetail } = props;

    return (
        <div className="tab-content" id="myTabContent">
            <div
                className="tab-pane fade show active"
                id="detail"
                role="tabpanel"
                aria-labelledby="home-tab">
                <div className="read-more-less" data-id="700">
                    <div className=" read-toggle" data-id='0'>
                        <article className="article" dangerouslySetInnerHTML={createHTML(productDetail.detail)} />
                    </div>
                </div>
            </div>
            <div
                className="tab-pane fade"
                id="uses"
                role="tabpanel"
                aria-labelledby="profile-tab">
                <div className="read-more-less" data-id="700">
                    <div className=" read-toggle" data-id='0'>
                        <article className="article" dangerouslySetInnerHTML={createHTML(productDetail.uses)} />
                    </div>
                </div>
            </div>
            <div
                className="tab-pane fade"
                id="elements"
                role="tabpanel"
                aria-labelledby="contact-tab">
                <div className="read-more-less" data-id="700">
                    <div className=" read-toggle" data-id='0'>
                        <article className="article" dangerouslySetInnerHTML={createHTML(productDetail.element)} />
                    </div>
                </div>
            </div>
            <div
                className="tab-pane fade"
                id="user_manual"
                role="tabpanel"
                aria-labelledby="contact-tab">
                <div className="read-more-less" data-id="700">
                    <div className=" read-toggle" data-id='0'>
                        <article className="article" dangerouslySetInnerHTML={createHTML(productDetail.userManual)} />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProductTabsContent;
