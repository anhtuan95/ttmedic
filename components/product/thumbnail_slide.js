import React, { useEffect } from 'react'
import Link from 'next/link'
import { FILE_URL } from '../../constants/config'

function ThumbnailSlider(props) {
    const { productImages } = props

    const thumbnailCarousel = () => {
        jQuery('.product_thumb').slick({
            slidesToShow: 4,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            // fade: true,
            infinite: true,
            arrows: false,
            vertical: true,
            verticalSwiping: true,
            focusOnSelect: true,
            asNavFor: '.product_large',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                    }
                }
            ]
        });
        jQuery('.product_large').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            // fade: true,
            infinite: true,
            arrows: false,
            asNavFor: '.product_thumb',
        })
    }

    useEffect(() => {
        thumbnailCarousel()
    }, [])

    return (
        <div className="row mx-0 mt-2">
            <div className="col-md-3 col-4 px-0">
                <div className="product_thumb">
                    {
                        productImages && productImages.map((image, index) => {
                            return (
                                <div className="item" key={index}>
                                    <img src={`${FILE_URL}${image.url}`} className="img-fluid lazyload" alt="" />
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            <div className="col-md-9 col-8 px-0">
                <div className="product_large">
                    {
                        productImages && productImages.map((image, index) => {
                            return (
                                <div className="item" key={index}>
                                    <Link href={`${FILE_URL}${image.url}`} >
                                        <a data-fancybox="">
                                            <img src={`${FILE_URL}${image.url}`} className="img-fluid lazyload" alt="" />
                                        </a>
                                    </Link>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default ThumbnailSlider