import React, { useState } from 'react';
import { CONSTANT_TEXT } from '../../constants/config';
import Transformer from '../../constants/transformer';
import Link from 'next/link';

function InfoDetail(props) {
    const { productDetail, updateCartQuantity, activeLang } = props;
    const [quantity, setQuantity] = useState(1);

    const subtractItem = () => {
        setQuantity(quantity - 1);
    };

    const addItem = () => {
        setQuantity(quantity + 1);
    };

    const addToCart = (e, productDetail, quantity) => {
        e.preventDefault();
        Transformer.addToCart(e, productDetail, quantity);
        updateCartQuantity();
    };

    return (
        <div className="info_detail">
            <h1 className="fs-30 text-uppercase cl-blue">{productDetail.title}</h1>
            <p>{productDetail.description}</p>
            <div className="price">{Transformer.convertCurrency(productDetail.salePrice)}</div>
            <div className="border-tb mb-3 py-md-2">
                <div className="row align-items-center">
                    <div className="col">{CONSTANT_TEXT.AVAILABLE[activeLang]}: {productDetail.isStocking ? CONSTANT_TEXT.INSTOCK[activeLang] : CONSTANT_TEXT.OUTSTOCK[activeLang]}</div>
                    <div className="col-auto">
                        <div className="input-group bd-lr px-3">
                            <span className="input-group-btn">
                                <button
                                    type="button"
                                    className="btn btn-default btn-number"
                                    onClick={subtractItem}
                                    data-type="minus" data-field="quant[1]" disabled={quantity === 1}>
                                    <i className="fa fa-minus"></i>
                                </button>
                            </span>
                            <input type="text" name="quant[1]" className="form-control input-number" value={quantity} style={{ backgroundColor: 'transparent' }} readOnly />
                            <span className="input-group-btn">
                                <button
                                    type="button"
                                    className="btn btn-default btn-number"
                                    onClick={addItem}
                                    data-type="minus" data-field="quant[1]">
                                    <i className="fa fa-plus"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div className="col">{productDetail.capacity}</div>
                </div>
            </div>
            <div className="mt-md-5">
                <a className="btn btn-gold" onClick={(e) => addToCart(e, productDetail, quantity)}><span>{CONSTANT_TEXT.ADD_TO_CART[activeLang]}</span></a>
            </div>
        </div>
    );
}

export default InfoDetail;
