import React, { Component } from 'react'
import Link from 'next/link'
import Transformer from '../../constants/transformer'
import { CONSTANT_TEXT } from '../../constants/config'

class FilterProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            keyword: '',
        }
    }

    handleValueChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    search = (e, keyword) => {
        e.preventDefault()
        this.props.search(keyword)
    }
    render() {
        const { numbersOfResult, activeLang, categoryName } = this.props
        const { keyword } = this.state
        // const category = categoryName ? Transformer.parseJson(categoryName, activeLang) : ''
        return (
            <div className="filter_product" data-aos="fade-up">
                <div className="row align-items-center">
                    <div className="col-md-7 col-6">{numbersOfResult} {CONSTANT_TEXT.PRODUCTS[activeLang]}</div>
                    <div className="col-md-5 col-6">
                        <form className="form-inline justify-content-end">
                            <input type="text" className="form-control" placeholder={`${CONSTANT_TEXT.SEARCH[activeLang]}`} onChange={this.handleValueChange} name="keyword" value={keyword} />
                            {/* <Link href={'/tin-tuc'}> */}
                                <button className="btn" onClick={(e) => this.search(e, keyword)}>
                                    <i className="fa fa-search"></i>
                                </button>
                            {/* </Link> */}
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default FilterProduct