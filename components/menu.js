import Link from 'next/link';
import { FILE_URL, URL_NULL_IMG, LANGUAGENAMES } from '../constants/config';
import Transformer from '../constants/transformer';
import FilterSearch from '../components/search/filter_search';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';

function renderRecursiveMenu(menuData, activeLang) {
    return (
        <ul>
            {
                menuData.map(item => {
                    return (
                        <li key={item.id} className={item.children.length > 0 ? 'has-child' : ''}>
                            <Link prefetch={false} href={item.data.objectType !== 0 ? `${item.data.seoLink}` : Transformer.redirectLink(item.data.url)} locale={LANGUAGENAMES[[activeLang]]}>
                                <a >{`${Transformer.parseJson(item.data.name, activeLang)}`}</a>
                            </Link>
                            {(item.children.length > 0) && renderRecursiveMenu(item.children)}
                        </li>);
                })
            }
        </ul>
    );
}

function Menu({ setting, menuTop, activeLang, totalQuantity, idPath, routerName, languages }) {
    const activeLanguage = languages.find(x => x.languageId === activeLang);
    const activeLangIcon = activeLanguage ? activeLanguage.icon : languages[0].icon;
    const router = useRouter();

    const selectLanguage = (languageId) => {
        router.push(router.pathname, router.asPath, { locale: LANGUAGENAMES[[languageId]] });
    };

    return (
        <div id="menu_fixed">
            <div className="bg-blue topline">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-4">HOTLINE: 0916 015 199</div>
                        <div className="col-md-4">
                            <div className="text-center cl-yellow">Freeship cho đơn hàng trên 1.000.000đ</div>
                        </div>
                        <div className="col-md-4">
                            <ul className="list-inline social">
                                <li className="list-inline-item"><a><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li className="list-inline-item"><a><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li className="list-inline-item"><a><i className="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row align-items-center mx-md-0">
                    <div className="col-md-2 px-md-0">
                        <Link href="/">
                            <a className="logo_site">
                                <img src={setting.logoMobile === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${setting.logoMobile}`} className="img-fluid lazyload" alt="" />
                            </a>
                        </Link>
                    </div>
                    <div className="col-md-8 px-md-0">
                        <ul className="menu_site">
                            {menuTop && menuTop.data.menuItems &&
                                menuTop.data.menuItems.filter((menu, index) => index % 2 === 0).map(item => (
                                    <li key={item.id} className={`${item.children.length > 0 ? ' has-child' : ''} ${(idPath.includes(item.data.id) || item.data.url === routerName) ? ' active' : ''}`}>
                                        <Link prefetch={false} href={item.data.objectType !== 0 ? `${item.data.seoLink}` : Transformer.redirectLink(item.data.url)}><a >{`${Transformer.parseJson(item.data.name, activeLang)}`}</a></Link>
                                        {(item.children.length > 0) && renderRecursiveMenu(item.children, activeLang)}
                                    </li>
                                ))}
                        </ul>
                        <ul className="menu_site">
                            {menuTop && menuTop.data.menuItems &&
                                menuTop.data.menuItems.filter((menu, index) => index % 2 === 1).map(item => (
                                    <li key={item.id} className={`${item.children.length > 0 ? ' has-child' : ''} ${(idPath.includes(item.data.id) || item.data.url === routerName) ? ' active' : ''}`}>
                                        <Link prefetch={false} href={item.data.objectType !== 0 ? `${item.data.seoLink}` : Transformer.redirectLink(item.data.url)}><a >{`${Transformer.parseJson(item.data.name, activeLang)}`}</a></Link>
                                        {(item.children.length > 0) && renderRecursiveMenu(item.children, activeLang)}
                                    </li>
                                ))}
                        </ul>
                    </div>
                    <div className="col-md-2 px-md-0">
                        <ul className="user_info menu_site">
                            <li className="has-child text-center">
                                <a className="text-center">
                                    <img src={`${FILE_URL}${activeLangIcon}`} height={'12'} className="lazyload" alt="" />
                                </a>
                                <ul>
                                    <li className="has-child"><a>Ngôn ngữ</a>
                                        <ul>
                                            {languages.map(item => (
                                                <li onClick={() => selectLanguage(item.languageId)} key={item.languageId}><a>{`${item.name}`}</a></li>
                                            ))}
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li className={`text-center cart_icon ${routerName === '/gio-hang' ? 'active' : ''}`}>
                                {
                                    totalQuantity > 0 &&
                                    <div className="quantity">{totalQuantity}</div>
                                }
                                <Link href="/gio-hang">
                                    <a className="text-center"><i className="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                </Link>
                            </li>
                            <li className={` ${routerName === '/san-pham-yeu-thich' ? 'active' : ''}`}>
                                <Link href={'/san-pham-yeu-thich'}>
                                    <a className="text-center"><i className="fa fa-heart" aria-hidden="true"></i></a>
                                </Link>
                            </li>
                            <li className={`text-center ${routerName === '/ca-nhan' ? 'active' : ''}`}><a className="text-center"><i className="fa fa-user" aria-hidden="true"></i></a></li>
                        </ul>
                        <FilterSearch />
                    </div>
                </div>
            </div>
        </div>
    );
}

Menu.propTypes = {
    setting: PropTypes.any,
    menuTop: PropTypes.any,
    activeLang: PropTypes.any,
    totalQuantity: PropTypes.any,
    idPath: PropTypes.any,
    routerName: PropTypes.any,
    languages: PropTypes.any,
    updateActiveLanguage: PropTypes.any
};

export default Menu;
