import React, { useEffect } from 'react'
import Link from 'next/link'
import { CONSTANT_TEXT, FILE_URL, URL_NULL_IMG } from '../../constants/config'
import Transformer from '../../constants/transformer'

var moment = require('moment')

function HighLightEvents(props) {
    let { events, activeLang } = props
    if (!events) {
        events = [];
    }

    return (
        <div className="bg-blue py-md-5 py-3 position-relative">
            <div className="title_page">
                <div className="container">
                    <div className="fs-30 ff-montbold text-gold">{CONSTANT_TEXT.NEWS[activeLang]}</div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-md-2">
                        <div className="fs-20 mb-3 ff-montbold text-gold text-uppercase" data-aos="fade-right">{CONSTANT_TEXT.EVENT[activeLang]} <br></br>{CONSTANT_TEXT.HIGHLIGHTS[activeLang]}</div>
                    </div>
                    <div className="col-md-10" >
                        <div className="post_featured" data-aos="fade-left">
                            {
                                events !== undefined && events.length !== 0 ?
                                    events.map((event, index) => {
                                        const dayOfMonth = moment(event.startTime).format('DD')
                                        const month = moment(event.startTime).format('M')
                                        const year = moment(event.startTime).format('YYYY')

                                        return (
                                            <div className="item" key={index}>
                                                <img src={event.featureImage === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${event.featureImage}`} className="img-fluid lazyload" alt={event.altFeatureImage} />
                                                <div className="post_date">
                                                    <div className="date">{dayOfMonth}</div>
                                                    <div className="month">{CONSTANT_TEXT.MONTH[activeLang]} {month}</div>
                                                    <div className="year">{year}</div>
                                                </div>
                                                <div className="meta_post">
                                                    <h3 className="cl-blue fs-20 ff-montbold" style={{ wordBreak: 'break-word' }}>{Transformer.truncate(event.title, 35)}</h3>
                                                    <p style={{ height: '57px' }}>{Transformer.truncate(event.description, 125)}</p>
                                                    <Link href={`/${event.seoLink}`}>
                                                        <a className="btn btn-gold"><span>{CONSTANT_TEXT.READ_MORE[activeLang]}</span></a>
                                                    </Link>
                                                </div>
                                            </div>
                                        )
                                    })
                                    :
                                    (
                                        <div className="item">
                                            <img src={`${FILE_URL}${URL_NULL_IMG}`} className="img-fluid lazyload" alt="" />
                                            <div className="post_date">
                                                <div className="date">04</div>
                                                <div className="month">{CONSTANT_TEXT.JAN[activeLang]} </div>
                                                <div className="year">2020</div>
                                            </div>
                                            <div className="meta_post">
                                                <h3 className="cl-blue fs-20 ff-montbold">{CONSTANT_TEXT.OPEN_FACILITY[activeLang]}</h3>
                                                <p>{CONSTANT_TEXT.CONTENT_SAMPLE[activeLang]}</p>
                                                <a className="btn btn-gold"><span>{CONSTANT_TEXT.READ_MORE[activeLang]}</span></a>
                                            </div>
                                        </div>
                                    )
                            }
                        </div>
                        {
                            events.length > 0 &&
                            <div className="post_nav">
                                <div className="next_slider">
                                    <img src="assets/images/nextgold.png" alt="" />
                                </div>
                                <div className="prev_slider">
                                    <img src="assets/images/prevgold.png" alt="" />
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HighLightEvents