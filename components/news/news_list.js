import React, { useState, useEffect } from 'react';
import userSWR from 'swr';
import fetch from 'unfetch';
import { API_URL, TENANT_ID, WEBSITE_ID, FILE_URL, NEWS_TYPES, DATE_FORMAT, CONSTANT_TEXT, URL_NULL_IMG } from '../../constants/config';
import Transformer from '../../constants/transformer';
import Pagination from '../pagination';
import Link from 'next/link';
import PropTypes from 'prop-types';

var moment = require('moment');

const fetcher = url => fetch(url).then(r => r.json());

function NewsList(props) {
    const { activeLang } = props;
    const [news, setNews] = useState([]);
    const [totalRows, setTotalRows] = useState(0);
    const [search, setSearch] = useState({
        page: 1,
        pageSize: 4,
        type: ''
    });
    const dateFormat = DATE_FORMAT.find(item => item.languageId === activeLang);
    const { data } = userSWR(`${API_URL}/News/search/${TENANT_ID}/${WEBSITE_ID}/${activeLang}?${Transformer.convertToPramsURI(search)}`, fetcher);

    useEffect(() => {
        if (data !== undefined) {
            setNews(data.data);
            setTotalRows(data.totalRows);
        }
    });

    return (
        <div>
            <div className="cate_filter py-md-5 py-3" id="search_list">
                <div className="container">
                    <div className="row">

                        <div className="col-md-4 col-6 order-md-0">
                            <div className="fs-20 ff-montbold cl-blue text-uppercase" data-aos="fade-left">{CONSTANT_TEXT.ALL[activeLang]} {CONSTANT_TEXT.NEWS[activeLang]}</div>
                        </div>
                        <div className="col-md-3 col-6 order-md-2">
                            <ul className="list_cate" data-aos="fade-right">
                                <li className={search.type === '' ? 'active' : ''} onClick={() => setSearch({ ...search, page: 1, type: '' })}>
                                    <a className={''} > {CONSTANT_TEXT.ALL[activeLang]} </a>
                                </li>
                                <li className={search.type === 'event' ? 'active' : ''} onClick={() => setSearch({ ...search, page: 1, type: 'event' })}>
                                    <a >{CONSTANT_TEXT.EVENT[activeLang]}</a>
                                </li>
                                <li className={search.type === 'news' ? 'active' : ''} onClick={() => setSearch({ ...search, page: 1, type: 'news' })}>
                                    <a >{CONSTANT_TEXT.NEWS[activeLang]}</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-3 order-md-1"></div>
                    </div>
                </div>
            </div>
            {
                news.length > 0
                    ? <div className="filter_list py-md-5">
                        {
                            news.map((item, index) => {
                                const type = Transformer.parseJson(NEWS_TYPES[item.type].name, activeLang);

                                return (
                                    <div className="item_post" data-aos="fade-up" key={index}>
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-md-1">
                                                    <div className="cate_name py-md-4 py-2">{type.toLowerCase()}</div>
                                                </div>
                                                <div className="col-md-8">
                                                    <div className="py-md-4">
                                                        <div className="row">
                                                            <div className="col-md-6 order-md-1">
                                                                <h4 style={{ wordBreak: 'break-word' }}>
                                                                    <Link href={`/${item.seoLink}`}>
                                                                        <a >{Transformer.truncate(item.title, 65)}</a>
                                                                    </Link>
                                                                </h4>
                                                            </div>
                                                            {
                                                                item.type !== 0
                                                                    ? <React.Fragment>
                                                                        <div className="col-md-6 order-md-3">
                                                                            <div className="date_post"><strong>T: </strong>{moment(item.startTime).format(dateFormat.format)}</div>
                                                                        </div>
                                                                        <div className="col-md-6 order-md-4">
                                                                            <div className="address_post mb-3"><strong>A: </strong>{Transformer.truncate(item.note, 200)}</div>
                                                                        </div>
                                                                    </React.Fragment>
                                                                    : <React.Fragment>
                                                                        <div className="col-md-6 order-md-3"></div>
                                                                        <div className="col-md-6 order-md-4">
                                                                            <div className="address_post mb-3">{item.note}</div>
                                                                        </div>
                                                                    </React.Fragment>
                                                            }
                                                            <div className="col-md-6 order-md-2">
                                                                <p>{Transformer.truncate(item.description, 250)}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-3 thumbnail_container">
                                                    <div className="thumbnail_post">
                                                        <img src={item.featureImage === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.featureImage}`} className="img-fluid lazyload" alt={item.altFeatureImage} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })
                        }
                        {
                            totalRows > 4 &&
                            <Pagination
                                position={'end'}
                                totalRows={totalRows}
                                search={search}
                                onChangePage={(pager) => setSearch({
                                    ...search,
                                    page: pager.currentPage
                                })} />
                        }
                    </div>
                    : <div className="filter_list py-md-5">
                        <div className="item_post" data-aos="fade-up">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-1">
                                        <div className="cate_name py-md-4 py-2">{CONSTANT_TEXT.EVENT[activeLang]}</div>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="py-md-4">
                                            <div className="row">
                                                <div className="col-md-6 order-md-1">
                                                    <h4>
                                                        <a>AMIEA PRIVATE CLASS - {CONSTANT_TEXT.COURSE[activeLang]} 2 – 2019</a>
                                                    </h4>
                                                </div>
                                                <div className="col-md-6 order-md-3">
                                                    <div className="date_post">
                                                        <strong>T:</strong> 04 {CONSTANT_TEXT.JAN[activeLang]},2020
                                                    </div>
                                                </div>
                                                <div className="col-md-6 order-md-4">
                                                    <div className="address_post mb-3">
                                                        <strong>A:</strong> Tầng 1, tòa nhà Comatce Tower,số 61 Ngụy Như Kon Tum, Thanh Xuân, Hà Nội.</div>
                                                </div>
                                                <div className="col-md-6 order-md-2">
                                                    <p>{CONSTANT_TEXT.CONTENT_SAMPLE[activeLang]}</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="col-md-3">
                                        <div className="thumbnail_post">
                                            <img src={`${FILE_URL}${URL_NULL_IMG}`} className="img-fluid lazyload" alt="" ></img>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            }
        </div>
    );
}

NewsList.propTypes = {
    activeLang: PropTypes.any
};

export default NewsList;
