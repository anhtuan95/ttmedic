import React from 'react'
import Link from 'next/link'
import { NEWS_TYPES, DATE_FORMAT } from '../../constants/config'
import Transformer from '../../constants/transformer'

var moment = require('moment')

export default function RelatedPosts(props) {
    const { relatedPosts, activeLang } = props
    const dateFormat = DATE_FORMAT.find(item => item.languageId === 'vi-VN')
    return (
        <div className="col-md-3">
            <ul className="post_related">
                {
                    relatedPosts.map((post, index) => {
                        const type = Transformer.parseJson(NEWS_TYPES[post.type].name, activeLang)

                        return (
                            <li key={index}>
                                <div className="cate_name">{type}</div>
                                <h3 style={{ wordBreak: 'break-word' }}>
                                    <Link href={`/${post.seoLink}`}>
                                        <a >{Transformer.truncate(post.title, 55)}</a>
                                    </Link>
                                </h3>
                                <div className="date_post">{post.type === 1 ? moment(post.startTime).format(dateFormat.format) : ''}</div>
                            </li>
                        )
                    })
                }

            </ul>
        </div>
    )
}
