import Link from 'next/link'
import React from 'react'
import { NEWS_TYPES, DATE_FORMAT } from '../../constants/config'
import Transformer from '../../constants/transformer'

var moment = require('moment')

function createHTML(string) {
    return { __html: string }
}

export default function Content(props) {
    const { post, socialNetworks, activeLang } = props

    if (post === null) return null
    const dateFormat = DATE_FORMAT.find(item => item.languageId === 'vi-VN')
    const type = Transformer.parseJson(NEWS_TYPES[post.type].name, activeLang)

    return (
        <div className="col-md-9">
            <div className="content_post">
                <div className="cate_name">{type}</div>
                <h1 className="cl-blue">{post.title}</h1>
                <div className="mb-md-5 detail_info">
                    <div className="row">
                        <div className="col-md-4">
                            {
                                post.type === 1 &&
                                <div className="date_post">
                                    <strong>T:</strong> {moment(post.startTime).format(dateFormat.format)}
                                </div>
                            }
                        </div>
                        <div className="col-md-6">
                            {
                                post.type === 1 &&
                                <div className="address_post">
                                    <strong>A:</strong> {post.note}
                                </div>
                            }
                        </div>
                        <div className="col-md-2">
                            <ul className="list-inline text-right">
                                {
                                    socialNetworks.map(item => (
                                        <li className="list-inline-item" key={item.id}>
                                            <Link href={item.url}>
                                                <a ><i className={`fa fa-${item.icon}`} aria-hidden="true"></i></a>
                                            </Link>
                                        </li>
                                    ))
                                }
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="content_inner" dangerouslySetInnerHTML={createHTML(post.content)}></div>
            </div>
        </div>
    )
}
