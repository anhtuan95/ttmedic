import React, { useEffect, useState } from 'react';
import { API_URL, TENANT_ID, WEBSITE_ID, FILE_URL, CONSTANT_TEXT, URL_NULL_IMG } from '../../constants/config';
import Transformer from '../../constants/transformer';
import userSWR from 'swr';
import fetch from 'unfetch';

const fetcher = url => fetch(url).then(r => r.json());

const initCart = {
    items: [],
    totalQuantity: 0,
    totalPrice: 0
};

// function generateQuery(cart) {
//     let query = ''
//     cart.items.map(item => {
//         query += `listProductId=${item.productId}&`
//     })

//     return query
// }

export default function ItemsList({ activeLang }) {
    const [cart, setCart] = useState(initCart);
    const [shipFee, setShipFee] = useState(0);
    const [searchQuery, setSearchQuery] = useState('');
    const [fetchingDone, setFetchingDone] = useState(false);

    const { data, error } = userSWR(searchQuery !== '' ? `${API_URL}Products/by-local/${TENANT_ID}/${WEBSITE_ID}/${activeLang}?${searchQuery}` : null, fetcher);

    useEffect(() => {
        const localCart = JSON.parse(localStorage.getItem('cart'));
        setCart({ ...localCart });
    }, []);

    useEffect(() => {
        const query = Transformer.generateQuery(cart);
        setSearchQuery(query);
    }, [cart]);

    useEffect(() => {
        if (data !== undefined) {
            const items = [];
            let totalQuantity = 0;
            let totalPrice = 0;

            data.data.map(item => {
                cart.items.map(cartItem => {
                    if (item.productId === cartItem.productId) {
                        item.quantity = cartItem.quantity;
                        totalQuantity += cartItem.quantity;
                        totalPrice += item.salePrice * cartItem.quantity;
                        items.push(item);
                    }
                });
            });

            setCart({
                items,
                totalPrice,
                totalQuantity
            });
            setFetchingDone(true);
        }
    }, [data]);

    return (
        <div className="col-md-6 order-md-1">
            <div className="cart_info" data-aos="fade-left">
                <div className="table-responsive">
                    <table className="table table-borderless">
                        <tbody>
                            {
                                cart.items.map((item, index) => (
                                    <tr key={index}>
                                        <td style={{ width: '100px' }}>
                                            <img src={item.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.images}`} className="img-fluid lazyload" alt={item.title}></img>
                                        </td>
                                        <td>
                                            <h3>{Transformer.truncate(item.title, 60)}</h3>
                                            <div>
                                                <span className="price_single">SL: {item.quantity}</span>
                                            </div>

                                        </td>
                                        <td>
                                            <div className="price_total" style={{ width: '100px' }}>{Transformer.convertCurrency(item.salePrice * item.quantity)}</div>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
                <hr></hr>
                <div className="row">
                    <div className="col-6">{CONSTANT_TEXT.TEMPORARY[activeLang]}</div>
                    <div className="col-6">
                        <div className="text-right mb-3">{Transformer.convertCurrency(cart.totalPrice)}</div>
                    </div>
                    <div className="col-6">{CONSTANT_TEXT.DELIVERY[activeLang]}</div>
                    <div className="col-6">
                        <div className="text-right mb-3">{Transformer.convertCurrency(shipFee)} </div>
                    </div>
                    <div className="col-md-12">
                        <hr></hr>
                    </div>
                    <div className="col-6">
                        <div className="ff-mulibold cl-blue fs-20">{CONSTANT_TEXT.TOTAL[activeLang]}</div>
                    </div>
                    <div className="col-6 pl-0">
                        <div className="text-right ff-mulibold cl-blue fs-20">{Transformer.convertCurrency(cart.totalPrice + shipFee)}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}
