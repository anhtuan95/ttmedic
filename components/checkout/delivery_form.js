import React, { useEffect, useState } from 'react'
import userSWR from 'swr'
import fetch from 'unfetch'
import api from '../../constants/api'
import { SPA_API_URL, COUNTRY_ID, CONSTANT_TEXT } from '../../constants/config'
import Transformer from '../../constants/transformer'
import Link from 'next/link'

var moment = require('moment')

const fetcher = url => fetch(url).then(r => r.json())

const initCart = {
    items: [],
    totalQuantity: 0,
    totalPrice: 0,
}

export default function DeliveryForm(props) {
    const { activeLang } = props
    const [provinces, setProvinces] = useState([])
    const [districts, setDistricts] = useState([])
    const [cart, setCart] = useState(initCart)
    const [selectedProvince, setSelectedProvince] = useState('')

    const dataProvinces = userSWR(`${SPA_API_URL}Publics/get-all-province/${COUNTRY_ID}`, fetcher)

    let provinceId = selectedProvince.split('###')[1]
    const dataDistrict = userSWR(selectedProvince !== '' ? `${SPA_API_URL}Publics/get-all-district/${provinceId}` : null, fetcher)

    useEffect(() => {
        let localCart = JSON.parse(localStorage.getItem('cart'))
        setCart({ ...localCart })

    }, [])

    useEffect(() => {
        if (dataProvinces.data !== undefined) {
            setProvinces([...dataProvinces.data])
        }
    }, [dataProvinces.data])

    useEffect(() => {
        if (dataDistrict.data !== undefined) {
            setDistricts([...dataDistrict.data])
        }
    }, [dataDistrict.data])

    return (
        <div className="col-md-6 order-md-0">
            <div className="form_billing mt-3" data-aos="fade-right">
                <div className="fs-20 ff-mulibold mb-3 text-uppercase cl-blue">{CONSTANT_TEXT.DELIVER_INFOR[activeLang]}</div>
                <input type="text" className="form-control" placeholder={CONSTANT_TEXT.CUSTOMER_NAME[activeLang]} id="customer_name"></input>
                <div className="row">
                    <div className="col-md-6">
                        <input type="text" className="form-control" placeholder="Email" id="email"></input>
                    </div>
                    <div className="col-md-6">
                        <input type="text" className="form-control" placeholder={CONSTANT_TEXT.PHONE_NUMBER[activeLang]} id="phone_number"></input>
                    </div>
                </div>
                <input type="text" className="form-control" placeholder={CONSTANT_TEXT.ADDRESS[activeLang]} id="address"></input>
                <div className="row">
                    <div className="col-md-4">
                        <select name="" className="form-control" id="province" onChange={(e) => setSelectedProvince(e.target.value)}>
                            <option value="">{CONSTANT_TEXT.PROVINCE[activeLang]}</option>
                            {
                                provinces.map((item, index) => (
                                    <option
                                        value={`${Transformer.parseJson(item.name, activeLang)}###${item.id}`}
                                        key={index}>
                                        {Transformer.parseJson(item.name, activeLang)}
                                    </option>
                                ))
                            }
                        </select>
                    </div>
                    <div className="col-md-4">
                        <select name="" className="form-control" id="district">
                            <option value="">{CONSTANT_TEXT.DISTRICT[activeLang]}</option>
                            {
                                districts.length > 0 &&
                                districts.map((item, index) => (
                                    <option
                                        value={`${Transformer.parseJson(item.name, activeLang)}`}
                                        key={index}>
                                        {Transformer.parseJson(item.name, activeLang)}
                                    </option>
                                ))
                            }
                        </select>
                    </div>
                </div>
                <div className="fs-20 ff-mulibold mb-3 mt-5 text-uppercase cl-blue">{CONSTANT_TEXT.DELIVER_METHOD[activeLang]}</div>
                <input type="text" className="form-control" placeholder="Vui lòng chọn tỉnh / thành để có danh sách phương thức vận chuyển" value={"Ha Noi"} id="transport_method" style={{ backgroundColor: 'transparent' }} readOnly></input>
                <div className="fs-20 ff-mulibold mb-3 mt-5 text-uppercase cl-blue">{CONSTANT_TEXT.PAYMENT_METHOD[activeLang]}</div>
                <div className="mb-2">
                    <input type="radio" name="payment_method" value="0" id="cod"></input> <label htmlFor="cod">{CONSTANT_TEXT.PAYMENT_ON_DELIVER[activeLang]} (COD)</label>
                </div>
                <div className="mb-2">
                    <input type="radio" name="payment_method" value="1" id="banking"></input> <label htmlFor="banking">{CONSTANT_TEXT.BANK_TRANSFER[activeLang]}</label>
                </div>
                <div className="fs-20 ff-mulibold mb-3 mt-5 text-uppercase cl-blue">{CONSTANT_TEXT.ORDER_NOTE[activeLang]} </div>
                <textarea name="" id="note" cols="30" rows="4" className="form-control" placeholder=""></textarea>
                <div className="row align-items-center mt-5">
                    <div className="col-md-6 col-4">
                        <Link href="/gio-hang" >
                            <a className="cl-blue ff-mulibold"><img src="assets/images/goback.png" className="goback" alt=""></img> {CONSTANT_TEXT.CART[activeLang]}</a>
                        </Link>
                    </div>
                    <div className="col-md-6 col-8">
                        <button type="submit" className="btn btn-gold btn-block" onClick={() => placeOrder(activeLang, cart.items)}><span>{CONSTANT_TEXT.COMPLETE_ORDER[activeLang]}</span></button>
                    </div>
                </div>
            </div>
        </div>
    )
}

async function placeOrder(activeLang, items) {
    let customerName = document.getElementById('customer_name').value
    let email = document.getElementById('email').value
    let phoneNumber = document.getElementById('phone_number').value
    let address = document.getElementById('address').value
    let province = document.getElementById('province').value
    const provinceName = province !== '' ? province.split('###')[0] : ''
    let district = document.getElementById('district').value
    let orderNotes = document.getElementById('note').value
    let deliveryDate = moment(new Date()).format()
    let radios = document.getElementsByName('payment_method')
    let paymentMethods = ''

    for (let i = 0; i < radios.length; i++) {
        if (radios[i].checked) {
            paymentMethods = radios[i].value
        }
    }
    // customer name
    if (!Transformer.validateCustomerName(customerName)) {
        document.getElementById('customer_name').focus()
        alert("Customer name can not be empty and contain number or special character")
        return
    }
    if (customerName === '') {
        alert('Please enter customer name')
        return
    }

    // email
    if (email === '') {
        alert('Please enter email')
        return
    }
    if (!Transformer.validateEmail(email.trim())) {
        document.getElementById('email').focus()
        alert("You have entered an invalid email address!")
        return
    }

    // phone number
    if (phoneNumber.charAt(0) === "+") {
        phoneNumber = phoneNumber.substring(1)
    }

    if (!Transformer.validatePhoneNumber(phoneNumber).result) {
        document.getElementById('phone_number').focus()
        alert(Transformer.validatePhoneNumber(phoneNumber).message)
        return
    }

    // address
    if (address === '') {
        document.getElementById('address').focus()
        alert('Please enter address')
        return
    }

    // province
    if (province === '') {
        document.getElementById('province').focus()
        alert('Please select province')
        return
    }

    // distritct
    if (district === '') {
        document.getElementById('district').focus()
        alert('Please select district')
        return
    }

    // payment
    if (paymentMethods === '') {
        alert('Please select payment method')
        return
    }

    const placeOrder = await api.post('Orders', null, {
        languageId: activeLang,
        customerId: null,
        customerName,
        customerEmail: email.trim(),
        customerPhone: phoneNumber,
        customerAddress: address,
        transport: `${address} - ${district} - ${provinceName}`,
        paymentMethods,
        deliveryDate,
        orderNotes,
        discount: 0,
        discountType: 0,
        coupon: "string",
        adminNode: "string",
        concurrencyStamp: "string",
        orderDetails: [...items],
    })

    alert(placeOrder.message)
    if (placeOrder.code === 1) {
        document.getElementById('customer_name').value = ""
        document.getElementById('email').value = ""
        document.getElementById('phone_number').value = ""
        document.getElementById('address').value = ""
        document.getElementById('note').value = ""
        document.getElementById('transport_method').value = ""
        for (let i = 0; i < radios.length; i++) {
            if (radios[i].checked) {
                radios[i].checked = false
            }
        }
        localStorage.removeItem('cart');
        window.location.href = "/after-checkout";
    }
}
