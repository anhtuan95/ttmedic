import React, { useState, useEffect } from 'react';
import { API_URL, FILE_URL, CONSTANT_TEXT, TENANT_ID, WEBSITE_ID, URL_NULL_IMG } from '../../constants/config';
import Transformer from '../../constants/transformer';
import Link from 'next/link';
import userSWR from 'swr';
import fetch from 'unfetch';

const fetcher = url => fetch(url).then(r => r.json());

function ProductList(props) {
    const { productListHC, activeLang, updateCartQuantity } = props;

    const url = `${API_URL}Products/home-page/${TENANT_ID}/${WEBSITE_ID}/${activeLang}/5`;
    const { data, error } = userSWR(url, fetcher);
    useEffect(() => {
        if (data !== undefined && data !== null) {
            setProducts(data);
        }
    });

    const [productFavorite, setProductFavorite] = useState([]);
    const [products, setProducts] = useState([]);

    const homeProductsCarousel = () => {
        jQuery('.slide_product').not('.slick-initialized').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            infinite: true,
            dots: false,
            variableWidth: true,
            prevArrow: jQuery('.nav_product .prev_product'),
            nextArrow: jQuery('.nav_product .next_product')
        });
    };

    useEffect(() => {
        if (products.length > 0) {
            let favorite = localStorage.getItem('favorite');
            if (favorite) {
                favorite = JSON.parse(favorite);
                setProductFavorite([...favorite]);
            }
            homeProductsCarousel();
        }
    });

    const updateFavorite = async (e, productId) => {
        e.preventDefault();
        Transformer.favoriteProduct(e, productId);
        let favorite = localStorage.getItem('favorite');
        if (favorite) {
            favorite = JSON.parse(favorite);
            setProductFavorite([...favorite]);
        }
    };

    const addToCart = (e, product) => {
        e.preventDefault();
        Transformer.addToCart(e, product);
        updateCartQuantity();
    };

    return (
        <div className="beauty_product py-md-5 py-3">
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div
                            className="mb-4 fs-30 ff-montbold cl-blue text-uppercase"
                            data-aos="fade-left"
                            dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(productListHC.name, activeLang))}></div>
                        <a href="san-pham" className="btn btn-blue" data-aos="fade-right">{CONSTANT_TEXT.READ_MORE[activeLang]}</a>
                    </div>
                    <div className="col-md-6">
                        <div className="text-right">
                            <ul className="list-inline nav_product" data-aos="fade-right">
                                <li className="list-inline-item"><a className="prev_product"><i className="fa fa-long-arrow-left" aria-hidden="true"></i></a></li>
                                <li className="list-inline-item"><a className="next_product"><i className="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container-fluid px-md-0">
                <div className="row mx-md-0">
                    <div className="col-md-8 px-md-0 order-md-1">
                        <div className="slide_product" data-aos="fade-in">
                            {
                                products !== undefined
                                    ? products.map((product, index) => (
                                        <div className="item" key={index}>
                                            <div className="wrap_product">
                                                <figure>
                                                    <img src={product.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${product.images}`} className="img-fluid lazyload" alt={product.alt} />
                                                    <div className="add_to_cart">
                                                        <a onClick={(e) => addToCart(e, product)}><i className="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div className="white_list">
                                                        <a onClick={(e) => updateFavorite(e, product.productId)}>
                                                            <i className={`fa fa-heart ${productFavorite.includes(product.productId) && 'redIcon'} `} aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </figure>
                                                <div className="title_container">
                                                    <h4 style={{ wordBreak: 'break-word' }}>{Transformer.truncate(product.title, 35)}</h4>
                                                </div>
                                                <div className="product_cate ff-montbold">- {Transformer.truncate(product.description, 65)} -</div>
                                                <p style={{ lineHeight: '1rem', height: '3rem', width: '100%', overflow: 'hidden' }}>{Transformer.truncate(product.content, 120)}</p>
                                                <div className="price">{Transformer.convertCurrency(product.salePrice)}</div>
                                                <Link href={`/${product.seoLink}`} >
                                                    <a className="btn btn-blue">{CONSTANT_TEXT.READ_MORE[activeLang]}</a>
                                                </Link>
                                            </div>
                                        </div>
                                    ))
                                    : (
                                        <div className="item">
                                            <div className="wrap_product">
                                                <figure>
                                                    <img src="assets/images/product2.png" className="img-fluid lazyload" alt="" />
                                                    <div className="add_to_cart">
                                                        <a><i className="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div className="white_list">
                                                        <a><i className="fa fa-heart" aria-hidden="true"></i></a>
                                                    </div>
                                                </figure>
                                                <h4>Nelly De Vuyst organic collection</h4>
                                                <div className="product_cate ff-montbold">- Serum chỉnh sửa da ban đêm -</div>
                                                <p>Hỗ trợ quá trình tự sửa chữa những tổn thương của da vào ban đêm, giảm thiểu tối đa nếp nhăn & bổ sung độ ẩm.</p>
                                                <div className="price">2.298.000  đ</div>
                                                <a className="btn btn-blue">{CONSTANT_TEXT.READ_MORE[activeLang]}</a>
                                            </div>
                                        </div>
                                    )
                            }
                        </div>
                    </div>
                    <div className="col-md-4 px-md-0 order-md-0">
                        <table className="table mt-md-5 mt-3" data-aos="fade-up">
                            <tbody>
                                {
                                    productListHC.hardContentDetail.map((item, index) => {
                                        return (
                                            <tr key={index}>
                                                <td></td>
                                                <td>0{index + 1}</td>
                                                <td style={{ whiteSpace: 'nowrap' }}>{Transformer.parseJson(item.name, activeLang)}</td>
                                                <td style={{ whiteSpace: 'nowrap' }}>{Transformer.parseJson(item.description, activeLang)}</td>
                                            </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProductList
    ;
