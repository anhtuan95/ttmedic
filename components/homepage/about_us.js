import { FILE_URL, CONSTANT_TEXT, URL_NULL_IMG, LANGUAGENAMES } from '../../constants/config';
import Transformer from '../../constants/transformer';
import Link from 'next/link';
import PropTypes from 'prop-types';

function AboutUs(props) {
    const { aboutUsHC, activeLang } = props;

    return (
        <div className="about_us py-md-5 py-3 bg-gray">
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <h3 className="mb-4 fs-30 ff-montbold cl-blue text-uppercase" data-aos="fade-left">{Transformer.parseJson(aboutUsHC.name, activeLang)}</h3>
                    </div>
                    <div className="col-md-3">
                        <div className="ff-italic fs-14" data-aos="flip-right">
                            <p>{Transformer.parseJson(aboutUsHC.hardContentDetail[0].description, activeLang)}</p>
                            <Link href={'/gioi-thieu'} locale={LANGUAGENAMES[[activeLang]]}>
                                <a className="btn btn-blue mt-md-3 mb-3">{CONSTANT_TEXT.READ_MORE[activeLang]}</a>
                            </Link>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="cl-grey" data-aos="fade-right">
                            <p>{Transformer.parseJson(aboutUsHC.hardContentDetail[1].description, activeLang)}</p>
                        </div>
                    </div>
                </div>
                <div className="video_intro mt-md-4 mt-2" data-aos="zoom-in">
                    <img src={aboutUsHC.image !== null ? `${FILE_URL}${aboutUsHC.image}` : `${FILE_URL}${URL_NULL_IMG}`} className="img-fluid lazyload" alt="" />
                    {
                        aboutUsHC.url !== '' && aboutUsHC.url !== '#' &&
                        <Link href={Transformer.redirectLink(aboutUsHC.url)}>
                            <a className="play_video" data-fancybox>
                                <i className="fa fa-play" aria-hidden="true"></i>
                            </a>
                        </Link>
                    }
                </div>
            </div>
        </div>
    );
}

AboutUs.propTypes = {
    aboutUsHC: PropTypes.any,
    activeLang: PropTypes.any
};

export default AboutUs;
