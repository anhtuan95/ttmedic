import Link from 'next/link'
import { FILE_URL, CONSTANT_TEXT, URL_NULL_IMG } from '../../constants/config'
import Transformer from '../../constants/transformer'

function OurCustomer(props) {
    const { ourCustomers, activeLang } = props
    return (
        <div className="our_customer bg-yellow py-5">
            {
                ourCustomers.length > 0 ?
                    <div className="container">
                        {
                            ourCustomers.map((item, index) => {
                                return (
                                    <div className="item_customer" data-aos="fade-up" data-aos-delay={index * 200} key={index}>
                                        <div className="row align-items-center justify-content-center">
                                            <div className="col-md-1 col-2">
                                                {
                                                    item.url !== "" && item.url !== "#" &&
                                                    <Link href={Transformer.redirectLink(item.url)}>
                                                        <a className="icon_play play_video" data-fancybox><i className="fa fa-caret-right" aria-hidden="true"></i></a>
                                                    </Link>
                                                }
                                            </div>
                                            <div className="col-md-3 col-8">
                                                <h4>{Transformer.truncate(Transformer.parseJson(item.name, activeLang), 20)}</h4>
                                                <div className="ff-italic">
                                                    <p>{Transformer.truncate(Transformer.parseJson(item.position, activeLang), 35)}</p>
                                                </div>
                                            </div>
                                            <div className="col-md-1 col-2">
                                                <img src={item.avata === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.avata}`}
                                                    className="img-fluid lazyload" alt={Transformer.parseJson(item.alt, activeLang)} />
                                            </div>
                                            <div className="col-md-5">
                                                <div className="ff-mulibold quote_customer">{Transformer.truncate(Transformer.parseJson(item.description, activeLang), 125)}</div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                        <div className="text-center mt-4">
                            <Link href="/cam-nhan-khach-hang">
                                <a className="d-flex align-items-center justify-content-center">{CONSTANT_TEXT.SEE_MORE[activeLang]}<img src="assets/images/next.png" className="w15 ml-2" alt="" /></a>
                            </Link>
                        </div>
                    </div> :
                    <div className="container">
                        <div className="item_customer" data-aos="fade-up" data-aos-delay="0">
                            <div className="row align-items-center justify-content-center">
                                <div className="col-md-1 col-2">
                                    <a className="icon_play"><i className="fa fa-caret-right" aria-hidden="true"></i></a>
                                </div>
                                <div className="col-md-3 col-8">
                                    <h4>Mark D Hurwitz</h4>
                                    <div className="ff-italic">
                                        <p>Tiến sĩ y học tại Hoa Kì</p>
                                    </div>
                                </div>
                                <div className="col-md-1 col-2">
                                    <img src="assets/images/customer1.png" className="img-fluid lazyload" alt=""></img>
                                </div>
                                <div className="col-md-5">
                                    <div className="ff-mulibold quote_customer">Tôi rất vui khi giới thiệu bạn bè tôi đến đây. Nếu tôi là người địa phương thì tôi sẽ là khách hàng thường xuyên của J Medical Spa</div>
                                </div>
                            </div>
                        </div>
                        <div className="item_customer" data-aos="fade-up" data-aos-delay="200">
                            <div className="row align-items-center justify-content-center">
                                <div className="col-md-1 col-2">
                                    <a className="icon_play"><i className="fa fa-caret-right" aria-hidden="true"></i></a>
                                </div>
                                <div className="col-md-3 col-8">
                                    <h4>Mrs. Hồng Nhung</h4>
                                    <div className="ff-italic">
                                        <p>Cục An Toàn Thực Phẩm - Bộ Y Tế</p>
                                    </div>
                                </div>
                                <div className="col-md-1 col-2">
                                    <img src="assets/images/customer2.png" className="img-fluid lazyload" alt=""></img>
                                </div>
                                <div className="col-md-5">
                                    <div className="ff-mulibold quote_customer">Mình sẽ là khách hàng lâu dài của J Medical Spa. Không chỉ là khi mang bầu mà còn là sau sinh, và sau sinh rất là lâu nữa…</div>
                                </div>
                            </div>
                        </div>
                        <div className="item_customer" data-aos="fade-up" data-aos-delay="400">
                            <div className="row align-items-center justify-content-center">
                                <div className="col-md-1 col-2">
                                    <a className="icon_play"><i className="fa fa-caret-right" aria-hidden="true"></i></a>
                                </div>
                                <div className="col-md-3 col-8">
                                    <h4>Chị Hạnh</h4>
                                    <div className="ff-italic">
                                        <p>Bắc Giang - mẹ bầu lần 3</p>
                                    </div>
                                </div>
                                <div className="col-md-1 col-2">
                                    <img src="assets/images/customer3.png" className="img-fluid lazyload" alt=""></img>
                                </div>
                                <div className="col-md-5">
                                    <div className="ff-mulibold quote_customer">Mình sử dụng dịch vụ của J Medical Spa từ năm 2013. Ngoài dịch vụ bầu thì mình còn sử dụng rất nhiều dịch vụ khác nữa…</div>
                                </div>
                            </div>
                        </div>
                        <div className="text-center mt-4">
                            <a className="d-flex align-items-center justify-content-center">Xem tất cả <img src="assets/images/next.png" className="w15 ml-2" alt=""></img></a>
                        </div>
                    </div>
            }
        </div>
    )
}

export default OurCustomer