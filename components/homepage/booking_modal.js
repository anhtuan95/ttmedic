import React, { useState, useEffect } from 'react'
import Select, { components } from 'react-select'
import { API_URL, SPA_API_URL, CONSTANT_TEXT, TENANT_ID } from '../../constants/config'
import Transformer from '../../constants/transformer'
import api from '../../constants/api'
import userSWR from 'swr'
import fetch from 'unfetch'
import BookingSuccess from '../../components/common/booking_success'
import Link from 'next/link'
import ReactSelect from 'react-select';

var moment = require('moment')
const fetcher = url => fetch(url).then(r => r.json())

function BookingModal({ activeLang, branchContacts, branchContact }) {
    const [branchId, setBranchId] = useState(null)
    const [allServices, setAllServices] = useState(null)
    const [timeStamps, setTimeStamps] = useState(null)
    const [selectedDate, setSelectedDate] = useState(moment(new Date()).format('YYYY-MM-DD'))
    const [selectedTime, setSelectedTime] = useState(null)
    const [selectedServices, setSelectedServices] = useState(null)
    const selectboxStyle = {
        valueContainer: base => ({
            ...base,
            maxHeight: '80px',
            overflowY: 'auto',
        })
    }
    function jqueryDatePicker() {
        const locale = activeLang.split("-")[0]
        jQuery.datetimepicker.setLocale(locale);
        jQuery('#datebooking').datetimepicker({
            format: 'Y-m-d',
            timepicker: false,
            inline: true,
            minDate: 0,
            onChangeDateTime: function (dp, $input) {
                const date = $input.val()
                setSelectedDate(date)
            }
        });
    }

    const getAppointmentTimeStamps = () => {
        const now = new Date()
        const startTime = moment('08:30:00', 'HH:mm:ss')
        const endTime = moment('19:45:00', 'HH:mm:ss')
        const diffMinutes = (moment.duration(endTime.diff(startTime)).asMinutes())
        let timeStamps = [{ value: startTime.format('HH:mm:ss'), label: startTime.format('HH:mm:ss') }]

        for (let i = 0; i < (diffMinutes / 15); i++) {
            let stamp = startTime.add(15, 'm').format('HH:mm:ss')
            timeStamps.push({ value: stamp, label: stamp })
        }

        if (moment(selectedDate, 'YYYY-MM-DD').isSame(moment(now).format('YYYY-MM-DD'))) {
            const filter = timeStamps.filter(time => moment(time.value, "HH:mm:ss").isSameOrAfter(moment(now, 'HH:mm:ss')))
            return filter
        }

        return timeStamps
    }

    let url = `${SPA_API_URL}Publics/get-all-service/${TENANT_ID}/${branchId}`;
    const { data, error } = userSWR(branchId ? url : null, fetcher)

    const getSelectedServices = (e) => {
        setSelectedServices(e)
    }

    const selectBranch = (e) => {
        setBranchId(e.target.value)
    }

    const getSelectedTime = (e) => {
        setSelectedTime(e.value)
    }

    const closeBookingModal = () => {
        document.getElementById('customer').value = ""
        document.getElementById('phone').value = ""
        document.getElementById('email').value = ""
        document.getElementById('note').value = ""
        document.getElementById('branch').value = ""
        setSelectedTime(null)
        setBranchId(null)
        document.getElementById('close-booking').click()
    }

    const submitBooking = async () => {
        let customerName = document.getElementById('customer').value
        let customerEmail = document.getElementById('email').value
        let customerPhone = document.getElementById('phone').value
        let description = document.getElementById('note').value
        let services = []
        let appointmentDate = `${selectedDate}T${selectedTime}`
        setSelectedServices(null)
        setSelectedTime(null)
        let customerAddress = JSON.stringify({})

        if (selectedServices === null) {
            alert('Please select service')
            return
        } else {
            selectedServices.map(item => {
                services.push(item.value)
            })
        }

        if (customerName === '') {
            alert('Please enter customer name')
            return
        }

        if (!Transformer.validateCustomerName(customerName)) {
            document.getElementById('customer').focus()
            alert("Customer name can not be empty and contain number or special character")
            return
        }

        if (customerEmail !== "") {
            if (!Transformer.validateEmail(customerEmail.trim())) {
                document.getElementById('email').focus()
                alert("You have entered an invalid email address!")
                return
            }
        }

        if (customerPhone.charAt(0) === "+") {
            customerPhone = customerPhone.substring(1)
        }

        if (!Transformer.validatePhoneNumber(customerPhone).result) {
            document.getElementById('phone').focus()
            alert(Transformer.validatePhoneNumber(customerPhone).message)
            return
        }

        if (branchId === null) {
            alert('Please selected 1 branch')
            return
        }

        if (selectedTime === null) {
            alert('Please select appointment time')
            return
        }

        if (selectedDate === null) {
            alert('Please select appointment date')
            return
        }

        const bookingAppointment = await api.post('Bookings', null, {
            branchId,
            customerName: customerName.trim(),
            customerEmail: customerEmail.trim(),
            customerPhone,
            customerAddress,
            appointmentTime: "",
            appointmentDate,
            description,
            content: "",
            concurrencyStamp: "",
            services,
        })

        // alert(bookingAppointment.message)
        if (bookingAppointment.code === 1) {
            closeBookingModal()
            setTimeout(() => {
                document.getElementById('show_booking_success').click()
            }, 300)
        }
    }

    useEffect(() => {
        let branchServices = []

        if (data !== undefined) {
            data.map(item => {
                branchServices.push({ value: item.id, label: Transformer.parseJson(item.name, activeLang) })
            })
        }

        setAllServices(branchServices)
    }, [data])

    useEffect(() => {
        jqueryDatePicker()
        setTimeStamps(getAppointmentTimeStamps())
    }, [])

    useEffect(() => {
        setSelectedServices(null)
    }, [branchId])

    useEffect(() => {
        jqueryDatePicker()
    }, [activeLang])

    useEffect(() => {
        setTimeStamps(getAppointmentTimeStamps())
    }, [selectedDate])

    return (
        <React.Fragment>
            <div className="modal fade" id="bookingnow" tabIndex="-1" role="dialog" aria-labelledby="booking" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg">
                    <div className="modal-content">
                        <div className="modal-body">
                            <div className="title_booking">
                                <div className="icon">
                                    <i className="fa fa-calendar-o"></i>
                                </div>
                                <h5 className="cl-blue fs-20 mb-0 ff-montbold" id="booking">{CONSTANT_TEXT.BOOKING_APPOINTMENT[activeLang]}</h5>
                                <p>{CONSTANT_TEXT.SLOGAN[activeLang]}</p>
                                <a className="close" data-dismiss="modal" aria-label="Close" id="close-booking" onClick={closeBookingModal}>+</a>
                            </div>
                            <div className="form_booking">
                                <div className="row mx-0">
                                    <div className="col-md-5 px-0 order-md-1">
                                        <div className="bg-blue p-3 h-100">
                                            <p><i className="fa fa-calendar"></i> {CONSTANT_TEXT.INTEND_TIME[activeLang]}:</p>
                                            <div className="row">
                                                <div className="appointment-time-box col-md-8 my-3">
                                                    <ReactSelect
                                                        value={selectedTime ? { label: selectedTime, value: selectedTime } : null}
                                                        options={timeStamps}
                                                        placeholder={CONSTANT_TEXT.INTEND_TIME[activeLang]}
                                                        onChange={(e) => getSelectedTime(e)} />
                                                </div>
                                            </div>
                                            <div className="date_area">
                                                <div id="datebooking"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-7 px-0 order-md-0">
                                        <div className="bg-gray p-3">
                                            <div className="row align-items-center">
                                                <div className="col-md-6">
                                                    <label htmlFor="customer" className="title_text">{CONSTANT_TEXT.CUSTOMER_NAME[activeLang]}*</label>
                                                    <input type="text" id="customer" className="form-control" />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="phone" className="title_text">{CONSTANT_TEXT.PHONE_NUMBER[activeLang]}*</label>
                                                    <input type="text" id="phone" className="form-control" />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="email" className="title_text">Email</label>
                                                    <input type="text" id="email" className="form-control" />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="brand" className="title_text">{CONSTANT_TEXT.CHOOSE_BRANCH[activeLang]}:</label>
                                                    <select name="" id="branch" className="form-control" onChange={(e) => selectBranch(e)}>
                                                        <option value="">{CONSTANT_TEXT.CHOOSE_BRANCH[activeLang]}</option>
                                                        {
                                                            branchContacts.map(item => {
                                                                const name = Transformer.parseJson(item.name, activeLang)
                                                                const address = Transformer.parseJson(item.address, activeLang)

                                                                return (
                                                                    <option value={item.id} key={item.id}>{`${name}: ${address}`}</option>
                                                                )
                                                            })
                                                        }
                                                    </select>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="mb-3 title_text">{CONSTANT_TEXT.CHOOSE_SERVICES[activeLang]}</div>
                                                </div>
                                                <div className="col-md-12 mb-2">
                                                    <Select
                                                        className={'service-select'}
                                                        isMulti={true}
                                                        options={allServices}
                                                        placeholder={branchId ? CONSTANT_TEXT.CHOOSE_SERVICES[activeLang] : CONSTANT_TEXT.SELECT_BRANCH_NOTICE[activeLang]}
                                                        closeMenuOnSelect={false}
                                                        value={selectedServices}
                                                        styles={selectboxStyle}
                                                        onChange={(e) => getSelectedServices(e)} />

                                                </div>
                                                <div className="col-md-12">
                                                    <label htmlFor="note" className="title_text">{CONSTANT_TEXT.NOTE[activeLang]}</label>
                                                    <textarea name="" id="note" className="form-control" rows="2"></textarea>
                                                </div>
                                                <div className="col-md-7">
                                                    <p>{CONSTANT_TEXT.BOOKING_NOTICE[activeLang]}</p>
                                                </div>
                                                <div className="col-md-5">
                                                    <div className="text-right">
                                                        <button type="submit" className="btn btn-gold" onClick={submitBooking}><span>{CONSTANT_TEXT.BOOK_NOW[activeLang]}</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <BookingSuccess bookingTime={`${selectedDate}T${selectedTime}`} activeLang={activeLang} branchContact={branchContact} />
        </React.Fragment>
    )
}

export default BookingModal