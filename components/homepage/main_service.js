import { useEffect } from 'react';
import Link from 'next/link';
import { FILE_URL, CONSTANT_TEXT, URL_NULL_IMG } from '../../constants/config';
import Transformer from '../../constants/transformer';
import PropTypes from 'prop-types';

function MainService(props) {
    const { menu, mainServicesHC, activeLang } = props;
    const serviceCarousel = () => {
        jQuery('.slider_service').slick({
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            infinite: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    };

    useEffect(() => {
        serviceCarousel();
    }, []);

    return (
        <div className="services_primary bg-gray">
            <div className="py-md-5 py-3">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <h3 className="mb-4 fs-30 ff-montbold cl-blue" data-aos="fade-left">{Transformer.parseJson(mainServicesHC.name, activeLang)}</h3>
                            <a href="/dich-vu" className="btn btn-blue mb-3" data-aos="fade-right">{CONSTANT_TEXT.READ_MORE[activeLang]}</a>
                        </div>
                        <div className="col-md-3">
                            <div className="ff-italic fs-14" data-aos="fade-down">
                                <p>{Transformer.parseJson(mainServicesHC.hardContentDetail[0].description, activeLang)}</p>
                                <div>
                                    <img src="assets/images/star.png" className="w15" alt="" />
                                    <img src="assets/images/star.png" className="w15" alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="cl-grey" data-aos="fade-up">
                                <p>{Transformer.parseJson(mainServicesHC.hardContentDetail[1].description, activeLang)}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="content_service">
                <div className="container">
                    <div className="slider_service">
                        {menu.hasOwnProperty('menuItems') && menu.menuItems.length > 0 &&
                            menu.menuItems.map((item, index) => (
                                <div className="item" key={index}>
                                    <Link prefetch={false} href={item.data.objectId ? `${item.data.seoLink}` : Transformer.redirectLink(item.data.url)}>
                                        <a className="item_service" data-aos="fade-up" data-aos-delay={(index % 3) * 200}>
                                            <figure>
                                                <img src={item.data.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.data.images}`} className="img-fluid lazyload" alt={''} />
                                            </figure>
                                            <h4>{Transformer.truncate(Transformer.parseJson(item.data.name, activeLang), 50)}</h4>
                                        </a>
                                    </Link>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

MainService.propTypes = {
    menu: PropTypes.any,
    mainServicesHC: PropTypes.any,
    activeLang: PropTypes.any
};

export default MainService;
