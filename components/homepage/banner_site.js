import React, { useEffect } from 'react';
import { FILE_URL, URL_NULL_IMG } from '../../constants/config';
function BannerSite(props) {
    const { banner } = props;

    const relatedBannerCarousel = () => {
        jQuery('.banner_site').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: true,
            infinite: true,
            arrows: true,
            prevArrow: '<div class="prev-slider"><span></span></div>',
            nextArrow: '<div class="next-slider"><span></span></div>'
        });
    };

    useEffect(() => {
        relatedBannerCarousel();
    }, []);

    return (
        <div>
            {
                banner !== undefined && banner.bannerItem.length > 0
                    ? <div className="banner_site">
                        {
                            banner.bannerItem.map(item => (
                                <div className="item" key={item.id}>
                                    <img src={item.images === null ? `${FILE_URL}${URL_NULL_IMG}` : `${FILE_URL}${item.images}`}
                                        className="img-fluid lazyload" alt={item.alt} />
                                </div>
                            ))
                        }
                    </div>
                    : <div className="banner_site">
                        <div className="item">
                            <img src={'assets/images/banner1.jpg'} className="img-fluid lazyload" alt="" />
                        </div>
                        <div className="item">
                            <img src={'assets/images/banner2.jpg'} className="img-fluid lazyload" alt="" />
                        </div>
                    </div>
            }
        </div>
    );
}

export default BannerSite
    ;
