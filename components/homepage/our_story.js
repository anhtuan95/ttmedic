import Transformer from '../../constants/transformer'

function OurStory(props) {
    const { ourCustomersHC, activeLang } = props
    return (
        <div className="our_story bg-blue text-center py-5">
            <div className="container">
                <div
                    className="text-gold ff-mulibold fs-30 mb-2"
                    data-aos="fade-up"
                    dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(ourCustomersHC.name, activeLang))}>
                </div>
                <div
                    className="text-gold"
                    data-aos="zoom-out"
                    dangerouslySetInnerHTML={Transformer.createHTML(Transformer.parseJson(ourCustomersHC.description, activeLang))}>
                </div>
            </div>
        </div>
    )
}

export default OurStory