var App = {
    scrollfixed: function () {
        if (jQuery('#header_site').length) {
            sticky = jQuery('#menu_fixed');
            var scroll = jQuery(window).scrollTop();
            var height = jQuery('#header_site').outerHeight();
            /* if (scroll >= height) {
                sticky.addClass('fixed');
            }else if (scroll == height) {
                sticky.removeClass('fixed');
            } */
            var lastScrollTop = 0;
            jQuery(window).scroll(function () {
                var scroll = jQuery(window).scrollTop();
                if (scroll >= height) {
                    sticky.addClass('fixed');
                } else if (scroll < height) {
                    sticky.removeClass('fixed');
                }
                lastScrollTop = scroll;
            });
        }
        jQuery('#header_site .showmenu').click(function () {
            jQuery('#header_site .showmenu .hamburger').toggleClass('is-active');
            jQuery('.menu_mobile').toggleClass('showmenu');
            jQuery('.overlay_menu').toggleClass('showmenu');
        });
        jQuery('.overlay_menu').click(function () {
            jQuery('#header_site .showmenu .hamburger').removeClass('is-active');
            jQuery('.menu_mobile').removeClass('showmenu');
            jQuery('.overlay_menu').removeClass('showmenu');
        });
        jQuery('.menu_site ul li a').click(function () {
            jQuery('#header_site .showmenu .hamburger').removeClass('is-active');
            jQuery('.menu_mobile').removeClass('showmenu');
            jQuery('.overlay_menu').removeClass('showmenu');
        });
    },
    // sliderBanner: function () {
    //     jQuery('.banner_site').slick({
    //         slidesToShow: 1,
    //         autoplay: true,
    //         autoplaySpeed: 5000,
    //         speed: 1500,
    //         dots: true,
    //         // fade: true,
    //         infinite: true,
    //         arrows: true,
    //         prevArrow: '<div class="prev-slider"><span></span></div>',
    //         nextArrow: '<div class="next-slider"><span></span></div>',
    //     });
    // },
    slideProduct: function () {
        // jQuery('.slide_product').slick({
        //     slidesToShow: 1,
        //     autoplay: true,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     infinite: true,
        //     dots: false,
        //     variableWidth: true,
        //     prevArrow: jQuery('.nav_product .prev_product'),
        //     nextArrow: jQuery('.nav_product .next_product'),
        // });
        // jQuery('.slider_banner_product').slick({
        //     slidesToShow: 1,
        //     autoplay: true,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     dots: false,
        //     // fade: true,
        //     infinite: true,
        //     arrows: true,
        //     prevArrow: '<div class="prev-slider"><span></span></div>',
        //     nextArrow: '<div class="next-slider"><span></span></div>',
        // });
        // jQuery('.product_related').slick({
        //     slidesToShow: 4,
        //     autoplay: true,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     dots: false,
        //     // fade: true,
        //     infinite: true,
        //     arrows: true,
        //     prevArrow: '<div class="prev-slider"><span></span></div>',
        //     nextArrow: '<div class="next-slider"><span></span></div>',
        //     responsive: [
        //         {
        //             breakpoint: 767,
        //             settings: {
        //                 slidesToShow: 1,
        //             }
        //         }
        //     ]
        // });
        // jQuery('.product_thumb').slick({
        //     slidesToShow: 4,
        //     autoplay: true,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     dots: false,
        //     // fade: true,
        //     infinite: true,
        //     arrows: false,
        //     vertical: true,
        //     verticalSwiping: true,
        //     focusOnSelect: true,
        //     asNavFor: '.product_large',
        //     responsive: [
        //         {
        //             breakpoint: 767,
        //             settings: {
        //                 slidesToShow: 3,
        //             }
        //         }
        //     ]
        // });
        // jQuery('.product_large').slick({
        //     slidesToShow: 1,
        //     autoplay: true,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     dots: false,
        //     // fade: true,
        //     infinite: true,
        //     arrows: false,
        //     asNavFor: '.product_thumb',
        // })
    },
    goTop: function () {
        jQuery('#goTop').click(function (e) {
            e.preventDefault();
            jQuery('html, body').animate({ scrollTop: 0 }, 2000);
            return false;
        });
    },
    sliderSevice: function () {
        // jQuery('.slider_service').slick({
        //     slidesToShow: 3,
        //     autoplay: true,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     infinite: true,
        //     dots: false,
        //     responsive: [
        //         {
        //             breakpoint: 767,
        //             settings: {
        //                 slidesToShow: 1,
        //             }
        //         }
        //     ]
        // });
        // jQuery('.slide_procedure').slick({
        //     slidesToShow: 3,
        //     autoplay: false,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     infinite: true,
        //     dots: false,
        //     variableWidth: true,
        //     nextArrow: jQuery('.nav_slide .next_procedure'),
        // });

        // jQuery('.slider_sv').slick({
        //     slidesToShow: 1,
        //     autoplay: false,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     infinite: true,
        //     dots: false,
        //     prevArrow: '<div class="prev-slider"><span></span></div>',
        //     nextArrow: '<div class="next-slider"><span></span></div>',
        // });
    },
    sliderAbout: function () {
        // jQuery('.slider_staff').slick({
        //     slidesToShow: 1,
        //     autoplay: false,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     infinite: true,
        //     dots: false,
        //     prevArrow: jQuery('.staff_nav .prev-slider'),
        //     nextArrow: jQuery('.staff_nav .next-slider'),
        // });
        // jQuery('.slider_partner').slick({
        //     slidesToShow: 5,
        //     autoplay: true,
        //     autoplaySpeed: 5000,
        //     speed: 1500,
        //     infinite: true,
        //     dots: false,
        //     centerMode: true,
        //     prevArrow: jQuery('.partner_nav .prev-slider'),
        //     nextArrow: jQuery('.partner_nav .next-slider'),
        //     responsive: [
        //         {
        //             breakpoint: 1024,
        //             settings: {
        //                 slidesToShow: 3,
        //             }
        //         },
        //         {
        //             breakpoint: 600,
        //             settings: {
        //                 slidesToShow: 2,
        //             }
        //         },
        //         {
        //             breakpoint: 480,
        //             settings: {
        //                 slidesToShow: 1,
        //             }
        //         }
        //         // You can unslick at a given breakpoint now by adding:
        //         // settings: "unslick"
        //         // instead of a settings object
        //     ]
        // });
    },
    showsubcate: function () {
        jQuery('.list_cate_product ul > li').click(function (e) {
            e.stopPropagation();
            jQuery(this).toggleClass('togglemenu');
            jQuery(this).find('> ul').stop().slideToggle();
        });
    },
    // sliderContact: function () {
    //     jQuery('.slider_location1').slick({
    //         slidesToShow: 1,
    //         autoplay: true,
    //         autoplaySpeed: 5000,
    //         speed: 1500,
    //         infinite: true,
    //         dots: true,
    //         prevArrow: jQuery('.slider1_prev'),
    //         nextArrow: jQuery('.slider1_next'),
    //     });
    //     jQuery('.slider_location2').slick({
    //         slidesToShow: 1,
    //         autoplay: true,
    //         autoplaySpeed: 5000,
    //         speed: 1500,
    //         infinite: true,
    //         dots: true,
    //         prevArrow: jQuery('.slider2_prev'),
    //         nextArrow: jQuery('.slider2_next'),
    //     });
    // },
    sliderNews: function () {
        jQuery('.post_featured').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            infinite: true,
            dots: true,
            prevArrow: jQuery('.post_nav .prev_slider'),
            nextArrow: jQuery('.post_nav .next_slider')
        });
    },
    changeQuanlity: function () {
        $('.btn-number').click(function (e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if (type == 'minus') {
                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }
                } else if (type == 'plus') {
                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }
                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function () {
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function () {
            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            var name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled');
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled');
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }
        });
        $('.input-number').keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    },
    menumobile: function () {
        jQuery('.menu_mobile .menu_site li.has-child:not(.not-grandchildren)').click(function (e) {
            e.stopPropagation();
            jQuery(this).find('>ul').slideToggle();
            jQuery(this).toggleClass('showmenu');
        });
    },
    bookingmodal: function () {
        // jQuery('#datebooking').datepicker({
        //     format: 'd.m.Y',
        //     timepicker: false,
        //     inline: true,
        //     minDate: 0,
        //     onSelect: (e) => {
        //         // var dateObject = $(this).datepicker('getDate');
        //         console.log('date', e)
        //     }
        // });
    },
    menushow: function () {
        jQuery('.menu_site > li > ul li > ul ').on('click', 'li.has-child:not(.showchild)', function (e) {
            e.preventDefault();
            var $this = jQuery(this);
            jQuery('.menu_site > li > ul > li > ul li.showchild >ul').slideUp();
            jQuery('.menu_site > li > ul li > ul li.has-child').removeClass('showchild');
            setTimeout(function () {
                $this.addClass('showchild');
                $this.find('>ul').slideDown();
            }, 100);
        });
    },
    searchbtn: function () {
        jQuery('body').on('click', '#header_site .search button', function (e) {
            if (jQuery('#header_site .search input').val() == '') {
                e.preventDefault();
                jQuery('#header_site .form_search').toggleClass('showform');
                jQuery('#menu_fixed .form_search').toggleClass('showform');
            }
        });
        jQuery('body').on('click', '#menu_fixed .search button', function (e) {
            if (jQuery('#menu_fixed .search input').val() == '') {
                e.preventDefault();
                jQuery('#header_site .form_search').toggleClass('showform');
                jQuery('#menu_fixed .form_search').toggleClass('showform');
            }
        });
    },
    readMore: function () {
        var contentArray = [];
        var index = '';
        var clickedIndex = '';
        var minimumLength = $('.read-more-less').attr('data-id');
        var initialContentLength = [];
        var initialContent = [];
        var readMore = "...<br/><span class='read-more'>Read More</span>";
        var readLess = "<br/><span class='read-less'>Read Less</span>";
        $('.read-toggle').each(function () {
            index = $(this).attr('data-id');
            contentArray[index] = $(this).html();
            initialContentLength[index] = $(this).html().length;
            if (initialContentLength[index] > minimumLength) {
                initialContent[index] = $(this).html().substr(0, minimumLength);
            } else {
                initialContent[index] = $(this).html();
            }
            $(this).html(initialContent[index] + readMore);
            // console.log(initialContent[0]);
        });
        $(document).on('click', '.read-more', function () {
            $(this).fadeOut(100, function () {
                clickedIndex = $(this).parents('.read-toggle').attr('data-id');
                $(this).parents('.read-toggle').html(contentArray[clickedIndex] + readLess);
            });
        });
        $(document).on('click', '.read-less', function () {
            $(this).fadeOut(100, function () {
                clickedIndex = $(this).parents('.read-toggle').attr('data-id');
                $(this).parents('.read-toggle').html(initialContent[clickedIndex] + readMore);
            });
        });
    }
};

jQuery(document).ready(function () {
    setTimeout(function () {
        App.scrollfixed();
        App.searchbtn();
        // App.sliderBanner();
        App.slideProduct();
        App.sliderSevice();
        // App.sliderAbout();
        // App.sliderContact();
        App.sliderNews();
        App.bookingmodal();
        App.showsubcate();
        App.changeQuanlity();
        App.menumobile();
        App.menushow();
        App.goTop();
        App.readMore();

        AOS.init();

        window.addEventListener('load', AOS.refresh());
    }, 500);
});
