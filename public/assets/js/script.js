// var Login = (function () {
//     var handleLogin = function () {
//         $('.login-form').validate({
//             errorElement: 'span', // default input error message container
//             errorClass: 'help-block', // default input error message class
//             focusInvalid: false, // do not focus the last invalid input
//             rules: {
//                 username: {
//                     required: true
//                 },
//                 password: {
//                     required: true
//                 },
//                 remember: {
//                     required: false
//                 }
//             },

//             messages: {
//                 username: {
//                     required: 'Username is required.'
//                 },
//                 password: {
//                     required: 'Password is required.'
//                 }
//             },

//             invalidHandler: function (event, validator) { // display error alert on form submit
//                 $('.alert-danger', $('.login-form')).show();
//             },

//             highlight: function (element) { // hightlight error inputs
//                 $(element)
//                     .closest('.form-group').addClass('has-error'); // set error class to the control group
//             },

//             success: function (label) {
//                 label.closest('.form-group').removeClass('has-error');
//                 label.remove();
//             },

//             errorPlacement: function (error, element) {
//                 error.insertAfter(element.closest('.input-icon'));
//             },

//             submitHandler: function (form) {
//                 form.submit(); // form validation success, call ajax form submit
//             }
//         });

//         $('.login-form input').keypress(function (e) {
//             if (e.which == 13) {
//                 if ($('.login-form').validate().form()) {
//                     $('.login-form').submit(); // form validation success, call ajax form submit
//                 }
//                 return false;
//             }
//         });

//         $('.forget-form input').keypress(function (e) {
//             if (e.which == 13) {
//                 if ($('.forget-form').validate().form()) {
//                     $('.forget-form').submit();
//                 }
//                 return false;
//             }
//         });

//         $('#forget-password').click(function () {
//             $('.login-form').hide();
//             $('.forget-form').show();
//         });

//         $('#back-btn').click(function () {
//             $('.login-form').show();
//             $('.forget-form').hide();
//         });
//     };

//     return {
//         // main function to initiate the module
//         init: function () {
//             handleLogin();

//             // init background slide images
//             $('.login-bg').backstretch([
//                 '/assets/images/login/bg1.jpg',
//                 '/assets/images/login/bg2.jpg',
//                 '/assets/images/login/bg3.jpg'
//             ], {
//                 fade: 1000,
//                 duration: 8000
//             }
//             );

//             $('.forget-form').hide();
//         }

//     };
// }());

// jQuery(document).ready(function () {
//     Login.init();
// });

(function ($) {
    'use strict';

    //     // init cubeportfolio
    //     $('#js-grid-juicy-projects').cubeportfolio({
    //         filters: '#js-filters-juicy-projects',
    //         loadMore: '#js-loadMore-juicy-projects',
    //         loadMoreAction: 'click',
    //         layoutMode: 'grid',
    //         defaultFilter: '*',
    //         animationType: 'quicksand',
    //         gapHorizontal: 35,
    //         gapVertical: 30,
    //         gridAdjustment: 'responsive',
    //         mediaQueries: [{
    //             width: 1500,
    //             cols: 5
    //         }, {
    //             width: 1100,
    //             cols: 4
    //         }, {
    //             width: 800,
    //             cols: 3
    //         }, {
    //             width: 480,
    //             cols: 2
    //         }, {
    //             width: 320,
    //             cols: 1
    //         }],
    //         caption: 'overlayBottomReveal',
    //         displayType: 'sequentially',
    //         displayTypeSpeed: 80,

    //         // lightbox
    //         lightboxDelegate: '.cbp-lightbox',
    //         lightboxGallery: true,
    //         lightboxTitleSrc: 'data-title',
    //         lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

    //         // singlePage popup
    //         singlePageDelegate: '.cbp-singlePage',
    //         singlePageDeeplinking: true,
    //         singlePageStickyNavigation: true,
    //         singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
    //         singlePageCallback: function (url, element) {
    //             // to update singlePage content use the following method: this.updateSinglePage(yourContent)
    //             var t = this;

    //             $.ajax({
    //                 url: url,
    //                 type: 'GET',
    //                 dataType: 'html',
    //                 timeout: 10000
    //             })
    //                 .done(function (result) {
    //                     t.updateSinglePage(result);
    //                 })
    //                 .fail(function () {
    //                     t.updateSinglePage('AJAX Error! Please refresh the page!');
    //                 });
    //         }
    //     });

    // activation carousel plugin
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 5,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            0: {
                slidesPerView: 3
            },
            992: {
                slidesPerView: 4
            }
        }
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });
    // change carousel item height
    // gallery-top
    const productCarouselTopWidth = $('.gallery-top').outerWidth();
    $('.gallery-top').css('height', productCarouselTopWidth);

    // gallery-thumbs
    const productCarouselThumbsItemWith = $('.gallery-thumbs .swiper-slide').outerWidth();
    $('.gallery-thumbs').css('height', productCarouselThumbsItemWith);

    // activation zoom plugin
    var $easyzoom = $('.easyzoom').easyZoom();
})(jQuery, window, document);
